# kjpc-django-template

[![pipeline status](https://gitlab.com/kjpc-tech/kjpc-django-template/badges/master/pipeline.svg)](https://gitlab.com/kjpc-tech/kjpc-django-template/commits/master)

## Basic [Django](https://www.djangoproject.com/) web application template.
Django web application template using [Django](https://www.djangoproject.com/), [Bootstrap4](https://getbootstrap.com/docs/4.1/getting-started/introduction/), and [webpack](https://webpack.js.org/). Optionally includes [Vue.js](https://vuejs.org/), [Celery](http://www.celeryproject.org/), [Django Channels](https://github.com/django/channels), [GeoDjango](https://docs.djangoproject.com/en/2.1/ref/contrib/gis/), [Django REST Framework](https://www.django-rest-framework.org/), [Wagtail](https://wagtail.io/), an ecommerce implementation using [Satchless](http://satchless.com/), and [pytest](https://docs.pytest.org/en/latest/).

Designed for and works well with [kjpc-ansible-roles](https://gitlab.com/kjpc-tech/kjpc-ansible-roles).

---
## KJ PC | [kjpc.tech](https://kjpc.tech/) | [kyle@kjpc.tech](mailto:kyle@kjpc.tech)
---

## Installation (requires [Python3](https://www.python.org/))
1. Install [cookiecutter](https://github.com/cookiecutter/cookiecutter) if not installed
    - `pip3 install cookiecutter`
2. Create project from template
    - `cookiecutter https://gitlab.com/kjpc-tech/kjpc-django-template`
3. Change directories to project root
    - `cd /path/to/new/project/`
4. Install [invoke](http://www.pyinvoke.org/index.html) if not installed
    - `pip3 install invoke`
5. Install extra dependencies if needed
    - *PostgreSQL*
        - `postgresql` (expects local install on port `5432`)
        - An empty database called `<project name>` with user `<project name>` and password `<project name>` must exist
        - Ensure that the `<project name>` user has `CREATEDB` permission so the test database can be created
    - *Celery*
        - `redis` (expects local install on port `6379`)
    - *Channels*
        - `redis` (expects local install on port `6379`)
    - *GeoDjango ([docs](https://docs.djangoproject.com/en/dev/ref/contrib/gis/install/geolibs/))*
        - `binutils`
        - `libproj-dev`
        - `gdal-bin`
        - If using Sqlite (the default) ([docs](https://docs.djangoproject.com/en/dev/ref/contrib/gis/install/spatialite/))
            - `libsqlite3-mod-spatialite`
        - If using PostgreSQL ([docs](https://docs.djangoproject.com/en/dev/ref/contrib/gis/install/postgis/))
            - `postgis`
            - `<project name>` database must have the `postgis` extension setup
            - Ensure that the `<project name>` user has `SUPERUSER` permission so the test database can be created

6. Setup local environment
    - `invoke site.setup`
        - *Extra parameters*
        - `--[no-]run-createsuperuser` (default `True`)
---

## Development (requires [Python3](https://www.python.org/))
1. Run development server
    - `invoke site.runserver`
2. Run webpack
    - `invoke site.runwebpack`
3. Run celery (if using celery)
    - `invoke site.runceleryworker`
    - `invoke site.runcelerybeat`
4. Run tests (uses [geckodriver](https://selenium-python.readthedocs.io/installation.html#drivers) by default)
    - `invoke site.test`

---

## Deployment (requires [Python3](https://www.python.org/), [Git](https://git-scm.com/), [Ansible](https://www.ansible.com/))
1. Make sure the git repository is initialized and hosted somewhere
2. Do initial setup
    - `invoke remote.setup`
    - This will add required submodules
3. Edit `ansible/hosts.yml`
    - Change the example host to point to a remote VPS (such a DigitalOcean Droplet or AWS EC2)
4. Update variables
    - Edit `ansible/vars/vars.yml`
    - Create and edit `ansible/vars/secrets.yml` with `ansible-vault`
5. Update deployment play
    - Update `ansible/deploy-prod.yml` to fit specific needs
    - (nginx config, database, settings, ssl, backups, monit)
6. First Deploy
    - `invoke remote.deploy --user=root --tags=apt,postfix,users` (deploy project basics using `root` user)
    - This will only work the first time because the `root` user login will be disabled. Use the newly created user afterwards
    - Make sure `main_user_name` in `ansible/vars/secrets.yml` matches `ansible_default_user` in `tasks.py` for convenience
7. Deploy
    - `invoke remote.deploy` (deploy entire project from bottom to top)
    - `invoke remote.deploy --tags=django` (deploy django portion of project)
    - `invoke remote.deploy --tags=monit` (deploy monit portion of project)
8. Updates and Rebooting
    - `invoke remote.update`
    - `invoke remote.reboot`

---

## CI/CD (requires [Git](https://git-scm.com/), [GitLab](https://gitlab.com/))
1. Make sure the git repository is initialized and hosted on [GitLab](https://gitlab.com/)
2. Set `SSH_PRIVATE_KEY_VAL` in GitLab CI/CD settings to the SSH private key that can access the remote host
3. Set `ANSIBLE_VAULT_PASS_VAL` in GitLab CI/CD settings to the Ansible Vault password for the encrypted variables
4. Push changes to GitLab
5. GitLab will test and deploy the project
6. More Information at [https://docs.gitlab.com/ee/ci/quick_start/](https://docs.gitlab.com/ee/ci/quick_start/)

---

### Screenshot
![](screenshot.png)

### Change Log
#### 1.7
  - Switch to cookiecutter template
  - Switch to django-environ based settings
  - Switch to `npm ci` in `.gitlab-ci.yml`

#### 1.6
  - Switched to requirements folder
  - Fixed celery ansible config
  - Made admin template override simpler

#### 1.5
  - Changed default Django version to 2.2 LTS

#### 1.4
  - Added a basic ecommerce implementation
  - Switched to latest node versions by default
  - Added user registration
  - Added user password reset
  - Renamed `site.setup` to `site.setup-initial`
  - Changed `site.setup` to work with non-initial installs
  - Added some integration with ecommerce and wagtail
  - Added flow tests to ecommerce app
  - Added more test configuration to the CI script
  - Made Vue.js optional
  - Updated ecommerce product admin
  - Relaxed initial NodeJS dependency versions
  - Added login link to ecommerce checkout pages
  - Changed default deployment `django_node_commands`
  - Fixed submodule initialization
  - Added more Django commands
  - Fixed logging configuration
  - Fixed testing configuration
  - Removed django-compressor

#### 1.3
  - Added support for wagtail
  - Added support for pytest
  - Separated tasks into groups
  - Added title block to templates
  - Allow for setup without creating superuser
  - Added CI to test setup
  - Added screenshot to README

#### 1.2
  - Cleaned up webpack configuration
  - Cleaned up `tasks.py`
  - Added selenium tests to accounts app
  - Added option to setup with PostgreSQL

#### 1.1
  - Added GitLab CI/CD script
  - Switched to geckodriver (Firefox) for testing
  - Switched django-developer-panel to django-debug-toolbar
  - Upgraded to Django 2.1
  - Enabled local backups by default for deployment
  - Updated ansible variables
  - Moved ansible deployment tasks into main tasks

#### 1.0
  - Initial version

### License
[MIT License](LICENSE)
