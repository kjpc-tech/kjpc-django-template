import os
import shutil


if "{{ cookiecutter.use_celery }}".lower() == "n":
    os.remove("{{ cookiecutter.project_name }}/{{ cookiecutter.project_name }}/celery.py")


if "{{ cookiecutter.use_channels }}".lower() == "n":
    os.remove("{{ cookiecutter.project_name }}/{{ cookiecutter.project_name }}/routing.py")


if "{{ cookiecutter.use_pytest }}".lower() == "n":
    os.remove("{{ cookiecutter.project_name }}/pytest.ini")
    os.remove("{{ cookiecutter.project_name }}/conftest.py")


if "{{ cookiecutter.use_wagtail }}".lower() == "n":
    shutil.rmtree("{{ cookiecutter.project_name }}/{{ cookiecutter.project_name }}/templates/wagtailadmin/")
    shutil.rmtree("{{ cookiecutter.project_name }}/frontend/")


if "{{ cookiecutter.use_ecommerce }}".lower() == "n":
    shutil.rmtree("{{ cookiecutter.project_name }}/store/")
elif "{{ cookiecutter.use_wagtail }}".lower() == "n":
    os.remove("{{ cookiecutter.project_name }}/store/wagtail_hooks.py")
    os.remove("{{ cookiecutter.project_name }}/store/wagtail_models.py")
    os.remove("{{ cookiecutter.project_name }}/store/migrations/0002_wagtail_cms.py")


if "{{ cookiecutter.use_vuejs }}".lower() == "n":
    os.remove("webpack/src/vueapp.js")
