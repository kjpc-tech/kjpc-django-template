import setuptools


setuptools.setup(
    name="kjpc-django-template",
    author="Kyle Johnson",
    author_email="kyle@kjpc.tech",
    license="MIT",
    url="https://gitlab.com/kjpc-tech/kjpc-django-template/",
    description="Django website template,",
    python_requires=">=3.6",
    install_requires=[
        "cookiecutter",
        "pytest",
        "pytest-cookies",
    ],
)
