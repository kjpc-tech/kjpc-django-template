def test_generation_with_defaults(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_no_debug(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'debug': 'n',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_postgresql(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'db': 'postgresql',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_celery(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'use_celery': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_channels(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'use_channels': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_geodjango(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'use_geodjango': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_drf(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'use_drf': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_vuejs(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'use_vuejs': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_wagtail(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'use_wagtail': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_ecommerce(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'use_ecommerce': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()


def test_generation_with_pytest(cookies):
    result = cookies.bake(extra_context={
        'project_name': 'kjpc_django_template_test',
        'use_pytest': 'y',
    })

    assert result.exit_code == 0
    assert result.exception is None
    assert result.project.isdir()
