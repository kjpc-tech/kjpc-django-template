invoke
Django>=3.0,<3.1
pytz
sqlparse
{% if cookiecutter.db == 'postgresql' %}psycopg2-binary{% endif %}
{% if cookiecutter.use_channels == 'y' %}daphne{% else %}uwsgi{% endif %}
django-environ
django-bootstrap4
django-registration
django-webpack-loader
nodeenv
{% if cookiecutter.cache == 'redis' %}django-redis
redis{% else %}python-memcached{% endif %}
{% if cookiecutter.use_pytest == 'y' %}pytest-django{% endif %}
{% if cookiecutter.use_pytest == 'y' and cookiecutter.use_channels == 'y' %}pytest-asyncio{% endif %}
{% if cookiecutter.use_celery == 'y' %}celery[redis]
django-celery-results
django-celery-beat{% endif %}
{% if cookiecutter.use_channels == 'y' %}channels
channels_redis{% endif %}
{% if cookiecutter.use_drf == 'y' %}djangorestframework{% endif %}
{% if cookiecutter.use_wagtail == 'y' %}wagtail{% endif %}
{% if cookiecutter.use_ecommerce == 'y' %}django-localflavor
django-mptt
django-orderable
django-payments
django-phonenumber-field
django-prices<2.0
phonenumberslite
pillow
satchless
sorl-thumbnail
stripe{% endif %}
