import os
import re
import urllib.request

from invoke import task, Collection


current_dir = os.path.dirname(os.path.abspath(__file__))
venv = os.path.join(current_dir, "venv")
site_dir = os.path.join(current_dir, "{{ cookiecutter.project_name }}")
ansible_dir = os.path.join(current_dir, "ansible")

# make sure this is the same as `main_user_name` in `ansible/vars/secrets.yml`
ansible_default_user = 'webber'


def _run_command(ctx, command, directory=current_dir):
    print(f"Running '{command}'.")
    with ctx.prefix(f'source {venv}/bin/activate'):
        with ctx.cd(directory):
            ctx.run(command, pty=True)


@task
def check(ctx, show_warnings=False):
    """
    Run the manage.py check command.
    """
    show_warnings = " -Wall" if show_warnings else ""
    cmd = f'python{show_warnings} manage.py check'
    _run_command(ctx, cmd, directory=site_dir)


@task
def test(ctx, show_warnings=False, extra=None):
    """
    Run the test command.
    """

    {% if cookiecutter.use_pytest == 'y' %}
    cmd = 'pytest --ds={{ cookiecutter.project_name }}.settings.test'{% else %}
    show_warnings = " -Wall" if show_warnings else ""
    cmd = f'python{show_warnings} manage.py test --settings={{ cookiecutter.project_name }}.settings.test'{% endif %}
    if extra:
        cmd += f' {extra}'
    _run_command(ctx, cmd, directory=site_dir)


@task
def makemigrations(ctx, extra=None):
    """
    Run the manage.py makemigrations command.
    """
    cmd = 'python manage.py makemigrations'
    if extra:
        cmd += f' {extra}'
    _run_command(ctx, cmd, directory=site_dir)


@task
def migrate(ctx, extra=None):
    """
    Run the manage.py migrate command.
    """
    cmd = 'python manage.py migrate'
    if extra:
        cmd += f' {extra}'
    _run_command(ctx, cmd, directory=site_dir)


@task
def showmigrations(ctx, extra=None):
    """
    Run the manage.py showmigrations command.
    """
    cmd = 'python manage.py showmigrations'
    if extra:
        cmd += f' {extra}'
    _run_command(ctx, cmd, directory=site_dir)


@task
def createsuperuser(ctx):
    """
    Run the manage.py createsuperuser command.
    """
    cmd = 'python manage.py createsuperuser'
    _run_command(ctx, cmd, directory=site_dir)


@task
def dumpdata(ctx, extra=None):
    """
    Run the manage.py dumpdata command.
    """
    cmd = 'python manage.py dumpdata'
    if extra:
        cmd += f' {extra}'
    _run_command(ctx, cmd, directory=site_dir)


@task
def loaddata(ctx, fixture):
    """
    Run the manage.py loaddata command.
    """
    cmd = f'python manage.py loaddata {fixture}'
    _run_command(ctx, cmd, directory=site_dir)


@task
def runserver(ctx):
    """
    Run the manage.py runserver command.
    """
    cmd = 'python manage.py runserver'
    _run_command(ctx, cmd, directory=site_dir)


@task
def shell(ctx):
    """
    Run the manage.py shell command.
    """
    cmd = 'python manage.py shell'
    _run_command(ctx, cmd, directory=site_dir)

{% if cookiecutter.use_celery == 'y' %}
@task
def runcelerybeat(ctx):
    """
    Start celery beat. Only works if celery is installed.
    """
    cmd = 'celery -A {{ cookiecutter.project_name }} beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler'
    _run_command(ctx, cmd, directory=site_dir)


@task
def runceleryworker(ctx):
    """
    Start celery workers. Only works if celery is installed.
    """
    cmd = 'celery -A {{ cookiecutter.project_name }} worker -l info --concurrency=2'
    _run_command(ctx, cmd, directory=site_dir)
{% endif %}

@task
def runwebpack(ctx, debug=True, watch=True):
    """
    Run the webpack command.
    """
    cmd = 'npm run build'
    if debug:
        cmd += '-dev'
    if watch:
        cmd += '-watch'
    _run_command(ctx, cmd)


@task
def setup(ctx, run_createsuperuser=True):
    """
    Setup the development environment.
    """
    # create virtual environment if it doesn't exist
    if not os.path.exists(os.path.join(venv, "bin", "activate")):
        print(f"Creating virtual environment '{venv}'.")
        ctx.run(f'python3 -m venv {venv}', pty=True)
    else:
        print(f"Virtual environment '{venv}' not created because it already exists.")

    # install dependencies
    with ctx.prefix(f'source {venv}/bin/activate'):
        with open(os.path.join(current_dir, "requirements", "pinned.txt")) as f:
            has_pinned_dependencies = 'Django' in f.read()
        if has_pinned_dependencies:
            # install pinned dependencies
            print("Installing dependencies from requirements/pinned.txt.")
            ctx.run('pip install -r requirements/pinned.txt', pty=True)
        else:
            # install normal dependencies
            print("Installing dependencies from requirements/base.txt.")
            ctx.run('pip install -r requirements/base.txt', pty=True)

            # freeze dependencies
            print("Freezing dependencies into requirements/pinned.txt.")
            ctx.run('pip freeze | grep -v "pkg-resources" > requirements/pinned.txt', pty=True)

            # install development dependencies
            print("Installing dependencies from requirements/development.txt.")
            ctx.run('pip install -r requirements/development.txt', pty=True)

    # install node modules
    with ctx.prefix(f'source {venv}/bin/activate'):
        print("Creating node environment.")
        ctx.run('nodeenv --python-virtualenv', pty=True)
        print("Installing node package.")
        if os.path.exists(os.path.join(current_dir, "package-lock.json")):
            ctx.run('npm ci', pty=True)
        else:
            ctx.run('npm install', pty=True)
        print("Building initial assets.")
        runwebpack(ctx, debug=True, watch=False)

    # install selenium driver
    driver_tar_dir = os.path.join(current_dir, "geckodriver.zip")
    driver_extract_dir = os.path.join(venv, "bin")
    driver_path = os.path.join(driver_extract_dir, "geckodriver")
    if not os.path.exists(driver_path):
        print("Getting Selenium driver.")
        try:
            page = urllib.request.urlopen("https://github.com/mozilla/geckodriver/releases/latest/")
            page_content = str(page.read(), 'utf-8')
            latest_driver = re.search(r'<a href="/mozilla/geckodriver/releases/tag/v\d+\.\d+\.\d+">(v\d+\.\d+\.\d+)</a>', page_content).group(1)
        except:
            latest_driver = None
        if latest_driver:
            ctx.run(f'wget https://github.com/mozilla/geckodriver/releases/download/{latest_driver}/geckodriver-{latest_driver}-linux64.tar.gz -O {driver_tar_dir}', hide=True)
            ctx.run(f'tar -xf {driver_tar_dir} -C {driver_extract_dir}', hide=True)
            ctx.run(f'chmod +x {driver_path}', hide=True)
            ctx.run(f'rm {driver_tar_dir}', hide=True)

    # create local settings
    settings_template = os.path.join(site_dir, ".env.template")
    settings_file = os.path.join(site_dir, ".env")
    if not os.path.exists(settings_file):
        print(f"Creating development settings '{settings_file}' from '{settings_template}'.")
        ctx.run(f'cp {settings_template} {settings_file}', hide=True)

    # run checks
    check(ctx)

    # migrate database
    migrate(ctx)

    # create superuser
    if run_createsuperuser:
        createsuperuser(ctx)

    {% if cookiecutter.use_wagtail == 'y' %}
    # install fixtures
    loaddata(ctx, "first-data.json")
    {% endif %}

    # run tests
    test(ctx)


@task
def deploy_setup(ctx):
    """
    First time setup for deployment. Add required submodules.
    """
    ansible_relative_dir = ansible_dir.replace(current_dir, '.')
    for repo_url, repo_path in [
        ("https://gitlab.com/kjpc-tech/kjpc-ansible-roles.git", os.path.join(ansible_relative_dir, "vendor", "kjpc-ansible-roles")),
        ("https://gitlab.com/cdetar/cfd-common-roles.git", os.path.join(ansible_relative_dir, "vendor", "cfd-common-roles")),
    ]:
        cmd = f'git submodule add {repo_url} {repo_path}'
        ctx.run(cmd, pty=True)


@task
def deploy(ctx, user=ansible_default_user, tags=None):
    """
    Deploy {{ cookiecutter.project_name }}.
    """
    tags = f'--tags {tags}' if tags is not None else ''
    cmd = f'ansible-playbook --inventory hosts.yml deploy-prod.yml --user {user} --ask-vault-pass {tags}'
    _run_command(ctx, cmd, directory=ansible_dir)


@task
def remote_update(ctx, user=ansible_default_user):
    """
    Update the remote server(s).
    """
    cmd = f'ansible-playbook --inventory hosts.yml updates.yml --user {user} --ask-vault-pass'
    _run_command(ctx, cmd, directory=ansible_dir)


@task
def remote_reboot(ctx, user=ansible_default_user):
    """
    Reboot the remote server(s).
    """
    cmd = f'ansible-playbook --inventory hosts.yml reboot.yml --user {user} --ask-vault-pass'
    _run_command(ctx, cmd, directory=ansible_dir)


site = Collection('site')
site.add_task(setup)
site.add_task(check)
site.add_task(test)
site.add_task(makemigrations)
site.add_task(migrate)
site.add_task(showmigrations)
site.add_task(createsuperuser)
site.add_task(dumpdata)
site.add_task(loaddata)
site.add_task(runserver)
site.add_task(shell)
{% if cookiecutter.use_celery == 'y' %}site.add_task(runcelerybeat)
site.add_task(runceleryworker){% endif %}
site.add_task(runwebpack)

remote = Collection('remote')
remote.add_task(remote_update, 'update')
remote.add_task(remote_reboot, 'reboot')
remote.add_task(deploy_setup, 'setup')
remote.add_task(deploy)

namespace = Collection()
namespace.add_collection(site)
namespace.add_collection(remote)
