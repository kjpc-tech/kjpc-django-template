$(document).ready(function() {
  try {
    feather.replace();
  } catch (_err) {
    console.log("Unable to use feather.");
  }

  try {
    $('[data-toggle="tooltip"]').tooltip();
  } catch (_err) {
    console.log("Unable to use tooltip.");
  }

  try {
    $('[data-toggle="popover"]').popover();
  } catch (_err) {
    console.log("Unable to use popover.");
  }
});
