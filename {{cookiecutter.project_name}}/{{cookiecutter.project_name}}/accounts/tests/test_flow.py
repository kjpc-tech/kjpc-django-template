import re

from django.core import mail
from django.test import override_settings
from django.urls import reverse

from {{ cookiecutter.project_name }}.tests.test_base import UserDataMixin, SeleniumTestCase


class RegisterUserFlowTests(UserDataMixin, SeleniumTestCase):
    @override_settings(REGISTRATION_OPEN=True)
    def test_registration_flow_with_valid_credentials(self):
        self.selenium.get(f"{self.live_server_url}{reverse('django_registration_register')}")

        self.assertEqual(self.get_current_url(), reverse('django_registration_register'))

        self.update_input_value('email', 'new@example.com')
        self.update_input_value('password1', 'longsecret')
        self.update_input_value('password2', 'longsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:profile'))

        self.assertPageDoesContain('Your account has been created successfully.')

    @override_settings(REGISTRATION_OPEN=True)
    def test_registration_flow_with_invalid_credentials(self):
        self.selenium.get(f"{self.live_server_url}{reverse('django_registration_register')}")

        self.assertEqual(self.get_current_url(), reverse('django_registration_register'))

        self.update_input_value('email', 'newemail')
        self.update_input_value('password1', 'longsecret')
        self.update_input_value('password2', 'longsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('django_registration_register'))

        self.update_input_value('email', 'new@example.com')
        self.update_input_value('password1', 'longsecret')
        self.update_input_value('password2', 'longwrong')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('django_registration_register'))

        self.update_input_value('email', 'new@example.com')
        self.update_input_value('password1', 'longwrong')
        self.update_input_value('password2', 'longsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('django_registration_register'))

    @override_settings(REGISTRATION_OPEN=False)
    def test_registration_flow_when_closed(self):
        self.selenium.get(f"{self.live_server_url}{reverse('django_registration_register')}")

        self.assertEqual(self.get_current_url(), reverse('django_registration_disallowed'))

    @override_settings(REGISTRATION_OPEN=True)
    def test_registration_flow_next(self):
        self.selenium.get(f"{self.live_server_url}{reverse('django_registration_register')}?next={reverse('accounts:password-change')}")

        self.assertEqual(self.get_current_url(), reverse('django_registration_register'))

        self.update_input_value('email', 'new@example.com')
        self.update_input_value('password1', 'longsecret')
        self.update_input_value('password2', 'longsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:password-change'))

        self.assertPageDoesContain('Your account has been created successfully.')


class LoginFlowTests(UserDataMixin, SeleniumTestCase):
    def test_login_flow_with_valid_credentials(self):
        self.selenium.get(f"{self.live_server_url}{reverse('accounts:login')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.update_input_value('username', 'test@example.com')
        self.update_input_value('password', 'secret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:profile'))

    def test_login_flow_with_invalid_credentials(self):
        self.selenium.get(f"{self.live_server_url}{reverse('accounts:login')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.update_input_value('username', 'wrong@example.com')
        self.update_input_value('password', 'secret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.update_input_value('username', 'test@example.com')
        self.update_input_value('password', 'wrong')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.selenium.get(f"{self.live_server_url}{reverse('accounts:profile')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

    def test_login_flow_next(self):
        self.selenium.get(f"{self.live_server_url}{reverse('accounts:login')}?next={reverse('accounts:password-change')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.update_input_value('username', 'test@example.com')
        self.update_input_value('password', 'secret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:password-change'))


class LogoutFlowTests(UserDataMixin, SeleniumTestCase):
    def test_logout_flow_when_logged_in(self):
        self.login_user('test@example.com', 'secret')

        self.selenium.get(f"{self.live_server_url}{reverse('accounts:profile')}")

        self.selenium.find_element_by_link_text("Logout").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.assertPageDoesContain('You have logged out.')


class ChangePasswordFlowTests(UserDataMixin, SeleniumTestCase):
    def test_password_change_flow_with_valid_credentials(self):
        self.login_user('test@example.com', 'secret')

        self.selenium.get(f"{self.live_server_url}{reverse('accounts:password-change')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:password-change'))

        self.update_input_value('old_password', 'secret')
        self.update_input_value('new_password1', 'newsecret')
        self.update_input_value('new_password2', 'newsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:profile'))

        self.assertPageDoesContain('Your password was changed successfully.')

    def test_password_change_flow_with_invalid_credentials(self):
        self.login_user('test@example.com', 'secret')

        self.selenium.get(f"{self.live_server_url}{reverse('accounts:password-change')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:password-change'))

        self.update_input_value('old_password', 'wrong')
        self.update_input_value('new_password1', 'newsecret')
        self.update_input_value('new_password2', 'newsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:password-change'))

        self.update_input_value('old_password', 'secret')
        self.update_input_value('new_password1', 'wrong')
        self.update_input_value('new_password2', 'newsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:password-change'))

        self.update_input_value('old_password', 'secret')
        self.update_input_value('new_password1', 'newsecret')
        self.update_input_value('new_password2', 'wrong')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:password-change'))


class ResetPasswordFlowTests(UserDataMixin, SeleniumTestCase):
    def test_reset_password_flow_with_valid_link_and_valid_credentials(self):
        self.selenium.get(f"{self.live_server_url}{reverse('accounts:password-reset')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:password-reset'))

        self.update_input_value('email', 'test@example.com')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:password-reset-done'))

        self.assertEqual(len(mail.outbox), 1)

        reset_link = re.search(r'{}{}[^/]+/[^/]+/'.format(
            re.escape(self.live_server_url),
            re.escape(reverse('accounts:password-reset'))
        ), str(mail.outbox[0].message())).group()

        self.selenium.get(reset_link)

        reset_url = self.get_current_url()

        self.assertTrue(reset_url.startswith(reverse('accounts:password-reset')))

        self.assertPageDoesntContain('The password reset link was invalid.')

        self.update_input_value('new_password1', 'newsecret')
        self.update_input_value('new_password2', 'newsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.assertPageDoesContain('Your password was reset successfully.')

    def test_reset_password_flow_with_valid_link_and_invalid_credentials(self):
        self.selenium.get(f"{self.live_server_url}{reverse('accounts:password-reset')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:password-reset'))

        self.update_input_value('email', 'test@example.com')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:password-reset-done'))

        self.assertEqual(len(mail.outbox), 1)

        reset_link = re.search(r'{}{}[^/]+/[^/]+/'.format(
            re.escape(self.live_server_url),
            re.escape(reverse('accounts:password-reset'))
        ), str(mail.outbox[0].message())).group()

        self.selenium.get(reset_link)

        reset_url = self.get_current_url()

        self.assertTrue(reset_url.startswith(reverse('accounts:password-reset')))

        self.assertPageDoesntContain('The password reset link was invalid.')

        self.update_input_value('new_password1', 'newsecret')
        self.update_input_value('new_password2', 'wrongsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertTrue(reset_url.startswith(reverse('accounts:password-reset')))

        self.update_input_value('new_password1', 'wrongsecret')
        self.update_input_value('new_password2', 'newsecret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertTrue(reset_url.startswith(reverse('accounts:password-reset')))

    def test_reset_password_flow_with_invalid_link(self):
        self.selenium.get(f"{self.live_server_url}{reverse('accounts:password-reset-confirm', args=('test', 'wrongtoken'))}")

        self.assertPageDoesContain('The password reset link was invalid.')
