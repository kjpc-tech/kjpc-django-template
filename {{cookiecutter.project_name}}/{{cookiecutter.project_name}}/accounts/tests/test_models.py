from django.conf import settings
from django.contrib.auth import get_user_model

from {{ cookiecutter.project_name }}.tests.test_base import BaseTestCase

from accounts.models import User


class UserModelTests(BaseTestCase):
    def test_create_user(self):
        self.assertEqual(get_user_model().objects.all().count(), 0)
        self.assertFalse(self.client.login(email='test@example.com', password='secret'))

        userObj = get_user_model().objects.create_user('test@example.com', 'secret')

        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        self.assertTrue(userObj.is_active)
        self.assertFalse(userObj.is_staff)
        self.assertFalse(userObj.is_superuser)
        self.assertFalse(userObj.is_email_confirmed)

    def test_create_superuser(self):
        self.assertEqual(get_user_model().objects.all().count(), 0)
        self.assertFalse(self.client.login(email='test@example.com', password='secret'))

        userObj = get_user_model().objects.create_superuser('test@example.com', 'secret')

        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        self.assertTrue(userObj.is_active)
        self.assertTrue(userObj.is_staff)
        self.assertTrue(userObj.is_superuser)
        self.assertTrue(userObj.is_email_confirmed)

    def test_inactive_user(self):
        self.assertEqual(get_user_model().objects.all().count(), 0)
        self.assertFalse(self.client.login(email='test@example.com', password='secret'))

        userObj = get_user_model().objects.create_user('test@example.com', 'secret', is_active=False)

        self.assertEqual(get_user_model().objects.all().count(), 1)
        self.assertFalse(self.client.login(email='test@example.com', password='secret'))

        self.assertFalse(userObj.is_active)
        self.assertFalse(userObj.is_staff)
        self.assertFalse(userObj.is_superuser)
        self.assertFalse(userObj.is_email_confirmed)

    def test_user_model_settings(self):
        self.assertEqual(settings.AUTH_USER_MODEL, 'accounts.User')

        self.assertIs(get_user_model(), User)
