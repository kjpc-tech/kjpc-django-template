from django.core import mail
from django.test import override_settings
from django.urls import reverse

from {{ cookiecutter.project_name }}.tests.test_base import BaseTestCase, UserDataMixin


class AccountsRegistrationViewTests(UserDataMixin, BaseTestCase):
    @override_settings(REGISTRATION_OPEN=True)
    def test_registration_view_with_valid_credentials(self):
        response = self.client.get(reverse('django_registration_register'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'django_registration/registration_form.html')

        response = self.client.post(reverse('django_registration_register'), {
            'email': 'new@example.com',
            'password1': 'longsecret',
            'password2': 'longsecret',
        }, follow=True)

        self.assertRedirects(response, reverse('accounts:profile'))

        self.assertContains(response, 'Your account has been created successfully.')

    @override_settings(REGISTRATION_OPEN=True)
    def test_registration_view_with_invalid_credentials(self):
        response = self.client.get(reverse('django_registration_register'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'django_registration/registration_form.html')

        response = self.client.post(reverse('django_registration_register'), {
            'email': 'newemail',
            'password1': 'longsecret',
            'password2': 'longsecret',
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'django_registration/registration_form.html')
        self.assertGreater(len(response.context['form'].errors.keys()), 0)

        response = self.client.post(reverse('django_registration_register'), {
            'email': 'new@example.com',
            'password1': 'longsecret',
            'password2': 'longwrong',
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'django_registration/registration_form.html')
        self.assertGreater(len(response.context['form'].errors.keys()), 0)

        response = self.client.post(reverse('django_registration_register'), {
            'email': 'new@example.com',
            'password1': 'longwrong',
            'password2': 'longsecret',
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'django_registration/registration_form.html')
        self.assertGreater(len(response.context['form'].errors.keys()), 0)

    @override_settings(REGISTRATION_OPEN=False)
    def test_registration_view_when_closed(self):
        response = self.client.get(reverse('django_registration_register'))

        self.assertRedirects(response, reverse('django_registration_disallowed'))

    @override_settings(REGISTRATION_OPEN=True)
    def test_registration_view_next(self):
        response = self.client.post(f"{reverse('django_registration_register')}?next={reverse('accounts:password-change')}", {
            'email': 'new@example.com',
            'password1': 'longsecret',
            'password2': 'longsecret',
        }, follow=True)

        self.assertRedirects(response, reverse('accounts:password-change'))

        self.assertContains(response, 'Your account has been created successfully.')


class AccountsLoginViewTests(UserDataMixin, BaseTestCase):
    def test_login_view_context(self):
        with self.settings(REGISTRATION_OPEN=True):
            response = self.client.get(reverse('accounts:login'))

            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'accounts/login.html')

            self.assertTrue(response.context['is_registration_open'])

        with self.settings(REGISTRATION_OPEN=False):
            response = self.client.get(reverse('accounts:login'))

            self.assertEqual(response.status_code, 200)
            self.assertTemplateUsed(response, 'accounts/login.html')

            self.assertFalse(response.context['is_registration_open'])

    def test_login_view_with_valid_credentials(self):
        response = self.client.get(reverse('accounts:login'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/login.html')

        response = self.client.post(reverse('accounts:login'), {
            'username': 'test@example.com',
            'password': 'secret',
        })

        self.assertRedirects(response, reverse('accounts:profile'))

    def test_login_view_with_invalid_credentials(self):
        response = self.client.get(reverse('accounts:login'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/login.html')

        response = self.client.post(reverse('accounts:login'), {
            'username': 'wrong@example.com',
            'password': 'secret',
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/login.html')
        self.assertGreater(len(response.context['form'].errors.keys()), 0)

        response = self.client.post(reverse('accounts:login'), {
            'username': 'test@example.com',
            'password': 'wrong',
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/login.html')
        self.assertGreater(len(response.context['form'].errors.keys()), 0)

        response = self.client.get(reverse('accounts:profile'))

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('accounts:profile')}")

    def test_login_view_next(self):
        response = self.client.post(f"{reverse('accounts:login')}?next={reverse('accounts:password-change')}", {
            'username': 'test@example.com',
            'password': 'secret',
        })

        self.assertRedirects(response, reverse('accounts:password-change'))


class AccountsLogoutViewTests(UserDataMixin, BaseTestCase):
    def test_logout_view_when_logged_in(self):
        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.post(reverse('accounts:logout'), follow=True)

        self.assertRedirects(response, reverse('accounts:login'))

        self.assertTemplateUsed(response, 'accounts/login.html')

        self.assertContains(response, 'You have logged out.')

    def test_logout_view_when_not_logged_in(self):
        response = self.client.post(reverse('accounts:logout'), follow=True)

        self.assertRedirects(response, reverse('accounts:login'))

        self.assertTemplateUsed(response, 'accounts/login.html')

        self.assertContains(response, 'You have logged out.')


class AccountsPasswordChangeViewTests(UserDataMixin, BaseTestCase):
    def test_password_change_view_with_valid_credentials(self):
        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('accounts:password-change'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-change.html')

        response = self.client.post(reverse('accounts:password-change'), {
            'old_password': 'secret',
            'new_password1': 'newsecret',
            'new_password2': 'newsecret',
        }, follow=True)

        self.assertRedirects(response, reverse('accounts:profile'))

        self.assertContains(response, 'Your password was changed successfully.')

    def test_password_change_view_with_invalid_credentials(self):
        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('accounts:password-change'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-change.html')

        response = self.client.post(reverse('accounts:password-change'), {
            'old_password': 'wrong',
            'new_password1': 'newsecret',
            'new_password2': 'newsecret',
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-change.html')
        self.assertGreater(len(response.context['form'].errors.keys()), 0)

        response = self.client.post(reverse('accounts:password-change'), {
            'old_password': 'secret',
            'new_password1': 'wrong',
            'new_password2': 'newsecret',
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-change.html')
        self.assertGreater(len(response.context['form'].errors.keys()), 0)

        response = self.client.post(reverse('accounts:password-change'), {
            'old_password': 'secret',
            'new_password1': 'newsecret',
            'new_password2': 'wrong',
        })

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-change.html')
        self.assertGreater(len(response.context['form'].errors.keys()), 0)


class AccountsPasswordResetViewTests(UserDataMixin, BaseTestCase):
    def test_password_reset_view_with_valid_email(self):
        response = self.client.get(reverse('accounts:password-reset'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-reset.html')

        response = self.client.post(reverse('accounts:password-reset'), {
            'email': 'test@example.com',
        }, follow=True)

        self.assertRedirects(response, reverse('accounts:password-reset-done'))

        self.assertEqual(len(mail.outbox), 1)

    def test_password_reset_view_with_invalid_email(self):
        response = self.client.get(reverse('accounts:password-reset'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-reset.html')

        response = self.client.post(reverse('accounts:password-reset'), {
            'email': 'invalid@example.com',
        }, follow=True)

        self.assertRedirects(response, reverse('accounts:password-reset-done'))

        self.assertEqual(len(mail.outbox), 0)


class AccountsPasswordResetDoneViewTests(UserDataMixin, BaseTestCase):
    def test_password_reset_done_view(self):
        response = self.client.get(reverse('accounts:password-reset-done'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-reset-done.html')


class AccountsPasswordResetConfirmViewTests(UserDataMixin, BaseTestCase):
    def _get_password_reset_token(self, email):
        response = self.client.post(reverse('accounts:password-reset'), {
            'email': email,
        })

        self.assertRedirects(response, reverse('accounts:password-reset-done'))

        self.assertEqual(len(mail.outbox), 1)

        self.assertIn('uid', response.context)
        self.assertIn('token', response.context)

        return (
            response.context['uid'],
            response.context['token'],
        )

    def test_password_reset_confirm_view_with_valid_link_and_valid_credentials(self):
        uid, token = self._get_password_reset_token('test@example.com')

        reset_link = reverse('accounts:password-reset-confirm', args=(uid, token))

        response = self.client.get(reset_link)

        self.assertEqual(response.status_code, 302)

        reset_url = response.url

        self.assertTrue(reset_url.startswith(reverse('accounts:password-reset')))
        self.assertIn(uid, reset_url)

        response = self.client.post(reset_url, {
            'new_password1': 'newsecret',
            'new_password2': 'newsecret',
        }, follow=True)

        self.assertRedirects(response, reverse('accounts:login'))

        self.assertContains(response, 'Your password was reset successfully.')

    def test_password_reset_confirm_view_with_valid_link_and_invalid_credentials(self):
        uid, token = self._get_password_reset_token('test@example.com')

        reset_link = reverse('accounts:password-reset-confirm', args=(uid, token))

        response = self.client.get(reset_link)

        self.assertEqual(response.status_code, 302)

        reset_url = response.url

        self.assertTrue(reset_url.startswith(reverse('accounts:password-reset')))
        self.assertIn(uid, reset_url)

        response = self.client.post(reset_url, {
            'new_password1': 'newsecret',
            'new_password2': 'wrongsecret',
        })

        self.assertEqual(response.status_code, 200)

        response = self.client.post(reset_url, {
            'new_password1': 'wrongsecret',
            'new_password2': 'newsecret',
        })

        self.assertEqual(response.status_code, 200)

    def test_password_reset_confirm_view_with_invalid_link(self):
        response = self.client.get(reverse('accounts:password-reset-confirm', args=('test', 'wrongtoken')))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/password-reset-confirm.html')

        self.assertFalse(response.context['validlink'])


class AccountsProfileViewTests(UserDataMixin, BaseTestCase):
    def test_profile_view(self):
        response = self.client.get(reverse('accounts:profile'), follow=True)

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('accounts:profile')}")

        self.assertTemplateUsed(response, 'accounts/login.html')

        self.assertContains(response, 'Please login to see this page.')

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('accounts:profile'))

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'accounts/profile.html')
