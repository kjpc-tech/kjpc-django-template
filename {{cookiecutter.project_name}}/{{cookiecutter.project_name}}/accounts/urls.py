from django.urls import path

from accounts import views


app_name = 'accounts'


urlpatterns = [
    path('login/', views.AccountsLoginView.as_view(), name='login'),
    path('logout/', views.AccountsLogoutView.as_view(), name='logout'),
    path('password/change/', views.AccountsPasswordChangeView.as_view(), name='password-change'),
    path('password/reset/', views.AccountsPasswordResetView.as_view(), name='password-reset'),
    path('password/reset/done/', views.AccountsPasswordResetDoneView.as_view(), name='password-reset-done'),  # noqa
    path('password/reset/<uidb64>/<token>/', views.AccountsPasswordResetConfirmView.as_view(), name='password-reset-confirm'),  # noqa
    path('profile/', views.AccountsProfileView.as_view(), name='profile'),
]
