from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import (
    LoginView, LogoutView, PasswordChangeView, PasswordResetView,
    PasswordResetDoneView, PasswordResetConfirmView,
)
from django.views.generic import TemplateView
from django.urls import reverse, reverse_lazy
from django.utils.http import is_safe_url

from django_registration.backends.one_step.views import RegistrationView

from .forms.registration_forms import UserRegistrationForm


class AccountsRegistrationView(RegistrationView):
    form_class = UserRegistrationForm

    def get_success_url(self, user=None):
        messages.info(self.request, "Your account has been created successfully.")

        _next = self.request.GET.get('next', None)

        if _next and is_safe_url(
            url=_next,
            allowed_hosts=self.request.get_host(),
            require_https=self.request.is_secure(),
        ):
            return _next

        return settings.LOGIN_REDIRECT_URL

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['next'] = self.request.GET.get('next', None)

        return context


class AccountsLoginView(LoginView):
    template_name = 'accounts/login.html'


class AccountsLogoutView(LogoutView):
    next_page = 'accounts:login'

    def dispatch(self, request, *args, **kwargs):
        response = super().dispatch(request, *args, **kwargs)

        messages.info(request, "You have logged out.")

        return response


class AccountsPasswordChangeView(PasswordChangeView):
    template_name = 'accounts/password-change.html'
    success_url = reverse_lazy('accounts:profile')

    def form_valid(self, form):
        response = super().form_valid(form)

        messages.info(self.request, "Your password was changed successfully.")

        return response


class AccountsPasswordResetView(PasswordResetView):
    template_name = 'accounts/password-reset.html'
    email_template_name = 'accounts/password-reset-email.html'
    subject_template_name = 'accounts/password-reset-email-subject.txt'

    def get_success_url(self):
        _next = self.request.GET.get('next', None)

        success_url = reverse('accounts:password-reset-done')

        if _next:
            success_url = f'{success_url}?next={_next}'

        return success_url

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['next'] = self.request.GET.get('next', None)

        return context


class AccountsPasswordResetDoneView(PasswordResetDoneView):
    template_name = 'accounts/password-reset-done.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['next'] = self.request.GET.get('next', None)

        return context


class AccountsPasswordResetConfirmView(PasswordResetConfirmView):
    template_name = 'accounts/password-reset-confirm.html'
    success_url = reverse_lazy('accounts:login')

    def form_valid(self, form):
        response = super().form_valid(form)

        messages.info(self.request, "Your password was reset successfully.")

        return response


class AccountsProfileView(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/profile.html'
