from wagtail.core.blocks import (
    CharBlock, RichTextBlock, BlockQuoteBlock, StreamBlock,
    StructBlock, ChoiceBlock, URLBlock, StaticBlock,
)
from wagtail.embeds.blocks import EmbedBlock
from wagtail.images.blocks import ImageChooserBlock


class HeadingBlock(StructBlock):
    level = ChoiceBlock(choices=[
        ('h1', 'H1'),
        ('h2', 'H2'),
        ('h3', 'H3'),
        ('h4', 'H4'),
        ('h5', 'H5'),
        ('h6', 'H6'),
    ], help_text='From largest H1 to smallest H6.')
    heading = CharBlock(help_text='Heading text.')

    class Meta:
        template = 'frontend/blocks/heading_block.html'
        icon = 'title'


class AddressBlock(StructBlock):
    line1 = CharBlock(help_text='Address line 1.')
    line2 = CharBlock(required=False, help_text='Address line 2.')
    city = CharBlock()
    state = CharBlock(max_length=2, help_text='State abbreviation.')
    zip_code = CharBlock(min_length=5, max_length=5)
    map_link = URLBlock(required=False, help_text='Link to this address on Google Maps.')

    class Meta:
        label = 'Address'
        template = 'frontend/blocks/address_block.html'
        icon = 'form'


class VerticalSpaceBlock(StaticBlock):
    class Meta:
        admin_text = 'Extra vertical space.'
        template = 'frontend/blocks/vertical_space_block.html'
        icon = 'placeholder'


class HorizontalLineBlock(StaticBlock):
    class Meta:
        admin_text = 'Horizonal line accross page.'
        template = 'frontend/blocks/horizontal_line_block.html'
        icon = 'horizontalrule'


class ContentStreamBlock(StreamBlock):
    heading = HeadingBlock()
    paragraph = RichTextBlock()
    image = ImageChooserBlock(
        template='frontend/blocks/image_block.html',
    )
    embed = EmbedBlock()
    blockquote = BlockQuoteBlock()
    address = AddressBlock()
    line = HorizontalLineBlock()
    vertical_space = VerticalSpaceBlock()


class TwoColumnBlock(StructBlock):
    left_column = ContentStreamBlock()
    right_column = ContentStreamBlock()

    class Meta:
        template = 'frontend/blocks/two_column_block.html'
        icon = 'grip'


class ThreeColumnBlock(TwoColumnBlock):
    middle_column = ContentStreamBlock()

    class Meta:
        template = 'frontend/blocks/three_column_block.html'
        icon = 'grip'


class PageStreamBlock(ContentStreamBlock):
    two_columns = TwoColumnBlock()
    three_columns = ThreeColumnBlock()
