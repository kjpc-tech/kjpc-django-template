from django.db import models

from wagtail.admin.edit_handlers import FieldPanel, StreamFieldPanel
from wagtail.core.fields import StreamField
from wagtail.core.models import Page

from frontend.blocks import PageStreamBlock


class BasicPage(Page):
    heading = models.CharField(
        max_length=255,
        help_text='Main heading.',
    )

    body = StreamField(PageStreamBlock())

    content_panels = Page.content_panels + [
        FieldPanel('heading'),
        StreamFieldPanel('body'),
    ]
