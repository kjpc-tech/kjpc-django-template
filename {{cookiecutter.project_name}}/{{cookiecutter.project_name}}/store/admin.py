import re

from django import forms
from django.contrib import admin
from django.forms.models import BaseInlineFormSet

from mptt.admin import MPTTModelAdmin
from orderable.admin import OrderableAdmin, OrderableTabularInline

from .models import (
    Category, ProductAttribute, Product, ProductImage,
    ProductVariant, Address, Order, OrderItem, Payment,
)


class CategoryAdminMixin:
    list_display = ['slug', 'name']
    search_fields = ['name']


@admin.register(Category)
class CategoryAdmin(CategoryAdminMixin, MPTTModelAdmin):
    list_editable = ['name']
    prepopulated_fields = {
        'slug': ('name',),
    }


class ProductAttributeAdminMixin:
    list_display = ['__str__', 'name']
    search_fields = ['name']


@admin.register(ProductAttribute)
class ProductAttributeAdmin(ProductAttributeAdminMixin, admin.ModelAdmin):
    list_editable = ['name']


class ProductImageInline(OrderableTabularInline):
    model = ProductImage
    readonly_fields = ['created', 'updated', 'deleted']
    extra = 0


class ProductVariantImagesField(forms.MultipleChoiceField):
    def valid_value(self, value):
        """
        Allow for existing choices or an unsaved ProductImage.
        """
        return super().valid_value(value) or re.search(r'images-\d+', value) is not None


class ProductVariantAdminForm(forms.ModelForm):
    images = ProductVariantImagesField(choices=[], required=False)

    class Meta:
        model = ProductVariant
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance', None)
        product = kwargs.pop('product', None)

        super().__init__(*args, **kwargs)

        if product:
            # set choices to related images
            self.fields['images'].choices = [
                (productimageObj.pk, str(productimageObj)) for productimageObj in product.images.all()
            ]
            if instance:
                # set default selected choices
                self.initial['images'] = list(instance.images.values_list('pk', flat=True))

    def save(self, commit=True, new_images=None):
        new_images = new_images or {}

        # remove `images` so it can be handled differently
        images = self.cleaned_data.pop('images', [])

        super().save(commit=commit)

        if commit:
            image_ids = []
            for image in images:
                if str(image).startswith('images-'):
                    if image in new_images:
                        image_ids.append(new_images[image])
                else:
                    image_ids.append(image)

            # save this variants images
            self.instance.images.set(image_ids)


class ProductVariantAdminFormSet(BaseInlineFormSet):
    def get_form_kwargs(self, index):
        kwargs = super().get_form_kwargs(index)

        # add product to kwargs so filtering can be applied
        kwargs['product'] = self.instance

        return kwargs

    def save(self, commit=True, new_images=None):
        if new_images:
            self._new_images = new_images

        return super().save(commit=commit)

    def save_new(self, form, commit=True):
        setattr(form.instance, self.fk.name, self.instance)
        return form.save(commit=commit, new_images=getattr(self, '_new_images', None))

    def save_existing(self, form, instance, commit=True):
        return form.save(commit=commit, new_images=getattr(self, '_new_images', None))


class ProductVariantInline(OrderableTabularInline):
    model = ProductVariant
    form = ProductVariantAdminForm
    formset = ProductVariantAdminFormSet
    readonly_fields = ['created', 'updated', 'deleted']
    extra = 0


class ProductAdminMixin:
    list_display = ['slug', 'name', 'price', 'available', 'sort_order_display']
    search_fields = ['name']


@admin.register(Product)
class ProductAdmin(ProductAdminMixin, OrderableAdmin):
    list_editable = ['name', 'price', 'available']
    readonly_fields = ['created', 'updated', 'deleted']
    inlines = [ProductImageInline, ProductVariantInline]
    prepopulated_fields = {
        'slug': ('name',),
    }
    change_form_template = 'store/product_admin_change_form.html'

    def save_formset(self, request, form, formset, change):
        if formset.prefix == 'images':
            formset.save()

            # link prefix to images
            self._new_images = {}
            for form in formset:
                self._new_images[form.prefix] = form.instance
        elif formset.prefix == 'variants':
            # use linked prefix-images
            formset.save(new_images=getattr(self, '_new_images', None))
        else:
            # handle other formsets normally
            super().save_formset(request, form, formset, change)

    def construct_change_message(self, request, form, formsets, add=False):
        """
        Ignore formsets here.
        """
        return super().construct_change_message(request, form, [], add=add)


class AddressAdminMixin:
    list_display = ['first_name', 'last_name', 'city', 'state']
    search_fields = ['first_name', 'last_name', 'user__email']


@admin.register(Address)
class AddressAdmin(AddressAdminMixin, admin.ModelAdmin):
    raw_id_fields = ['user']
    readonly_fields = ['created', 'updated', 'deleted']


class OrderItemInline(admin.StackedInline):
    model = OrderItem
    readonly_fields = ['created', 'updated', 'deleted']
    extra = 0


class PaymentInline(admin.StackedInline):
    model = Payment
    readonly_fields = ['created', 'modified', 'deleted']
    extra = 0


class OrderAdminMixin:
    list_display = ['token_id', 'status', 'get_total', 'get_tax', 'get_shipping', 'is_fully_paid']
    list_filter = ['status']
    search_fields = ['user__email', 'user__first_name', 'user__last_name', 'anonymous_user_email']


@admin.register(Order)
class OrderAdmin(OrderAdminMixin, admin.ModelAdmin):
    raw_id_fields = ['user', 'billing_address', 'shipping_address']
    readonly_fields = ['created', 'updated', 'deleted']
    inlines = [OrderItemInline, PaymentInline]
