from django.conf import settings

from satchless.cart import CartLine, Cart as BaseCart

from prices import Money

from .models import ProductVariant, Order

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')


class CartItem(CartLine):
    @classmethod
    def deserialize(cls, cart_item_data):
        """
        Create from serialized data.
        """
        product = ProductVariant.objects.get(pk=cart_item_data.get('product'))
        quantity = cart_item_data.get('quantity')
        data = cart_item_data.get('data')

        return cls(product, quantity, data)

    def serialize(self):
        """
        Convert to serialized data.
        """
        return {
            'product': self.product.pk,
            'quantity': self.quantity,
            'data': self.data,
        }

    def get_item_total(self):
        return self.get_price_per_item() * self.get_quantity()

    @property
    def is_shipping_required(self):
        return self.product.is_shipping_required

    @property
    def is_user_required(self):
        return self.product.is_user_required


class Cart(BaseCart):
    def __init__(self, items=None, order=None, data=None):
        super().__init__(items=items)

        self.order = order
        self.data = data or {}

    @classmethod
    def deserialize(cls, cart_data):
        """
        Create from serialized data.
        """
        items = []
        for cart_item_data in cart_data['items']:
            items.append(CartItem.deserialize(cart_item_data))

        order = cart_data.get('order', None)
        if order:
            order = Order.objects.get(pk=order)

        data = cart_data.get('data', {})

        return cls(items, order, data)

    def serialize(self):
        """
        Convert to serialized data.
        """
        return {
            'items': [item.serialize() for item in self],
            'order': self.order.pk if self.order is not None else None,
            'data': self.data,
        }

    def clear(self, cancel_order=True):
        super().clear()

        self.clear_order(cancel_order=cancel_order)

    def clear_order(self, cancel_order=True):
        if self.order is not None and cancel_order:
            self.order.cancel_if_applicable()

        self.order = None

    def create_line(self, product, quantity, data):
        """
        Override to use our CartItem.
        """
        return CartItem(product, quantity, data=data)

    def get_cart_total(self):
        total = Money(0, currency=CURRENCY)

        for item in self:
            total += item.get_item_total()

        return total

    @property
    def is_shipping_required(self):
        return any([item.is_shipping_required for item in self])

    @property
    def is_user_required(self):
        return any([item.is_user_required for item in self])
