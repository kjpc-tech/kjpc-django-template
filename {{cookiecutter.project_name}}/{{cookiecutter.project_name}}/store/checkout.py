from django.urls import reverse

from satchless.process import InvalidData, ProcessManager, Step

from .models import Order


class CheckoutStep(Step):
    def __init__(self, cart):
        self.cart = cart


class CheckoutStartStep(CheckoutStep):
    def __str__(self):
        return 'start'

    def validate(self):
        """
        Cart must have item(s) to start checkout.
        """
        if self.cart.count() == 0:
            raise InvalidData('Cart must have an item to start the checkout process.')

    def get_absolute_url(self):
        return reverse('store:checkout-start')


class CheckoutUserStep(CheckoutStep):
    def __str__(self):
        return 'user'

    def validate(self):
        """
        A user may need to be logged in.
        """
        if self.cart.is_user_required and not self.cart.data.get('user'):
            raise InvalidData('Authenticated user required.')

    def get_absolute_url(self):
        return reverse('store:checkout-user')


class CheckoutContactStep(CheckoutStep):
    def __str__(self):
        return 'contact'

    def validate(self):
        """
        At this point, we need an order and some contact information.
        """
        if not self.cart.order:
            raise InvalidData('Cart.order cannot be None.')

        if self.cart.order.status == Order.STATUS_CANCELLED:
            raise InvalidData('Order cannot be cancelled.')

        if not self.cart.order.billing_address:
            raise InvalidData('Billing address required.')

        if self.cart.order.is_shipping_required and not self.cart.order.shipping_address:
            raise InvalidData('Shipping address required.')

        if self.cart.order.is_user_required and not self.cart.order.user:
            raise InvalidData('Authenticated user required.')

        if not self.cart.order.get_user_email:
            raise InvalidData('Email address required.')

    def get_absolute_url(self):
        return reverse('store:checkout-contact')


class CheckoutReviewStep(CheckoutStep):
    def __str__(self):
        return 'review'

    def validate(self):
        """
        At this point, the customer may cancel or continue the order.
        """
        if not self.cart.order:
            raise InvalidData('Cart.order cannot be None.')

        if self.cart.order.status != Order.STATUS_REVIEWED:
            raise InvalidData('Order needs reviewed by customer.')

    def get_absolute_url(self):
        return reverse('store:checkout-review')


class CheckoutPaymentStep(CheckoutStep):
    def __str__(self):
        return 'payment'

    def validate(self):
        """
        If continuing the order, the customer now pays for it.
        """
        if not self.cart.order:
            raise InvalidData('Cart.order cannot be None.')

        if not self.cart.order.is_fully_paid():
            raise InvalidData('Order not fully paid.')

    def get_absolute_url(self):
        return reverse('store:checkout-review')


class Checkout(ProcessManager):
    def __init__(self, cart):
        self.cart = cart

    def __iter__(self):
        for step in [
            CheckoutStartStep(self.cart),
            CheckoutUserStep(self.cart),
            CheckoutContactStep(self.cart),
            CheckoutReviewStep(self.cart),
            CheckoutPaymentStep(self.cart),
        ]:
            yield step
