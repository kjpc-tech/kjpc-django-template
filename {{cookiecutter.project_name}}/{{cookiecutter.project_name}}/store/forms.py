from django import forms
from django.conf import settings
from django.db.models import Min, Max, Q

from django_prices.forms import MoneyField

from localflavor.us.forms import USStateField, USZipCodeField

from phonenumber_field.formfields import PhoneNumberField

from .models import Category, ProductAttribute, ProductVariant, Address

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')


class ProductFilterForm(forms.Form):
    categories = forms.ModelMultipleChoiceField(queryset=Category.tree.all(), required=False)
    attributes = forms.ModelMultipleChoiceField(queryset=ProductAttribute.objects.all(), required=False)
    price_low = MoneyField(currency=CURRENCY, required=False)
    price_high = MoneyField(currency=CURRENCY, required=False)
    search = forms.CharField(max_length=100, required=False)

    class Media:
        js = (
            'store/js/product_filter_form.js',
        )

    def clean(self):
        cleaned_data = super().clean()

        # make sure price_low <= price_high
        price_low = cleaned_data.get('price_low')
        price_high = cleaned_data.get('price_high')
        if price_low and price_high and price_low > price_high:
            cleaned_data['price_high'] = None

        return cleaned_data

    def get_categories_roots(self):
        return Category.tree.root_nodes()

    def has_filters(self):
        return hasattr(self, 'cleaned_data') and (
            self.cleaned_data.get('categories') or
            self.cleaned_data.get('attributes') or
            self.cleaned_data.get('price_low') or
            self.cleaned_data.get('price_high') or
            self.cleaned_data.get('search')
        )

    def filter_queryset(self, product_queryset):
        if hasattr(self, 'cleaned_data'):
            # apply filters
            filter_categories = self.cleaned_data.get('categories')
            if filter_categories:
                product_queryset = product_queryset.filter(
                    categories__in=filter_categories,
                )

            filter_attributes = self.cleaned_data.get('attributes')
            if filter_attributes:
                product_queryset = product_queryset.filter(
                    attributes__in=filter_attributes,
                )

            filter_price_low = self.cleaned_data.get('price_low')
            filter_price_high = self.cleaned_data.get('price_high')
            if filter_price_low or filter_price_high:
                product_queryset = product_queryset.annotate(
                    variant_price_min=Min('variants__price_override'),
                    variant_price_max=Max('variants__price_override'),
                )

                if filter_price_low:
                    product_queryset = product_queryset.filter(
                        Q(price__gte=filter_price_low) | Q(variant_price_max__gte=filter_price_low),
                    )

                if filter_price_high:
                    product_queryset = product_queryset.filter(
                        Q(price__lte=filter_price_high) | Q(variant_price_min__lte=filter_price_high),
                    )

            filter_search = self.cleaned_data.get('search')
            if filter_search:
                product_queryset = product_queryset.filter(
                    Q(name__icontains=filter_search) | Q(description__icontains=filter_search),
                )

        return product_queryset


class CartAddForm(forms.Form):
    variant = forms.ModelChoiceField(queryset=ProductVariant.objects.all())
    quantity = forms.IntegerField(min_value=1, max_value=999)

    def clean(self):
        cleaned_data = super().clean()

        if 'variant' in cleaned_data:
            variant = cleaned_data.get('variant')

            if not variant.is_available:
                raise forms.ValidationError({'variant': 'This item is currently unavailable.'})

            if 'quantity' in cleaned_data:
                if variant.singular and cleaned_data.get('quantity') > 1:
                    cleaned_data['quantity'] = 1

        return cleaned_data


class CartUpdateForm(forms.Form):
    variant = forms.ModelChoiceField(queryset=ProductVariant.objects.all())
    quantity = forms.IntegerField(min_value=0, max_value=999)

    def clean(self):
        cleaned_data = super().clean()

        if 'variant' in cleaned_data:
            variant = cleaned_data.get('variant')

            if not variant.is_available:
                raise forms.ValidationError({'variant': 'This item is currently unavailable.'})

            if 'quantity' in cleaned_data:
                if variant.singular and cleaned_data.get('quantity') > 1:
                    raise forms.ValidationError({'quantity': 'Only one of this item can be purchased.'})

        return cleaned_data


class CheckoutContactForm(forms.Form):
    billing_first_name = forms.CharField(label='First name', max_length=200)
    billing_last_name = forms.CharField(label='Last name', max_length=200)
    billing_email = forms.EmailField(label='Email')
    billing_address1 = forms.CharField(label='Address', max_length=256)
    billing_address2 = forms.CharField(label='Address 2', max_length=256, required=False)
    billing_city = forms.CharField(label='City', max_length=200)
    billing_state = USStateField(label='State')
    billing_zip_code = USZipCodeField(label='Zip code')
    billing_phone = PhoneNumberField(label='Phone', help_text='xxx-xxx-xxxx')

    class Media:
        js = (
            'store/js/checkout_contact_form.js',
        )

    def __init__(self, *args, **kwargs):
        userObj = kwargs.pop('user', None)
        shipping_required = kwargs.pop('shipping_required', False)

        # handle anonymous users as None
        if userObj is not None and userObj.is_anonymous:
            userObj = None

        super().__init__(*args, **kwargs)

        self.userObj = userObj
        self.shipping_required = shipping_required

        existing_address = None

        if self.userObj is not None:
            existing_address = self.userObj.addresses.last()

            self.fields['billing_email'].initial = self.userObj.email

            if existing_address:  # default to existing address
                self.fields['billing_first_name'].initial = existing_address.first_name or self.userObj.first_name
                self.fields['billing_last_name'].initial = existing_address.last_name or self.userObj.last_name
                self.fields['billing_address1'].initial = existing_address.address1
                self.fields['billing_address2'].initial = existing_address.address2
                self.fields['billing_city'].initial = existing_address.city
                self.fields['billing_state'].initial = existing_address.state
                self.fields['billing_zip_code'].initial = existing_address.zip_code
                self.fields['billing_phone'].initial = existing_address.phone
            else:
                self.fields['billing_first_name'].initial = self.userObj.first_name
                self.fields['billing_last_name'].initial = self.userObj.last_name

        # add shipping fields if needed
        if self.shipping_required:
            self.fields['use_same_contacts'] = forms.BooleanField(label='Same as Billing', required=False)

            self.fields['shipping_first_name'] = forms.CharField(label='First name', max_length=200, required=False)
            self.fields['shipping_last_name'] = forms.CharField(label='Last name', max_length=200, required=False)
            self.fields['shipping_email'] = forms.EmailField(label='Email', required=False)
            self.fields['shipping_address1'] = forms.CharField(label='Address', max_length=256, required=False)
            self.fields['shipping_address2'] = forms.CharField(label='Address 2', max_length=256, required=False)
            self.fields['shipping_city'] = forms.CharField(label='City', max_length=200, required=False)
            self.fields['shipping_state'] = USStateField(label='State', required=False)
            self.fields['shipping_zip_code'] = USZipCodeField(label='Zip code', required=False)
            self.fields['shipping_phone'] = PhoneNumberField(label='Phone', required=False, help_text='xxx-xxx-xxxx')

            if self.userObj is not None:
                self.fields['shipping_email'].initial = self.userObj.email

                if existing_address:  # default to existing address
                    self.fields['use_same_contacts'].initial = True
                    self.fields['shipping_first_name'].initial = existing_address.first_name or self.userObj.first_name
                    self.fields['shipping_last_name'].initial = existing_address.last_name or self.userObj.last_name
                    self.fields['shipping_address1'].initial = existing_address.address1
                    self.fields['shipping_address2'].initial = existing_address.address2
                    self.fields['shipping_city'].initial = existing_address.city
                    self.fields['shipping_state'].initial = existing_address.state
                    self.fields['shipping_zip_code'].initial = existing_address.zip_code
                    self.fields['shipping_phone'].initial = existing_address.phone
                else:
                    self.fields['shipping_first_name'].initial = self.userObj.first_name
                    self.fields['shipping_last_name'].initial = self.userObj.last_name

    def clean(self):
        cleaned_data = super().clean()

        if self.shipping_required:
            if cleaned_data.get('use_same_contacts', False):
                # shipping fields not reqiured if using same contacts
                cleaned_data['shipping_first_name'] = cleaned_data.get('billing_first_name')
                cleaned_data['shipping_last_name'] = cleaned_data.get('billing_last_name')
                cleaned_data['shipping_email'] = cleaned_data.get('billing_email')
                cleaned_data['shipping_address1'] = cleaned_data.get('billing_address1')
                cleaned_data['shipping_address2'] = cleaned_data.get('billing_address2')
                cleaned_data['shipping_city'] = cleaned_data.get('billing_city')
                cleaned_data['shipping_state'] = cleaned_data.get('billing_state')
                cleaned_data['shipping_zip_code'] = cleaned_data.get('billing_zip_code')
                cleaned_data['shipping_phone'] = cleaned_data.get('billing_phone')
            else:
                # shipping fields reqiured if using differenct contacts
                shipping_errors = {}

                for shipping_field in [
                    'shipping_first_name',
                    'shipping_last_name',
                    'shipping_email',
                    'shipping_address1',
                    'shipping_city',
                    'shipping_state',
                    'shipping_zip_code',
                    'shipping_phone',
                ]:
                    if not cleaned_data.get(shipping_field):
                        shipping_errors[shipping_field] = 'This field is required.'

                if len(shipping_errors.keys()):
                    raise forms.ValidationError(shipping_errors)

        return cleaned_data

    def _get_address(self, prefix):
        if prefix not in ['billing', 'shipping']:
            raise Exception('CheckoutContactForm._get_address must have prefix `billing` or `shipping`.')

        return Address.objects.get_or_create(
            first_name=self.cleaned_data.get(f'{prefix}_first_name'),
            last_name=self.cleaned_data.get(f'{prefix}_last_name'),
            address1=self.cleaned_data.get(f'{prefix}_address1'),
            address2=self.cleaned_data.get(f'{prefix}_address2'),
            city=self.cleaned_data.get(f'{prefix}_city'),
            state=self.cleaned_data.get(f'{prefix}_state'),
            zip_code=self.cleaned_data.get(f'{prefix}_zip_code'),
            phone=self.cleaned_data.get(f'{prefix}_phone'),
            user=self.userObj,
        )[0]

    def get_billing_address(self):
        if not hasattr(self, 'cleaned_data'):
            raise Exception('CheckoutContactForm must be validated before calling get_billing_address.')

        return self._get_address('billing')

    def get_shipping_address(self):
        if not hasattr(self, 'cleaned_data'):
            raise Exception('CheckoutContactForm must be validated before calling get_shipping_address.')

        if self.shipping_required:
            return self._get_address('shipping')
        else:
            return None
