import logging

from .cart import Cart

LOGGER_STORE = logging.getLogger('{{ cookiecutter.project_name }}.store')


class StoreMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # grab cart from session
        request.store_cart = self._get_cart(request)

        response = self.get_response(request)

        # save cart to session
        self._save_cart(request)

        return response

    def _get_cart(self, request):
        existing_cart = request.session.get('store_cart', None)

        _cart = None

        if existing_cart is not None:
            try:
                _cart = Cart.deserialize(existing_cart)  # existing cart
            except Exception as e:
                LOGGER_STORE.error(f'StoreMiddleware._get_cart: {e}')

        return _cart or Cart()

    def _save_cart(self, request):
        if request.store_cart.modified:
            request.store_cart.modified = False

            try:
                request.session['store_cart'] = request.store_cart.serialize() if request.store_cart is not None else None
            except Exception as e:
                LOGGER_STORE.error(f'StoreMiddleware._save_cart: {e}')
