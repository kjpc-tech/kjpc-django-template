from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager
import django_prices.models
import localflavor.us.models
import mptt.fields
import {{ cookiecutter.project_name }}.models_abstract
import phonenumber_field.modelfields
import satchless.item
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('address1', models.CharField(max_length=256)),
                ('address2', models.CharField(blank=True, max_length=256)),
                ('city', models.CharField(max_length=200)),
                ('state', localflavor.us.models.USStateField(max_length=2)),
                ('zip_code', localflavor.us.models.USZipCodeField(max_length=10)),
                ('phone', phonenumber_field.modelfields.PhoneNumberField(max_length=128)),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='addresses', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name_plural': 'Addresses',
            },
            bases=({{ cookiecutter.project_name }}.models_abstract.AbstractSoftModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=100, unique=True)),
                ('description', models.TextField(blank=True)),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
                ('parent', mptt.fields.TreeForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='store.Category')),
            ],
            options={
                'verbose_name_plural': 'Categories',
                'ordering': ['name'],
            },
            managers=[
                ('tree', django.db.models.manager.Manager()),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('status', models.PositiveSmallIntegerField(choices=[(1, 'Pending'), (2, 'Cancelled'), (3, 'Processing'), (4, 'Paid'), (5, 'Payment Error'), (6, 'Shipped'), (7, 'Completed')], default=1)),
                ('status_changed_at', models.DateTimeField(auto_now_add=True)),
                ('anonymous_user_email', models.EmailField(blank=True, max_length=254)),
                ('total_price', django_prices.models.MoneyField(blank=True, currency='USD', decimal_places=2, max_digits=8, null=True)),
                ('total_tax', django_prices.models.MoneyField(blank=True, currency='USD', decimal_places=2, max_digits=6, null=True)),
                ('total_shipping', django_prices.models.MoneyField(blank=True, currency='USD', decimal_places=2, max_digits=6, null=True)),
                ('last_email_sent_at', models.DateTimeField(blank=True, null=True)),
                ('token_id', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('token_created', models.DateTimeField(auto_now_add=True)),
                ('billing_address', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='+', to='store.Address')),
                ('shipping_address', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='store.Address')),
                ('user', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='orders', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['-status_changed_at'],
            },
            bases=({{ cookiecutter.project_name }}.models_abstract.AbstractSoftModelMixin, models.Model, satchless.item.ItemSet),
        ),
        migrations.CreateModel(
            name='OrderItem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('product_name', models.CharField(max_length=100)),
                ('product_sku', models.CharField(blank=True, max_length=32)),
                ('quantity', models.PositiveSmallIntegerField()),
                ('unit_price', django_prices.models.MoneyField(currency='USD', decimal_places=2, max_digits=8)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='store.Order')),
            ],
            options={
                'ordering': ['pk'],
            },
            bases=({{ cookiecutter.project_name }}.models_abstract.AbstractSoftModelMixin, models.Model, satchless.item.ItemLine),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('variant', models.CharField(max_length=255)),
                ('status', models.CharField(choices=[('waiting', 'Waiting for confirmation'), ('preauth', 'Pre-authorized'), ('confirmed', 'Confirmed'), ('rejected', 'Rejected'), ('refunded', 'Refunded'), ('error', 'Error'), ('input', 'Input')], default='waiting', max_length=10)),
                ('fraud_status', models.CharField(choices=[('unknown', 'Unknown'), ('accept', 'Passed'), ('reject', 'Rejected'), ('review', 'Review')], default='unknown', max_length=10, verbose_name='fraud check')),
                ('fraud_message', models.TextField(blank=True, default='')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('transaction_id', models.CharField(blank=True, max_length=255)),
                ('currency', models.CharField(max_length=10)),
                ('total', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('delivery', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('tax', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('description', models.TextField(blank=True, default='')),
                ('billing_first_name', models.CharField(blank=True, max_length=256)),
                ('billing_last_name', models.CharField(blank=True, max_length=256)),
                ('billing_address_1', models.CharField(blank=True, max_length=256)),
                ('billing_address_2', models.CharField(blank=True, max_length=256)),
                ('billing_city', models.CharField(blank=True, max_length=256)),
                ('billing_postcode', models.CharField(blank=True, max_length=256)),
                ('billing_country_code', models.CharField(blank=True, max_length=2)),
                ('billing_country_area', models.CharField(blank=True, max_length=256)),
                ('billing_email', models.EmailField(blank=True, max_length=254)),
                ('customer_ip_address', models.GenericIPAddressField(blank=True, null=True)),
                ('extra_data', models.TextField(blank=True, default='')),
                ('message', models.TextField(blank=True, default='')),
                ('token', models.CharField(blank=True, default='', max_length=36)),
                ('captured_amount', models.DecimalField(decimal_places=2, default='0.0', max_digits=9)),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='payments', to='store.Order')),
            ],
            options={
                'ordering': ['-pk'],
            },
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, db_index=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('name', models.CharField(max_length=100)),
                ('slug', models.SlugField(max_length=100, unique=True)),
                ('description', models.TextField()),
                ('price', django_prices.models.MoneyField(currency='USD', decimal_places=2, max_digits=8)),
                ('weight', models.DecimalField(blank=True, decimal_places=2, help_text='lbs', max_digits=6, null=True)),
                ('available', models.BooleanField(default=True)),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
            bases=({{ cookiecutter.project_name }}.models_abstract.AbstractSoftModelMixin, models.Model, satchless.item.ItemRange),
        ),
        migrations.CreateModel(
            name='ProductAttribute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100)),
            ],
            options={
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, db_index=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('image', models.ImageField(upload_to='products')),
                ('alternative_text', models.CharField(blank=True, max_length=100)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='store.Product')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
            bases=({{ cookiecutter.project_name }}.models_abstract.AbstractSoftModelMixin, models.Model),
        ),
        migrations.CreateModel(
            name='ProductVariant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sort_order', models.IntegerField(blank=True, db_index=True)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('deleted', models.DateTimeField(blank=True, null=True)),
                ('name', models.CharField(blank=True, max_length=100)),
                ('sku', models.CharField(blank=True, max_length=32, null=True, unique=True)),
                ('price_override', django_prices.models.MoneyField(blank=True, currency='USD', decimal_places=2, max_digits=8, null=True)),
                ('weight_override', models.DecimalField(blank=True, decimal_places=2, help_text='lbs', max_digits=6, null=True)),
                ('available', models.BooleanField(default=True, help_text='Is this product currently available?')),
                ('shipped', models.BooleanField(default=True, help_text='Does this product require shipping?')),
                ('possessed', models.BooleanField(default=False, help_text='Does this product require an authenticated user?')),
                ('singular', models.BooleanField(default=False, help_text='Do you only buy one of this product?')),
                ('images', models.ManyToManyField(blank=True, to='store.ProductImage')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='variants', to='store.Product')),
            ],
            options={
                'ordering': ['sort_order'],
                'abstract': False,
            },
            bases=({{ cookiecutter.project_name }}.models_abstract.AbstractSoftModelMixin, models.Model, satchless.item.Item),
        ),
        migrations.AddField(
            model_name='product',
            name='attributes',
            field=models.ManyToManyField(blank=True, related_name='products', to='store.ProductAttribute'),
        ),
        migrations.AddField(
            model_name='product',
            name='categories',
            field=models.ManyToManyField(related_name='products', to='store.Category'),
        ),
        migrations.AddField(
            model_name='orderitem',
            name='product',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='store.Product'),
        ),
        migrations.AddField(
            model_name='orderitem',
            name='variant',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='store.ProductVariant'),
        ),
    ]
