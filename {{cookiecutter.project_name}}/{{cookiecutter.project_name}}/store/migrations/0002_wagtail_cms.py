from django.db import migrations
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderitem',
            name='order',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='items', to='store.Order'),
        ),
        migrations.AlterField(
            model_name='payment',
            name='order',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='payments', to='store.Order'),
        ),
        migrations.AlterField(
            model_name='productimage',
            name='product',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='images', to='store.Product'),
        ),
        migrations.AlterField(
            model_name='productvariant',
            name='product',
            field=modelcluster.fields.ParentalKey(on_delete=django.db.models.deletion.CASCADE, related_name='variants', to='store.Product'),
        ),
    ]
