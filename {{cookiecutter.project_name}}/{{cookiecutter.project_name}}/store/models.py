import datetime
import logging
import uuid

from decimal import Decimal

from django.conf import settings
from django.core.mail import send_mail
from django.db import models, transaction
from django.db.models.signals import pre_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django.utils.functional import cached_property
from django.utils.html import escape, format_html, mark_safe

from django_prices.models import MoneyField

from localflavor.us.models import USStateField, USZipCodeField

{% if cookiecutter.use_wagtail == 'y' %}from modelcluster.fields import ParentalKey{% endif %}

from mptt.managers import TreeManager
from mptt.models import MPTTModel, TreeForeignKey

from payments import PaymentStatus, PurchasedItem
from payments.models import BasePayment

from phonenumber_field.modelfields import PhoneNumberField

from prices import Money

from satchless.item import Item, ItemRange, ItemLine, ItemSet

from {{ cookiecutter.project_name }}.models_abstract import (
    AbstractSoftModel, SoftModelManager,
)

from .models_abstract import AbstractOrderableSoftModel
{% if cookiecutter.use_wagtail == 'y' %}from .wagtail_models import (
    CategoryCMSMixin, ProductAttributeCMSMixin, ProductCMSModel,
    ProductImageCMSModel, ProductVariantCMSModel, AddressCMSMixin,
    OrderCMSModel, OrderItemCMSMixin, PaymentCMSMixin,
){% endif %}

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')

LOGGER_STORE = logging.getLogger('{{ cookiecutter.project_name }}.store')


class Category({% if cookiecutter.use_wagtail == 'y' %}CategoryCMSMixin, {% endif %}MPTTModel):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    description = models.TextField(blank=True)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    tree = TreeManager()

    class Meta:
        ordering = ['name']
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name


class ProductAttribute({% if cookiecutter.use_wagtail == 'y' %}ProductAttributeCMSMixin, {% endif %}models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Product({% if cookiecutter.use_wagtail == 'y' %}ProductCMSModel{% else %}AbstractOrderableSoftModel{% endif %}, ItemRange):
    name = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    description = models.TextField()
    categories = models.ManyToManyField(Category, related_name='products')
    price = MoneyField(currency=CURRENCY, max_digits=8, decimal_places=2)
    weight = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True, help_text='lbs')
    attributes = models.ManyToManyField(ProductAttribute, related_name='products', blank=True)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    def __iter__(self):
        return iter(self.variants.all())

    def get_absolute_url(self):
        return reverse('store:product-detail', kwargs={'product': self.slug})

    def get_feature_image(self):
        try:
            return self.variants.first().get_feature_image()
        except Exception as e:
            LOGGER_STORE.error(f'Product.get_feature_image: {e}')

        return self.images.first()


class ProductImage({% if cookiecutter.use_wagtail == 'y' %}ProductImageCMSModel{% else %}AbstractOrderableSoftModel{% endif %}):
    product = {% if cookiecutter.use_wagtail == 'y' %}ParentalKey{% else %}models.ForeignKey{% endif %}(Product, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to='products')
    alternative_text = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return f'Image: {self.alternative_text or self.product}'


class ProductVariant({% if cookiecutter.use_wagtail == 'y' %}ProductVariantCMSModel{% else %}AbstractOrderableSoftModel{% endif %}, Item):
    name = models.CharField(max_length=100, blank=True)
    sku = models.CharField(max_length=32, unique=True, null=True, blank=True)
    price_override = MoneyField(currency=CURRENCY, max_digits=8, decimal_places=2, null=True, blank=True)
    weight_override = models.DecimalField(max_digits=6, decimal_places=2, blank=True, null=True, help_text='lbs')
    product = product = {% if cookiecutter.use_wagtail == 'y' %}ParentalKey{% else %}models.ForeignKey{% endif %}(Product, on_delete=models.CASCADE, related_name='variants')
    images = models.ManyToManyField(ProductImage, blank=True)
    available = models.BooleanField(default=True, help_text='Is this product currently available?')
    shipped = models.BooleanField(default=True, help_text='Does this product require shipping?')
    possessed = models.BooleanField(default=False, help_text='Does this product require an authenticated user?')
    singular = models.BooleanField(default=False, help_text='Do you only buy one of this product?')

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # convert blank sku to None
        if not self.sku:
            self.sku = None

        # convert blank name to Product.name
        if not self.name:
            self.name = self.product.name

        super().save(*args, **kwargs)

    def get_price_per_item(self, **kwargs):
        return self.price_override or self.product.price

    def get_feature_image(self):
        return self.images.first() or self.product.images.first()

    @property
    def is_available(self):
        return self.available and self.product.available

    @property
    def is_shipping_required(self):
        return self.shipped

    @property
    def is_user_required(self):
        return self.possessed


class Address({% if cookiecutter.use_wagtail == 'y' %}AddressCMSMixin, {% endif %}AbstractSoftModel):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    address1 = models.CharField(max_length=256)
    address2 = models.CharField(max_length=256, blank=True)
    city = models.CharField(max_length=200)
    state = USStateField()
    zip_code = USZipCodeField()
    phone = PhoneNumberField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True, related_name='addresses')

    class Meta:
        verbose_name_plural = 'Addresses'

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    @property
    def get_html_display(self):
        return format_html(
            '{} {}<br>{}{}<br>{}, {} {}<br>{}',
            self.first_name,
            self.last_name,
            self.address1,
            mark_safe(f'<br>{escape(self.address2)}') if self.address2 else '',
            self.city,
            self.state,
            self.zip_code,
            self.phone,
        )


class Order({% if cookiecutter.use_wagtail == 'y' %}OrderCMSModel{% else %}AbstractSoftModel{% endif %}, ItemSet):
    STATUS_PENDING = 1
    STATUS_CANCELLED = 2
    STATUS_REVIEWED = 3
    STATUS_PAID = 4
    STATUS_PAYMENT_ERROR = 5
    STATUS_SHIPPED = 6
    STATUS_COMPLETE = 7
    STATUS_CHOICES = (
        (STATUS_PENDING, 'Pending'),
        (STATUS_CANCELLED, 'Cancelled'),
        (STATUS_REVIEWED, 'Processing'),
        (STATUS_PAID, 'Paid'),
        (STATUS_PAYMENT_ERROR, 'Payment Error'),
        (STATUS_SHIPPED, 'Shipped'),
        (STATUS_COMPLETE, 'Completed'),
    )
    status = models.PositiveSmallIntegerField(choices=STATUS_CHOICES, default=STATUS_PENDING)
    status_changed_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True, related_name='orders')
    billing_address = models.ForeignKey(Address, on_delete=models.PROTECT, related_name='+')
    shipping_address = models.ForeignKey(Address, on_delete=models.PROTECT, null=True, blank=True, related_name='+')
    anonymous_user_email = models.EmailField(blank=True)
    total_price = MoneyField(currency=CURRENCY, max_digits=8, decimal_places=2, null=True, blank=True)
    total_tax = MoneyField(currency=CURRENCY, max_digits=6, decimal_places=2, null=True, blank=True)
    total_shipping = MoneyField(currency=CURRENCY, max_digits=6, decimal_places=2, null=True, blank=True)
    last_email_sent_at = models.DateTimeField(null=True, blank=True)
    token_id = models.UUIDField(default=uuid.uuid4, unique=True, editable=False)
    token_created = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        ordering = ['-status_changed_at']

    def __str__(self):
        return f'Order #{self.token_id}'

    def __iter__(self):
        return iter(self.items.all())

    def get_absolute_url(self):
        return reverse('store:order-detail', kwargs={'token': self.token_id})

    @classmethod
    def create_from_cart(cls, cart, email_address, billing_address, shipping_address, userObj=None):
        # handle anonymous users as None
        if userObj is not None and userObj.is_anonymous:
            userObj = None

        with transaction.atomic():
            orderObj = cls.objects.create(
                user=userObj,
                billing_address=billing_address,
                shipping_address=shipping_address,
                anonymous_user_email=email_address if userObj is None else '',
            )

            orderObj.update_order_with_cart(cart)

        return orderObj

    def can_access_by_token(self):
        return (self.token_created + datetime.timedelta(days=7)) >= timezone.now()

    @cached_property
    def get_user_email(self):
        if self.user:
            return self.user.email

        return self.anonymous_user_email

    def get_total(self, **kwargs):
        """
        Either return the stored total, or calculate it from items.
        """
        if self.total_price is not None:
            return self.total_price

        return super().get_total(**kwargs)

    def get_tax(self):
        """
        Either return the stored tax, or calculate it from items.
        """
        if self.total_tax is not None:
            return self.total_tax

        # Add tax logic here (currently no tax)
        return Money(0, currency=CURRENCY)

    def get_shipping(self):
        """
        Either return the stored shipping, or calculate it from items.
        """
        if self.total_shipping is not None:
            return self.total_shipping

        # Add shipping logic here (currently free shipping)
        if self.is_shipping_required:
            return Money(0, currency=CURRENCY)
        else:
            return Money(0, currency=CURRENCY)

    def get_grand_total(self):
        return self.get_total() + self.get_tax() + self.get_shipping()

    def is_fully_paid(self):
        total_paid = sum([payment.total if payment.variant == 'dummy' else payment.captured_amount for payment in self.payments.filter(status='confirmed')], Decimal())
        total_paid = Money(total_paid, currency=CURRENCY)
        total = self.get_grand_total()
        if total is not None:
            return total_paid >= total
        else:
            return False

    def can_user_cancel(self, userObj):
        if userObj is not None and userObj == self.user:
            return self.status in [
                Order.STATUS_PENDING,
                Order.STATUS_REVIEWED,
                Order.STATUS_PAID,
                Order.STATUS_PAYMENT_ERROR,
            ]

        return False

    def cancel_if_applicable(self):
        if self.status in [
            Order.STATUS_PENDING,
            Order.STATUS_REVIEWED,
            Order.STATUS_PAID,
            Order.STATUS_PAYMENT_ERROR,
        ]:
            with transaction.atomic():
                self.update_status(Order.STATUS_CANCELLED)

                # refund payments
                for paymentObj in self.payments.filter(status=PaymentStatus.CONFIRMED):
                    paymentObj.refund()

            return True

        return False

    def update_order_with_cart(self, cart):
        if self.status == Order.STATUS_PENDING:
            _variants = []

            for item in cart:
                _variants.append(item.product.pk)

                _quantity = item.get_quantity()
                _unit_price = item.get_price_per_item()

                orderitemObj = OrderItem.objects.get_or_create(
                    order=self,
                    product=item.product.product,
                    variant=item.product,
                    defaults={
                        'product_name': item.product.name,
                        'product_sku': item.product.sku or '',
                        'quantity': _quantity,
                        'unit_price': _unit_price,
                    }
                )[0]
                orderitemObj.quantity = _quantity
                orderitemObj.unit_price = _unit_price
                orderitemObj.save(update_fields=['quantity', 'unit_price'])

            # remove any order items that are no longer in the order
            OrderItem.objects.filter(
                order=self,
            ).exclude(
                variant__in=_variants,
            ).delete()

    @cached_property
    def is_shipping_required(self):
        return any([item.variant.is_shipping_required for item in self.items.all().select_related('variant')])

    @property
    def needs_shipped(self):
        return self.is_shipping_required and self.status == Order.STATUS_PAID

    @property
    def is_shipped(self):
        return self.status in [
            Order.STATUS_SHIPPED,
            Order.STATUS_COMPLETE,
        ]

    def ship(self):
        if self.needs_shipped:
            self.update_status(Order.STATUS_SHIPPED)

            return True
        return False

    @property
    def needs_completed(self):
        if self.is_shipping_required:
            return self.status == Order.STATUS_SHIPPED
        else:
            return self.status == Order.STATUS_PAID

    @property
    def is_complete(self):
        return self.status == Order.STATUS_COMPLETE

    def complete(self):
        if self.needs_completed:
            self.update_status(Order.STATUS_COMPLETE)

            return True
        return False

    @cached_property
    def is_user_required(self):
        return any([item.variant.is_user_required for item in self.items.all().select_related('variant')])

    def update_status(self, status):
        if not self.status == Order.STATUS_CANCELLED:
            self.status = status
            self.status_changed_at = timezone.now()
            self.save(update_fields=['status', 'status_changed_at'])

    def _can_send_email(self):
        return self.last_email_sent_at is None or (timezone.now() - self.last_email_sent_at).total_seconds() > 3600

    def send_confirmation_email(self):
        if self.is_fully_paid() and self._can_send_email():
            send_mail(
                '{{ cookiecutter.project_name }} Order Confirmed',  # subject
                f'Your order {self.token_id} has been confirmed.',  # message
                getattr(settings, 'DEFAULT_FROM_EMAIL'),  # from
                [self.get_user_email],  # to
            )

            self.last_email_sent_at = timezone.now()
            self.save(update_fields=['last_email_sent_at'])

    def send_shipped_email(self):
        if self.is_shipped and self._can_send_email():
            send_mail(
                '{{ cookiecutter.project_name }} Order Shipped',  # subject
                f'Your order {self.token_id} has been shipped.',  # message
                getattr(settings, 'DEFAULT_FROM_EMAIL'),  # from
                [self.get_user_email],  # to
            )

            self.last_email_sent_at = timezone.now()
            self.save(update_fields=['last_email_sent_at'])


class OrderItem({% if cookiecutter.use_wagtail == 'y' %}OrderItemCMSMixin, {% endif %}AbstractSoftModel, ItemLine):
    order = product = {% if cookiecutter.use_wagtail == 'y' %}ParentalKey{% else %}models.ForeignKey{% endif %}(Order, on_delete=models.CASCADE, related_name='items')
    product = models.ForeignKey(Product, on_delete=models.SET_NULL, null=True, blank=True, related_name='+')
    variant = models.ForeignKey(ProductVariant, on_delete=models.SET_NULL, null=True, blank=True, related_name='+')
    product_name = models.CharField(max_length=100)
    product_sku = models.CharField(max_length=32, blank=True)
    quantity = models.PositiveSmallIntegerField()
    unit_price = MoneyField(currency=CURRENCY, max_digits=8, decimal_places=2)

    class Meta:
        ordering = ['pk']

    def __str__(self):
        return self.product_name

    def get_quantity(self):
        return self.quantity

    def get_price_per_item(self):
        return self.unit_price


class Payment({% if cookiecutter.use_wagtail == 'y' %}PaymentCMSMixin, {% endif %}BasePayment):
    order = product = {% if cookiecutter.use_wagtail == 'y' %}ParentalKey{% else %}models.ForeignKey{% endif %}(Order, on_delete=models.CASCADE, related_name='payments')

    # already has `created` and `modified` fields
    deleted = models.DateTimeField(blank=True, null=True)

    objects = SoftModelManager()

    class Meta:
        ordering = ['-pk']

    def __str__(self):
        return self.token

    def get_failure_url(self):
        return reverse('store:checkout-payment-failed')

    def get_success_url(self):
        return reverse('store:checkout-payment-success')

    def get_purchased_items(self):
        for orderitemObj in self.order:
            yield PurchasedItem(
                name=orderitemObj.product_name,
                sku=orderitemObj.product_sku,
                quantity=orderitemObj.get_quantity(),
                price=orderitemObj.get_price_per_item(),
                currency=CURRENCY,
            )


@receiver(pre_delete, sender=ProductImage)
def remove_product_image_files(sender, instance, **kwargs):
    """
    Remove the ProductImage.image file from disk when deleting.
    """
    try:
        instance.image.delete(save=False)
    except:
        pass
