from django.db import models

from orderable.managers import OrderableManager
from orderable.models import Orderable

from {{ cookiecutter.project_name }}.models_abstract import (
    AbstractSoftModelMixin, SoftModelManagerMixin,
)


class OrderableSoftModelManager(SoftModelManagerMixin, OrderableManager):
    pass


class AbstractOrderableSoftModel(AbstractSoftModelMixin, Orderable):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(blank=True, null=True)

    objects = OrderableSoftModelManager()

    class Meta(Orderable.Meta):
        abstract = True
