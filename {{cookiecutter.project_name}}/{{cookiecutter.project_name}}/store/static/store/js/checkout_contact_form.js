var _contact_form_sync_contacts = false;

function _contact_form_sync_input(billing_input) {
  $('input[name=' + $(billing_input).attr('name').replace('billing_', 'shipping_') + ']').val($(billing_input).val());
}

function _contact_form_toggle() {
  if ($('input[name=use_same_contacts]').prop('checked')) {
    // populate shipping fields with billing values
    $('input[name^=billing_]').each(function() {
      _contact_form_sync_input(this);
    });

    // disable shipping inputs
    $('input[name^=shipping_]').prop('disabled', true);

    // start syncing
    _contact_form_sync_contacts = true;
  } else {
    // enable shipping inputs
    $('input[name^=shipping_]').prop('disabled', false);

    // stop syncing
    _contact_form_sync_contacts = false;
  }
}

$(document).ready(function() {
  $('input[name=use_same_contacts]').click(function() {
    _contact_form_toggle();
  });

  $('input[name^=billing_]').change(function() {
    if (_contact_form_sync_contacts) {
      _contact_form_sync_input(this);
    }
  });

  _contact_form_toggle();
});
