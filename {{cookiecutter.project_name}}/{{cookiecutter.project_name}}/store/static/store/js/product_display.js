$(document).ready(function() {
  // images (on product detail page)
  $('.product-image-small').click(function() {
    var product_feature_image = $('.product-image-feature[data-product=' + $(this).data('product') + ']');
    product_feature_image.attr('src', $(this).data('large-image-url'));
    product_feature_image.attr('alt', $(this).attr('alt'));
    product_feature_image.attr('title', $(this).attr('title'));
  });

  // images (on product list and detail page)
  $('select[name=variant]').change(function() {
    var product_variant_option = $(this).find('option:selected');
    var product_feature_image = $('.product-image-feature[data-product=' + $(this).data('product') + ']');
    if (product_variant_option.attr('data-feature-image-url')) {
      product_feature_image.attr('src', product_variant_option.data('feature-image-url'));
      product_feature_image.attr('alt', product_variant_option.data('feature-image-alt'));
      product_feature_image.attr('title', product_variant_option.data('feature-image-alt'));
    } else if (product_variant_option.attr('data-feature-image-id')) {
      var product_small_image = $('.product-image-small[data-image-id=' + product_variant_option.data('feature-image-id') + ']');
      product_feature_image.attr('src', product_small_image.data('large-image-url'));
      product_feature_image.attr('alt', product_small_image.attr('alt'));
      product_feature_image.attr('title', product_small_image.attr('title'));
    }
  });

  // add items to cart via AJAX
  $('form.product-add-to-cart-form').submit(function(e) {
    e.preventDefault();

    var update_cart_content_url = $(this).data('cart-update-content');

    $.ajax({
      url: $(this).attr('action'),
      method: $(this).attr('method'),
      data: $(this).serialize(),
      error: function(jqXHR, status) {
        // remove old notifications
        $('.add-to-cart-status').addClass('d-none');
      },
      success: function(data, status) {
        // remove old notifications
        $('.add-to-cart-status').addClass('d-none');

        if (data.status == 'added') {
          // add added notification
          $('.add-to-cart-status.text-success[data-product=' + data.product + ']').removeClass('d-none');

          // update cart in nav-bar
          $('.store-cart').load(update_cart_content_url);
        } else {
          // add error notification
          $('.add-to-cart-status.text-danger[data-product=' + data.product + ']').removeClass('d-none');
        }
      },
    });
  });
});
