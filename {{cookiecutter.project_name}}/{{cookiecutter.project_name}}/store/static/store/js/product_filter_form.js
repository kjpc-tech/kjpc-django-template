function product_filter_toggler_down(category_check, category_checked) {
  /*
  Check/uncheck all children of selected node.
  */
  // check or uncheck checkbox
  $(category_check).prop('checked', category_checked);

  // recurse down the tree
  $('input[name=categories][data-node-parent=' + $(category_check).data('node-id') + ']').each(function() {
    product_filter_toggler_down($(this), category_checked);
  });
}

function product_filter_toggler_up(category_check) {
  /*
  Check/uncheck all parents of selected node.
  */
  // check or uncheck checkbox (if it's not a child node)
  if ($('input[name=categories][data-node-parent=' + $(category_check).data('node-id') + ']').length) {
    $(category_check).prop('checked', $('input[name=categories][data-node-parent=' + $(category_check).data('node-id') + ']:not(:checked)').length == 0);
  }

  // recurse up the tree
  $('input[name=categories][data-node-id=' + $(category_check).data('node-parent') + ']').each(function() {
    product_filter_toggler_up($(this));
  });
}

$(document).ready(function() {
  $('input[name=categories]').click(function() {
    product_filter_toggler_down($(this), $(this).prop('checked'));
    product_filter_toggler_up($(this));
  });
});
