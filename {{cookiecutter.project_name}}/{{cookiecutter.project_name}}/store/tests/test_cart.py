from decimal import Decimal

from django.conf import settings

from prices import Money

from store.cart import CartItem, Cart

from {{ cookiecutter.project_name }}.tests.test_base import BaseTestCase
from store.tests.test_models import StoreDataTestMixin

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')


class CartItemTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))
        self.productvariantObj = self.create_productvariant('Product 1', self.productObj)

    def test_cart_item_deserialize(self):
        cartitemObj = CartItem.deserialize({
            'product': self.productvariantObj.pk,
            'quantity': 1,
            'data': None,
        })

        self.assertEqual(cartitemObj.product, self.productvariantObj)
        self.assertEqual(cartitemObj.quantity, 1)
        self.assertIsNone(cartitemObj.data)

        cartitemObj = CartItem.deserialize({
            'product': self.productvariantObj.pk,
            'quantity': 2,
            'data': {
                'test': 'something',
            },
        })

        self.assertEqual(cartitemObj.product, self.productvariantObj)
        self.assertEqual(cartitemObj.quantity, 2)
        self.assertIsNotNone(cartitemObj.data)

    def test_cart_item_serialize(self):
        cart_item_data = CartItem(self.productvariantObj, 1).serialize()

        self.assertEqual(cart_item_data['product'], self.productvariantObj.pk)
        self.assertEqual(cart_item_data['quantity'], 1)
        self.assertIsNone(cart_item_data['data'])

        cart_item_data = CartItem(self.productvariantObj, 2, data={'test': 'something'}).serialize()

        self.assertEqual(cart_item_data['product'], self.productvariantObj.pk)
        self.assertEqual(cart_item_data['quantity'], 2)
        self.assertIsNotNone(cart_item_data['data'])

    def test_cart_item_get_item_total(self):
        self.assertEqual(
            CartItem(self.productvariantObj, 1).get_item_total(),
            Money(Decimal(10.00), currency=CURRENCY)
        )

        self.assertEqual(
            CartItem(self.productvariantObj, 2).get_item_total(),
            Money(Decimal(20.00), currency=CURRENCY)
        )

    def test_cart_item_is_shipping_required(self):
        self.assertTrue(CartItem(self.productvariantObj, 1).is_shipping_required)

        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])

        self.assertFalse(CartItem(self.productvariantObj, 1).is_shipping_required)

    def test_cart_item_is_user_required(self):
        self.assertFalse(CartItem(self.productvariantObj, 1).is_user_required)

        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['possessed'])

        self.assertTrue(CartItem(self.productvariantObj, 1).is_user_required)


class CartTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))
        self.productvariantObj = self.create_productvariant('Product 1', self.productObj)
        self.addressObj = self.create_address('John', 'Doe', '1234 5th Street', 'Two Dot', 'MT', '59085', '406-555-1234')
        self.orderObj = self.create_order(self.addressObj, anonymous_user_email='john@example.com')

    def test_cart_deserialize(self):
        cartObj = Cart.deserialize({
            'items': [{
                'product': self.productvariantObj.pk,
                'quantity': 1,
                'data': None,
            }],
            'order': None,
            'data': {},
        })

        self.assertEqual(cartObj.count(), 1)
        self.assertIsNone(cartObj.order)
        self.assertEqual(len(cartObj.data.keys()), 0)

        cartObj = Cart.deserialize({
            'items': [],
            'order': self.orderObj.pk,
            'data': {
                'test': 'something',
            },
        })

        self.assertEqual(cartObj.count(), 0)
        self.assertEqual(cartObj.order, self.orderObj)
        self.assertEqual(len(cartObj.data.keys()), 1)

        cartObj = Cart.deserialize({
            'items': [{
                'product': self.productvariantObj.pk,
                'quantity': 2,
                'data': {
                    'test': 'something',
                },
            }],
            'order': self.orderObj.pk,
            'data': {},
        })

        self.assertEqual(cartObj.count(), 2)
        self.assertEqual(cartObj.order, self.orderObj)
        self.assertEqual(len(cartObj.data.keys()), 0)

    def test_cart_serialize(self):
        cart_data = Cart([CartItem(self.productvariantObj, 1)]).serialize()

        self.assertEqual(len(cart_data['items']), 1)
        self.assertIsNone(cart_data['order'])
        self.assertEqual(len(cart_data['data'].keys()), 0)

        cart_data = Cart([], self.orderObj, {'test': 'something'}).serialize()

        self.assertEqual(len(cart_data['items']), 0)
        self.assertEqual(cart_data['order'], self.orderObj.pk)
        self.assertEqual(len(cart_data['data'].keys()), 1)

        cart_data = Cart([CartItem(self.productvariantObj, 2, data={'test': 'something'})], self.orderObj, {}).serialize()

        self.assertEqual(len(cart_data['items']), 1)
        self.assertEqual(cart_data['order'], self.orderObj.pk)
        self.assertEqual(len(cart_data['data'].keys()), 0)

    def test_cart_clear(self):
        cartObj = Cart([CartItem(self.productvariantObj, 1)])

        self.assertEqual(cartObj.count(), 1)

        cartObj.clear()

        self.assertEqual(cartObj.count(), 0)

        cartObj = Cart([], self.orderObj)

        self.assertEqual(cartObj.order, self.orderObj)

        cartObj.clear()

        self.assertIsNone(cartObj.order)

        cartObj = Cart([CartItem(self.productvariantObj, 2)], self.orderObj)

        self.assertEqual(cartObj.count(), 2)
        self.assertEqual(cartObj.order, self.orderObj)

        cartObj.clear()

        self.assertEqual(cartObj.count(), 0)
        self.assertIsNone(cartObj.order)

    def test_cart_clear_order(self):
        cartObj = Cart([], self.orderObj)

        self.assertEqual(cartObj.order, self.orderObj)

        cartObj.clear_order()

        self.assertIsNone(cartObj.order)

        cartObj = Cart([CartItem(self.productvariantObj, 1)], self.orderObj)

        self.assertEqual(cartObj.count(), 1)
        self.assertEqual(cartObj.order, self.orderObj)

        cartObj.clear_order()

        self.assertEqual(cartObj.count(), 1)
        self.assertIsNone(cartObj.order)

    def test_cart_create_line(self):
        cartObj = Cart()

        cartitemObj = cartObj.create_line(self.productvariantObj, 1, None)

        self.assertIsInstance(cartitemObj, CartItem)

        self.assertEqual(cartitemObj.product, self.productvariantObj)
        self.assertEqual(cartitemObj.quantity, 1)

    def test_cart_get_cart_total(self):
        cartObj = Cart()

        self.assertEqual(cartObj.get_cart_total(), Money(0, currency=CURRENCY))

        cartObj.add(self.productvariantObj, 1)

        self.assertEqual(cartObj.get_cart_total(), Money(Decimal(10.00), currency=CURRENCY))

        cartObj.add(self.productvariantObj, 1)

        self.assertEqual(cartObj.get_cart_total(), Money(Decimal(20.00), currency=CURRENCY))

        cartObj.add(self.productvariantObj, 1, replace=True)

        self.assertEqual(cartObj.get_cart_total(), Money(Decimal(10.00), currency=CURRENCY))

        cartObj.add(self.productvariantObj, 0, replace=True)

        self.assertEqual(cartObj.get_cart_total(), Money(0, currency=CURRENCY))

    def test_cart_is_shipping_required(self):
        cartObj = Cart()

        cartObj.add(self.productvariantObj, 1)

        self.assertTrue(cartObj.is_shipping_required)

        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])

        self.assertFalse(cartObj.is_shipping_required)

    def test_cart_is_user_required(self):
        cartObj = Cart()

        cartObj.add(self.productvariantObj, 1)

        self.assertFalse(cartObj.is_user_required)

        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['possessed'])

        self.assertTrue(cartObj.is_user_required)
