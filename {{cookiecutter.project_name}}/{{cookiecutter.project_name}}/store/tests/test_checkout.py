from decimal import Decimal

from django.conf import settings

from payments import PaymentStatus

from prices import Money

from satchless.process import InvalidData

from store.models import Order
from store.cart import Cart
from store.checkout import (
    CheckoutStartStep, CheckoutUserStep, CheckoutContactStep,
    CheckoutReviewStep, CheckoutPaymentStep, Checkout,
)

from {{ cookiecutter.project_name }}.tests.test_base import BaseTestCase, UserDataMixin
from store.tests.test_models import StoreDataTestMixin

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')


class CheckoutTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    CREATE_USER_DATA = False
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.userObj = self.create_user('john@example.com', 'secret')
        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))
        self.productvariantObj = self.create_productvariant('Product 1', self.productObj)
        self.addressObj = self.create_address('John', 'Doe', '1234 5th Street', 'Two Dot', 'MT', '59085', '406-555-1234')
        self.orderObj = self.create_order(self.addressObj, anonymous_user_email='john@example.com')
        self.orderitemObj = self.create_orderitem(self.orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))
        self.cartObj = Cart()

    def test_checkout_start_step(self):
        step = CheckoutStartStep(self.cartObj)

        with self.assertRaises(InvalidData):
            step.validate()

        self.cartObj.add(self.productvariantObj, 1)

        step.validate()

    def test_checkout_user_step(self):
        step = CheckoutUserStep(self.cartObj)

        step.validate()

        self.cartObj.add(self.productvariantObj, 1)

        step.validate()

        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['possessed'])

        with self.assertRaises(InvalidData):
            step.validate()

        self.cartObj.data['user'] = self.userObj.pk

        step.validate()

    def test_checkout_contact_step(self):
        step = CheckoutContactStep(self.cartObj)

        with self.assertRaises(InvalidData):
            step.validate()

        self.cartObj.order = self.orderObj

        with self.assertRaises(InvalidData):
            step.validate()

        self.orderObj.shipping_address = self.addressObj
        self.orderObj.save(update_fields=['shipping_address'])
        del self.orderObj.is_shipping_required  # since it's cached

        step.validate()

        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['possessed'])
        del self.orderObj.is_user_required  # since it's cached

        with self.assertRaises(InvalidData):
            step.validate()

        self.orderObj.user = self.userObj
        self.orderObj.save(update_fields=['user'])

        step.validate()

    def test_checkout_review_step(self):
        step = CheckoutReviewStep(self.cartObj)

        with self.assertRaises(InvalidData):
            step.validate()

        self.cartObj.order = self.orderObj

        with self.assertRaises(InvalidData):
            step.validate()

        self.orderObj.status = Order.STATUS_REVIEWED
        self.orderObj.save(update_fields=['status'])

        step.validate()

    def test_checkout_payment_step(self):
        step = CheckoutPaymentStep(self.cartObj)

        with self.assertRaises(InvalidData):
            step.validate()

        self.cartObj.order = self.orderObj

        with self.assertRaises(InvalidData):
            step.validate()

        paymentObj = self.create_payment(self.orderObj, 'dummy', self.orderObj.token_id)
        paymentObj.status = PaymentStatus.CONFIRMED
        paymentObj.total = Decimal(10.00)
        paymentObj.captured_amount = Decimal(10.00)
        paymentObj.save(update_fields=['status', 'total', 'captured_amount'])

        step.validate()

    def test_checkout_flow_without_user_with_shipping(self):
        self.productvariantObj.shipped = True
        self.productvariantObj.possessed = False
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        checkoutObj = Checkout(self.cartObj)

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutStartStep)

        self.cartObj.add(self.productvariantObj, 1)

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutContactStep)

        self.cartObj.order = self.orderObj

        self.orderObj.shipping_address = self.addressObj
        self.orderObj.save(update_fields=['shipping_address'])

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutReviewStep)

        self.orderObj.status = Order.STATUS_REVIEWED
        self.orderObj.save(update_fields=['status'])

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutPaymentStep)

        paymentObj = self.create_payment(self.orderObj, 'dummy', self.orderObj.token_id)
        paymentObj.status = PaymentStatus.CONFIRMED
        paymentObj.total = Decimal(10.00)
        paymentObj.captured_amount = Decimal(10.00)
        paymentObj.save(update_fields=['status', 'total', 'captured_amount'])

        self.assertIsNone(checkoutObj.get_next_step())

    def test_checkout_flow_with_user_without_shipping(self):
        self.productvariantObj.shipped = False
        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        checkoutObj = Checkout(self.cartObj)

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutStartStep)

        self.cartObj.add(self.productvariantObj, 1)

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutUserStep)

        self.cartObj.data['user'] = self.userObj.pk

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutContactStep)

        self.cartObj.order = self.orderObj

        self.orderObj.user = self.userObj
        self.orderObj.save(update_fields=['user'])

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutReviewStep)

        self.orderObj.status = Order.STATUS_REVIEWED
        self.orderObj.save(update_fields=['status'])

        self.assertIsInstance(checkoutObj.get_next_step(), CheckoutPaymentStep)

        paymentObj = self.create_payment(self.orderObj, 'dummy', self.orderObj.token_id)
        paymentObj.status = PaymentStatus.CONFIRMED
        paymentObj.total = Decimal(10.00)
        paymentObj.captured_amount = Decimal(10.00)
        paymentObj.save(update_fields=['status', 'total', 'captured_amount'])

        self.assertIsNone(checkoutObj.get_next_step())
