import time
import unittest

from decimal import Decimal

from django.conf import settings
from django.core import mail
from django.test import override_settings
from django.urls import reverse
from django.utils import timezone

from prices import Money

from {{ cookiecutter.project_name }}.tests.test_base import UserDataMixin, SeleniumTestCase

from store.tests.test_models import StoreDataTestMixin

from store.models import Order

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')


class StoreDashboardFlowTests(StoreDataTestMixin, UserDataMixin, SeleniumTestCase):
    """
    Staff dashboard flow.
    """
    def setUp(self):
        super().setUp()

        self.staffObj = self.create_user('staff@example.com', 'secret', is_staff=True)

    def test_store_dashboard_ship_order_flow(self):
        self.login_user('staff@example.com', 'secret')

        self.selenium.get(f"{self.live_server_url}{reverse('store:dashboard')}")

        self.assertEqual(self.get_current_url(), reverse('store:dashboard'))

        order_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(order_table.find_elements_by_css_selector('tr')), 0)

        self.orderObj.status = Order.STATUS_PAID
        self.orderObj.save(update_fields=['status'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:dashboard')}")

        self.assertEqual(self.get_current_url(), reverse('store:dashboard'))

        order_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(order_table.find_elements_by_css_selector('tr')), 1)

        self.selenium.find_element_by_css_selector('table tbody tr.order-row td:first-child').click()

        time.sleep(0.5)

        self.assertEqual(self.selenium.find_element_by_css_selector('.modal-body h2').text, f'Order #{self.orderObj.token_id}')

        self.orderObj.refresh_from_db()
        self.assertEqual(self.orderObj.status, Order.STATUS_PAID)
        self.assertTrue(self.orderObj.is_shipping_required)
        self.assertTrue(self.orderObj.needs_shipped)
        self.assertFalse(self.orderObj.is_shipped)
        self.assertFalse(self.orderObj.needs_completed)
        self.assertFalse(self.orderObj.is_complete)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Mark as Shipped")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:dashboard'))

        self.orderObj.refresh_from_db()
        self.assertEqual(self.orderObj.status, Order.STATUS_SHIPPED)
        self.assertTrue(self.orderObj.is_shipping_required)
        self.assertFalse(self.orderObj.needs_shipped)
        self.assertTrue(self.orderObj.is_shipped)
        self.assertTrue(self.orderObj.needs_completed)
        self.assertFalse(self.orderObj.is_complete)

    def test_store_dashboard_complete_order_flow(self):
        self.login_user('staff@example.com', 'secret')

        self.selenium.get(f"{self.live_server_url}{reverse('store:dashboard')}")

        self.assertEqual(self.get_current_url(), reverse('store:dashboard'))

        order_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(order_table.find_elements_by_css_selector('tr')), 0)

        self.orderObj.status = Order.STATUS_SHIPPED
        self.orderObj.save(update_fields=['status'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:dashboard')}")

        self.assertEqual(self.get_current_url(), reverse('store:dashboard'))

        order_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(order_table.find_elements_by_css_selector('tr')), 1)

        self.selenium.find_element_by_css_selector('table tbody tr.order-row td:first-child').click()

        time.sleep(0.5)

        self.assertEqual(self.selenium.find_element_by_css_selector('.modal-body h2').text, f'Order #{self.orderObj.token_id}')

        self.orderObj.refresh_from_db()
        self.assertEqual(self.orderObj.status, Order.STATUS_SHIPPED)
        self.assertTrue(self.orderObj.is_shipping_required)
        self.assertFalse(self.orderObj.needs_shipped)
        self.assertTrue(self.orderObj.is_shipped)
        self.assertTrue(self.orderObj.needs_completed)
        self.assertFalse(self.orderObj.is_complete)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Mark as Complete")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:dashboard'))

        self.orderObj.refresh_from_db()
        self.assertEqual(self.orderObj.status, Order.STATUS_COMPLETE)
        self.assertTrue(self.orderObj.is_shipping_required)
        self.assertFalse(self.orderObj.needs_shipped)
        self.assertTrue(self.orderObj.is_shipped)
        self.assertFalse(self.orderObj.needs_completed)
        self.assertTrue(self.orderObj.is_complete)


class ShoppingFlowTests(StoreDataTestMixin, UserDataMixin, SeleniumTestCase):
    """
    Filtering, adding/updating cart.
    """
    def setUp(self):
        super().setUp()

        self.categoryObj2 = self.create_category('Another Category', 'another-category')
        self.productattributeObj2 = self.create_productattribute('Another Attribute')
        self.productObj2 = self.create_product('Another Product', 'another-product', 'Another test product.', Money(Decimal(19.99), currency=CURRENCY), categories=[self.categoryObj2], attributes=[self.productattributeObj2])
        self.productimageObj2 = self.create_productimage(self.productObj2, self.create_image(), alternative_text='Another product image.')
        self.productvariantObj2_1 = self.create_productvariant('Another Product Variant 1', self.productObj2, sku='23451', images=[self.productimageObj2])
        self.productvariantObj2_2 = self.create_productvariant('Another Product Variant 2', self.productObj2, sku='23452', price_override=Money(Decimal(18.99), currency=CURRENCY), images=[self.productimageObj2])

    def test_shopping_filters_flow(self):
        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        # both products are on the page
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj.pk}"]')
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj2.pk}"]')

        # open filter section
        self.selenium.find_element_by_css_selector('button[data-target="#product-filters-collapse"]').click()

        # select 'Another Category' category
        self.selenium.find_element_by_css_selector(f'input[name="categories"][value="{self.categoryObj2.pk}"]').click()

        # apply filters
        self.selenium.find_element_by_xpath('//button[contains(text(), "Apply Filters")]').click()

        # only one product is on the page
        self.assertElementDoesntExist(f'.product-image-feature[data-product="{self.productObj.pk}"]')
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj2.pk}"]')

        # select 'Test Attribute' attribute
        self.selenium.find_element_by_css_selector(f'input[name="attributes"][value="{self.productattributeObj.pk}"]').click()

        # apply filters
        self.selenium.find_element_by_xpath('//button[contains(text(), "Apply Filters")]').click()

        # no products are on the page
        self.assertElementDoesntExist(f'.product-image-feature[data-product="{self.productObj.pk}"]')
        self.assertElementDoesntExist(f'.product-image-feature[data-product="{self.productObj2.pk}"]')

        # unselect 'Another Category' category
        self.selenium.find_element_by_css_selector(f'input[name="categories"][value="{self.categoryObj2.pk}"]').click()

        # unselect 'Test Attribute' attribute
        self.selenium.find_element_by_css_selector(f'input[name="attributes"][value="{self.productattributeObj.pk}"]').click()

        # add high price
        self.update_input_value('price_high', '15.00')

        # apply filters
        self.selenium.find_element_by_xpath('//button[contains(text(), "Apply Filters")]').click()

        # only one product is on the page
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj.pk}"]')
        self.assertElementDoesntExist(f'.product-image-feature[data-product="{self.productObj2.pk}"]')

        # add low price
        self.update_input_value('price_low', '14.00')

        # apply filters
        self.selenium.find_element_by_xpath('//button[contains(text(), "Apply Filters")]').click()

        # no products are on the page
        self.assertElementDoesntExist(f'.product-image-feature[data-product="{self.productObj.pk}"]')
        self.assertElementDoesntExist(f'.product-image-feature[data-product="{self.productObj2.pk}"]')

        # clear prices
        self.clear_input_with_backspace('price_low')
        self.clear_input_with_backspace('price_high')

        # apply filters
        self.selenium.find_element_by_xpath('//button[contains(text(), "Apply Filters")]').click()

        # both products are on the page
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj.pk}"]')
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj2.pk}"]')

        # open filter section (it closed when there were no filters)
        self.selenium.find_element_by_css_selector('button[data-target="#product-filters-collapse"]').click()

        # add search text
        self.update_input_value('search', 'another')

        # apply filters
        self.selenium.find_element_by_xpath('//button[contains(text(), "Apply Filters")]').click()

        # only one product is on the page
        self.assertElementDoesntExist(f'.product-image-feature[data-product="{self.productObj.pk}"]')
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj2.pk}"]')

        # reset filters
        self.selenium.find_element_by_xpath('//a[contains(text(), "Reset Filters")]').click()

        # both products are on the page
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj.pk}"]')
        self.assertElementDoesExist(f'.product-image-feature[data-product="{self.productObj2.pk}"]')

    def test_shopping_add_to_cart_flow(self):
        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        store_cart = self.selenium.find_element_by_css_selector('li.store-cart')

        # cart should not be displayed at this point
        self.assertEqual(len(store_cart.find_elements_by_xpath('.//*')), 0)

        p1_status_added = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj.pk}"].text-success')
        p1_status_error = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj.pk}"].text-danger')
        p2_status_added = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj2.pk}"].text-success')
        p2_status_error = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj2.pk}"].text-danger')

        # should be no success/error indicators at this point
        for status_indicator in [
            p1_status_added,
            p1_status_error,
            p2_status_added,
            p2_status_error,
        ]:
            self.assertIn('d-none', status_indicator.get_attribute('class'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        # check success/error indicators
        for status_indicator in [
            p1_status_error,
            p2_status_added,
            p2_status_error,
        ]:
            self.assertIn('d-none', status_indicator.get_attribute('class'))
        self.assertNotIn('d-none', p1_status_added.get_attribute('class'))

        # cart should be displayed now
        self.assertGreater(len(store_cart.find_elements_by_xpath('.//*')), 0)

        self.selenium.find_element_by_css_selector(f'.product-image-feature[data-product="{self.productObj2.pk}"]').click()

        self.assertEqual(self.get_current_url(), self.productObj2.get_absolute_url())

        store_cart = self.selenium.find_element_by_css_selector('li.store-cart')

        # cart should be displayed
        self.assertGreater(len(store_cart.find_elements_by_xpath('.//*')), 0)

        p2_status_added = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj2.pk}"].text-success')
        p2_status_error = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj2.pk}"].text-danger')

        # should be no success/error indicators at this point
        for status_indicator in [
            p2_status_added,
            p2_status_error,
        ]:
            self.assertIn('d-none', status_indicator.get_attribute('class'))

        # change variant
        self.choose_select_option('variant', self.productvariantObj2_2.pk)

        # change quantity
        self.update_input_value('quantity', 3)

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj2.pk}"] button.btn-add-to-cart').click()

        # check success/error indicators
        self.assertNotIn('d-none', p2_status_added.get_attribute('class'))
        self.assertIn('d-none', p2_status_error.get_attribute('class'))

        # cart should be displayed
        self.assertGreater(len(store_cart.find_elements_by_xpath('.//*')), 0)

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        store_cart = self.selenium.find_element_by_css_selector('li.store-cart')

        # cart should be displayed
        self.assertGreater(len(store_cart.find_elements_by_xpath('.//*')), 0)

        p1_status_added = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj.pk}"].text-success')
        p1_status_error = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj.pk}"].text-danger')
        p2_status_added = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj2.pk}"].text-success')
        p2_status_error = self.selenium.find_element_by_css_selector(f'.add-to-cart-status[data-product="{self.productObj2.pk}"].text-danger')

        # should be no success/error indicators at this point
        for status_indicator in [
            p1_status_added,
            p1_status_error,
            p2_status_added,
            p2_status_error,
        ]:
            self.assertIn('d-none', status_indicator.get_attribute('class'))

        # change quantity (product 1)
        p1_quantity = self.selenium.find_element_by_css_selector(f'[name=quantity][data-product="{self.productObj.pk}"]')
        p1_quantity.clear()
        p1_quantity.send_keys(str(2))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        # check success/error indicators
        for status_indicator in [
            p1_status_error,
            p2_status_added,
            p2_status_error,
        ]:
            self.assertIn('d-none', status_indicator.get_attribute('class'))
        self.assertNotIn('d-none', p1_status_added.get_attribute('class'))

        # cart should be displayed
        self.assertGreater(len(store_cart.find_elements_by_xpath('.//*')), 0)

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj2.pk}"] button.btn-add-to-cart').click()

        # check success/error indicators
        for status_indicator in [
            p1_status_added,
            p1_status_error,
            p2_status_error,
        ]:
            self.assertIn('d-none', status_indicator.get_attribute('class'))
        self.assertNotIn('d-none', p2_status_added.get_attribute('class'))

        # cart should be displayed
        self.assertGreater(len(store_cart.find_elements_by_xpath('.//*')), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "View Cart")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        cart_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(cart_table.find_elements_by_css_selector('tr[data-variant]')), 3)

        cart_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('113.98', cart_footer.text)

    def test_shopping_update_cart_flow(self):
        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj2.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        # cart should be displayed
        self.assertGreater(len(self.selenium.find_element_by_css_selector('li.store-cart').find_elements_by_xpath('.//*')), 0)

        cart_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(cart_table.find_elements_by_css_selector('tr[data-variant]')), 2)

        cart_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('32.33', cart_footer.text)

        # change quantity (product 1 variant 1)
        p1_quantity = self.selenium.find_element_by_css_selector(f'form[data-variant="{self.productvariantObj.pk}"] input[name=quantity]')
        p1_quantity.clear()
        p1_quantity.send_keys(str(2))

        self.selenium.find_element_by_css_selector(f'form[data-variant="{self.productvariantObj.pk}"] button.btn-update-cart').click()

        cart_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(cart_table.find_elements_by_css_selector('tr[data-variant]')), 2)

        cart_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('44.67', cart_footer.text)

        # change quantity (product 1 variant 1)
        p2_quantity = self.selenium.find_element_by_css_selector(f'form[data-variant="{self.productvariantObj2_1.pk}"] input[name=quantity]')
        p2_quantity.clear()
        p2_quantity.send_keys(str(0))

        self.selenium.find_element_by_css_selector(f'form[data-variant="{self.productvariantObj2_1.pk}"] button.btn-update-cart').click()

        cart_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(cart_table.find_elements_by_css_selector('tr[data-variant]')), 1)

        cart_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('24.68', cart_footer.text)

        # cart should be displayed
        self.assertGreater(len(self.selenium.find_element_by_css_selector('li.store-cart').find_elements_by_xpath('.//*')), 0)

        # empty the cart completely
        self.selenium.find_element_by_xpath('//a[contains(text(), "Empty Cart")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        # cart should not be displayed
        self.assertEqual(len(self.selenium.find_element_by_css_selector('li.store-cart').find_elements_by_xpath('.//*')), 0)

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        cart_table = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(cart_table.find_elements_by_css_selector('tr[data-variant]')), 0)

        cart_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('0.00', cart_footer.text)

        # cart should not be displayed
        self.assertEqual(len(self.selenium.find_element_by_css_selector('li.store-cart').find_elements_by_xpath('.//*')), 0)


class CheckoutFlowTests(StoreDataTestMixin, UserDataMixin, SeleniumTestCase):
    """
    Contact info, payment, redirect.
    """
    def setUp(self):
        super().setUp()

        Order.objects.all().delete()

    @property
    def _card_expiration_year(self):
        return str(timezone.localdate().year + 1)[2:4]

    @unittest.skipUnless('dummy' in settings.PAYMENT_VARIANTS, 'Dummy provider is not configured.')
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='dummy')
    def test_checkout_without_shipping_and_without_user_flow(self):
        self.productvariantObj.shipped = False
        self.productvariantObj.possessed = False
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Proceed to Checkout")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-contact'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.update_input_value('billing_first_name', 'John')
        self.update_input_value('billing_last_name', 'Doe')
        self.update_input_value('billing_email', 'john@example.com')
        self.update_input_value('billing_address1', '1234 5th Street')
        self.update_input_value('billing_address2', '')
        self.update_input_value('billing_city', 'Two Dot')
        self.update_input_value('billing_state', 'MT')
        self.update_input_value('billing_zip_code', '59085')
        self.update_input_value('billing_phone', '406-555-1234')

        self.selenium.find_element_by_xpath('//button[contains(text(), "Proceed")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(len(mail.outbox), 0)

        orderObj = Order.objects.last()

        review_table_header = self.selenium.find_element_by_css_selector('table thead')

        self.assertIn('Email: john@example.com', review_table_header.text)
        self.assertIn(f'#{orderObj.token_id}', review_table_header.text)

        review_table_body = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(review_table_body.find_elements_by_css_selector('tr')), 1)

        review_table_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('12.34', review_table_footer.text)

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.choose_select_option('status', 'confirmed')
        self.choose_select_option('fraud_status', 'accept')
        self.choose_select_option('gateway_response', '3ds-redirect')
        self.choose_select_option('verification_result', 'confirmed')

        self.selenium.find_element_by_css_selector('#payment-form button.btn-primary').click()

        self.assertEqual(self.get_current_url(), orderObj.get_absolute_url())

        self.assertEqual(len(mail.outbox), 1)

        orderObj.refresh_from_db()

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertTrue(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.assertEqual(orderObj.status, Order.STATUS_PAID)
        self.assertFalse(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_shipped)
        self.assertFalse(orderObj.is_user_required)
        self.assertTrue(orderObj.needs_completed)

    @unittest.skipUnless('dummy' in settings.PAYMENT_VARIANTS, 'Dummy provider is not configured.')
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='dummy')
    def test_checkout_without_shipping_and_with_user_flow(self):
        self.productvariantObj.shipped = False
        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Proceed to Checkout")]').click()

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.update_input_value('username', 'test@example.com')
        self.update_input_value('password', 'secret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-contact'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.update_input_value('billing_first_name', 'John')
        self.update_input_value('billing_last_name', 'Doe')
        self.update_input_value('billing_email', 'john@example.com')
        self.update_input_value('billing_address1', '1234 5th Street')
        self.update_input_value('billing_address2', '')
        self.update_input_value('billing_city', 'Two Dot')
        self.update_input_value('billing_state', 'MT')
        self.update_input_value('billing_zip_code', '59085')
        self.update_input_value('billing_phone', '406-555-1234')

        self.selenium.find_element_by_xpath('//button[contains(text(), "Proceed")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(len(mail.outbox), 0)

        orderObj = Order.objects.last()

        review_table_header = self.selenium.find_element_by_css_selector('table thead')

        self.assertIn('Email: test@example.com', review_table_header.text)
        self.assertIn(f'#{orderObj.token_id}', review_table_header.text)

        review_table_body = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(review_table_body.find_elements_by_css_selector('tr')), 1)

        review_table_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('12.34', review_table_footer.text)

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.choose_select_option('status', 'confirmed')
        self.choose_select_option('fraud_status', 'accept')
        self.choose_select_option('gateway_response', '3ds-redirect')
        self.choose_select_option('verification_result', 'confirmed')

        self.selenium.find_element_by_css_selector('#payment-form button.btn-primary').click()

        self.assertEqual(self.get_current_url(), orderObj.get_absolute_url())

        self.assertEqual(len(mail.outbox), 1)

        orderObj.refresh_from_db()

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertTrue(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.assertEqual(orderObj.status, Order.STATUS_PAID)
        self.assertFalse(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_shipped)
        self.assertTrue(orderObj.is_user_required)
        self.assertTrue(orderObj.needs_completed)

    @unittest.skipUnless('dummy' in settings.PAYMENT_VARIANTS, 'Dummy provider is not configured.')
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='dummy')
    def test_checkout_with_shipping_and_without_user_flow_and_with_different_contacts(self):
        self.productvariantObj.shipped = True
        self.productvariantObj.possessed = False
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Proceed to Checkout")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-contact'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.update_input_value('billing_first_name', 'John')
        self.update_input_value('billing_last_name', 'Doe')
        self.update_input_value('billing_email', 'john@example.com')
        self.update_input_value('billing_address1', '1234 5th Street')
        self.update_input_value('billing_address2', '')
        self.update_input_value('billing_city', 'Two Dot')
        self.update_input_value('billing_state', 'MT')
        self.update_input_value('billing_zip_code', '59085')
        self.update_input_value('billing_phone', '406-555-1234')
        self.update_input_value('shipping_first_name', 'John')
        self.update_input_value('shipping_last_name', 'Doe')
        self.update_input_value('shipping_email', 'john@example.com')
        self.update_input_value('shipping_address1', 'PO Box 96')
        self.update_input_value('shipping_address2', '')
        self.update_input_value('shipping_city', 'Two Dot')
        self.update_input_value('shipping_state', 'MT')
        self.update_input_value('shipping_zip_code', '59085')
        self.update_input_value('shipping_phone', '406-555-1234')

        self.selenium.find_element_by_xpath('//button[contains(text(), "Proceed")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(len(mail.outbox), 0)

        orderObj = Order.objects.last()

        review_table_header = self.selenium.find_element_by_css_selector('table thead')

        self.assertIn('Email: john@example.com', review_table_header.text)
        self.assertIn(f'#{orderObj.token_id}', review_table_header.text)

        review_table_body = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(review_table_body.find_elements_by_css_selector('tr')), 1)

        review_table_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('12.34', review_table_footer.text)

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.choose_select_option('status', 'confirmed')
        self.choose_select_option('fraud_status', 'accept')
        self.choose_select_option('gateway_response', '3ds-redirect')
        self.choose_select_option('verification_result', 'confirmed')

        self.selenium.find_element_by_css_selector('#payment-form button.btn-primary').click()

        self.assertEqual(self.get_current_url(), orderObj.get_absolute_url())

        self.assertEqual(len(mail.outbox), 1)

        orderObj.refresh_from_db()

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertTrue(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.assertEqual(orderObj.status, Order.STATUS_PAID)
        self.assertTrue(orderObj.is_shipping_required)
        self.assertTrue(orderObj.needs_shipped)
        self.assertFalse(orderObj.is_user_required)
        self.assertFalse(orderObj.needs_completed)

        self.assertNotEqual(orderObj.billing_address, orderObj.shipping_address)

    @unittest.skipUnless('dummy' in settings.PAYMENT_VARIANTS, 'Dummy provider is not configured.')
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='dummy')
    def test_checkout_with_shipping_and_without_user_flow_and_with_same_contacts(self):
        self.productvariantObj.shipped = True
        self.productvariantObj.possessed = False
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Proceed to Checkout")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-contact'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.update_input_value('billing_first_name', 'John')
        self.update_input_value('billing_last_name', 'Doe')
        self.update_input_value('billing_email', 'john@example.com')
        self.update_input_value('billing_address1', '1234 5th Street')
        self.update_input_value('billing_address2', '')
        self.update_input_value('billing_city', 'Two Dot')
        self.update_input_value('billing_state', 'MT')
        self.update_input_value('billing_zip_code', '59085')
        self.update_input_value('billing_phone', '406-555-1234')
        self.selenium.find_element_by_css_selector('[name=use_same_contacts]').click()

        self.selenium.find_element_by_xpath('//button[contains(text(), "Proceed")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(len(mail.outbox), 0)

        orderObj = Order.objects.last()

        review_table_header = self.selenium.find_element_by_css_selector('table thead')

        self.assertIn('Email: john@example.com', review_table_header.text)
        self.assertIn(f'#{orderObj.token_id}', review_table_header.text)

        review_table_body = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(review_table_body.find_elements_by_css_selector('tr')), 1)

        review_table_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('12.34', review_table_footer.text)

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.choose_select_option('status', 'confirmed')
        self.choose_select_option('fraud_status', 'accept')
        self.choose_select_option('gateway_response', '3ds-redirect')
        self.choose_select_option('verification_result', 'confirmed')

        self.selenium.find_element_by_css_selector('#payment-form button.btn-primary').click()

        self.assertEqual(self.get_current_url(), orderObj.get_absolute_url())

        self.assertEqual(len(mail.outbox), 1)

        orderObj.refresh_from_db()

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertTrue(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.assertEqual(orderObj.status, Order.STATUS_PAID)
        self.assertTrue(orderObj.is_shipping_required)
        self.assertTrue(orderObj.needs_shipped)
        self.assertFalse(orderObj.is_user_required)
        self.assertFalse(orderObj.needs_completed)

        self.assertEqual(orderObj.billing_address, orderObj.shipping_address)

    @unittest.skipUnless('dummy' in settings.PAYMENT_VARIANTS, 'Dummy provider is not configured.')
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='dummy')
    def test_checkout_with_shipping_and_with_user_flow(self):
        self.productvariantObj.shipped = True
        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Proceed to Checkout")]').click()

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.update_input_value('username', 'test@example.com')
        self.update_input_value('password', 'secret')

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-contact'))

        self.assertEqual(Order.objects.all().count(), 0)

        # no need to enter address, the logged in user already saved on

        self.selenium.find_element_by_xpath('//button[contains(text(), "Proceed")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(len(mail.outbox), 0)

        orderObj = Order.objects.last()

        review_table_header = self.selenium.find_element_by_css_selector('table thead')

        self.assertIn('Email: test@example.com', review_table_header.text)
        self.assertIn(f'#{orderObj.token_id}', review_table_header.text)

        review_table_body = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(review_table_body.find_elements_by_css_selector('tr')), 1)

        review_table_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('12.34', review_table_footer.text)

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.choose_select_option('status', 'confirmed')
        self.choose_select_option('fraud_status', 'accept')
        self.choose_select_option('gateway_response', '3ds-redirect')
        self.choose_select_option('verification_result', 'confirmed')

        self.selenium.find_element_by_css_selector('#payment-form button.btn-primary').click()

        self.assertEqual(self.get_current_url(), orderObj.get_absolute_url())

        self.assertEqual(len(mail.outbox), 1)

        orderObj.refresh_from_db()

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertTrue(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.assertEqual(orderObj.status, Order.STATUS_PAID)
        self.assertTrue(orderObj.is_shipping_required)
        self.assertTrue(orderObj.needs_shipped)
        self.assertTrue(orderObj.is_user_required)
        self.assertFalse(orderObj.needs_completed)

    @unittest.skipUnless(
        'stripe' in settings.PAYMENT_VARIANTS and
        'pk_test_' in settings.PAYMENT_VARIANTS['stripe'][1]['public_key'] and
        'sk_test_' in settings.PAYMENT_VARIANTS['stripe'][1]['secret_key'],
        'Stripe provider is not configured for testing.')
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='stripe')
    def test_checkout_with_stripe_and_valid_card(self):
        self.productvariantObj.shipped = True
        self.productvariantObj.possessed = False
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Proceed to Checkout")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-contact'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.update_input_value('billing_first_name', 'John')
        self.update_input_value('billing_last_name', 'Doe')
        self.update_input_value('billing_email', 'test@example.com')
        self.update_input_value('billing_address1', '1234 5th Street')
        self.update_input_value('billing_address2', '')
        self.update_input_value('billing_city', 'Two Dot')
        self.update_input_value('billing_state', 'MT')
        self.update_input_value('billing_zip_code', '59085')
        self.update_input_value('billing_phone', '406-555-1234')
        self.selenium.find_element_by_css_selector('[name=use_same_contacts]').click()

        self.selenium.find_element_by_xpath('//button[contains(text(), "Proceed")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(len(mail.outbox), 0)

        orderObj = Order.objects.last()

        review_table_header = self.selenium.find_element_by_css_selector('table thead')

        self.assertIn('Email: test@example.com', review_table_header.text)
        self.assertIn(f'#{orderObj.token_id}', review_table_header.text)

        review_table_body = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(review_table_body.find_elements_by_css_selector('tr')), 1)

        review_table_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('12.34', review_table_footer.text)

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        # pay with stripe
        self.selenium.find_element_by_css_selector('#payment-form button[type=submit]').click()

        self.selenium.switch_to.frame('stripe_checkout_app')
        self.selenium.find_element_by_css_selector('[type=email][placeholder="Email"]').send_keys('test@example.com')
        self.selenium.find_element_by_css_selector('[type=tel][placeholder="Card number"]').send_keys('4242424242424242')
        self.selenium.find_element_by_css_selector('[type=tel][placeholder="MM / YY"]').send_keys(f'01{self._card_expiration_year}')
        self.selenium.find_element_by_css_selector('[type=tel][placeholder="CVC"]').send_keys('123')

        pay_button = self.selenium.find_element_by_css_selector('form button[type=submit]')

        self.assertIn('12.34', pay_button.text)

        pay_button.click()

        self.selenium.switch_to.default_content()

        # wait until we're redirected
        self.assertIsNotNone(self.selenium.find_element_by_xpath('//button[contains(text(), "Print")]'))

        self.assertEqual(self.get_current_url(), orderObj.get_absolute_url())

        self.assertEqual(len(mail.outbox), 1)

        orderObj.refresh_from_db()

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertTrue(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.assertEqual(orderObj.status, Order.STATUS_PAID)
        self.assertTrue(orderObj.is_shipping_required)
        self.assertTrue(orderObj.needs_shipped)
        self.assertFalse(orderObj.is_user_required)
        self.assertFalse(orderObj.needs_completed)

    @unittest.skipUnless(
        'stripe' in settings.PAYMENT_VARIANTS and
        'pk_test_' in settings.PAYMENT_VARIANTS['stripe'][1]['public_key'] and
        'sk_test_' in settings.PAYMENT_VARIANTS['stripe'][1]['secret_key'],
        'Stripe provider is not configured for testing.')
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='stripe')
    def test_checkout_with_stripe_and_invalid_card_declined(self):
        self.productvariantObj.shipped = True
        self.productvariantObj.possessed = False
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Proceed to Checkout")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-contact'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.update_input_value('billing_first_name', 'John')
        self.update_input_value('billing_last_name', 'Doe')
        self.update_input_value('billing_email', 'test@example.com')
        self.update_input_value('billing_address1', '1234 5th Street')
        self.update_input_value('billing_address2', '')
        self.update_input_value('billing_city', 'Two Dot')
        self.update_input_value('billing_state', 'MT')
        self.update_input_value('billing_zip_code', '59085')
        self.update_input_value('billing_phone', '406-555-1234')
        self.selenium.find_element_by_css_selector('[name=use_same_contacts]').click()

        self.selenium.find_element_by_xpath('//button[contains(text(), "Proceed")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(len(mail.outbox), 0)

        orderObj = Order.objects.last()

        review_table_header = self.selenium.find_element_by_css_selector('table thead')

        self.assertIn('Email: test@example.com', review_table_header.text)
        self.assertIn(f'#{orderObj.token_id}', review_table_header.text)

        review_table_body = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(review_table_body.find_elements_by_css_selector('tr')), 1)

        review_table_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('12.34', review_table_footer.text)

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        # pay with stripe
        self.selenium.find_element_by_css_selector('#payment-form button[type=submit]').click()

        self.selenium.switch_to.frame('stripe_checkout_app')

        self.selenium.find_element_by_css_selector('[type=email][placeholder="Email"]').send_keys('test@example.com')
        card_number_input = self.selenium.find_element_by_css_selector('[type=tel][placeholder="Card number"]')
        self.selenium.find_element_by_css_selector('[type=tel][placeholder="MM / YY"]').send_keys(f'01{self._card_expiration_year}')
        self.selenium.find_element_by_css_selector('[type=tel][placeholder="CVC"]').send_keys('123')

        pay_button = self.selenium.find_element_by_css_selector('form button[type=submit]')

        for card_number, card_error in [
            ('4000000000000002', 'This card was declined.'),
            ('4000000000009995', 'Your card has insufficient funds.'),
            ('4000000000000069', None),  # expired card
            ('4000000000000127', None),  # bad CVC
            ('4000000000000119', 'An error occurred while processing your card. Try again in a little bit.'),
        ]:
            with self.subTest(card_number=card_number, card_error=card_error):
                while len(card_number_input.get_attribute('value')):
                    card_number_input.send_keys(self.KEY_BACKSPACE)
                card_number_input.send_keys(card_number)

                pay_button.click()

                if card_error is not None:
                    card_error_popup = self.selenium.find_element_by_css_selector('form .Popover-content')

                    time.sleep(0.5)

                    self.assertIn(card_error, card_error_popup.text)

        self.assertEqual(len(mail.outbox), 0)

        orderObj.refresh_from_db()

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.assertEqual(orderObj.status, Order.STATUS_PENDING)
        self.assertTrue(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_shipped)
        self.assertFalse(orderObj.is_user_required)
        self.assertFalse(orderObj.needs_completed)

    @unittest.skipUnless(
        'stripe' in settings.PAYMENT_VARIANTS and
        'pk_test_' in settings.PAYMENT_VARIANTS['stripe'][1]['public_key'] and
        'sk_test_' in settings.PAYMENT_VARIANTS['stripe'][1]['secret_key'],
        'Stripe provider is not configured for testing.')
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='stripe')
    def test_checkout_with_stripe_and_invalid_card_charge_blocked(self):
        self.productvariantObj.shipped = True
        self.productvariantObj.possessed = False
        self.productvariantObj.save(update_fields=['shipped', 'possessed'])

        self.selenium.get(f"{self.live_server_url}{reverse('store:product-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:product-list'))

        self.selenium.find_element_by_css_selector(f'form[data-product="{self.productObj.pk}"] button.btn-add-to-cart').click()

        self.selenium.get(f"{self.live_server_url}{reverse('store:cart-detail')}")

        self.assertEqual(self.get_current_url(), reverse('store:cart-detail'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.selenium.find_element_by_xpath('//a[contains(text(), "Proceed to Checkout")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-contact'))

        self.assertEqual(Order.objects.all().count(), 0)

        self.update_input_value('billing_first_name', 'John')
        self.update_input_value('billing_last_name', 'Doe')
        self.update_input_value('billing_email', 'test@example.com')
        self.update_input_value('billing_address1', '1234 5th Street')
        self.update_input_value('billing_address2', '')
        self.update_input_value('billing_city', 'Two Dot')
        self.update_input_value('billing_state', 'MT')
        self.update_input_value('billing_zip_code', '59085')
        self.update_input_value('billing_phone', '406-555-1234')
        self.selenium.find_element_by_css_selector('[name=use_same_contacts]').click()

        self.selenium.find_element_by_xpath('//button[contains(text(), "Proceed")]').click()

        self.assertEqual(self.get_current_url(), reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertEqual(len(mail.outbox), 0)

        orderObj = Order.objects.last()

        review_table_header = self.selenium.find_element_by_css_selector('table thead')

        self.assertIn('Email: test@example.com', review_table_header.text)
        self.assertIn(f'#{orderObj.token_id}', review_table_header.text)

        review_table_body = self.selenium.find_element_by_css_selector('main table tbody')

        self.assertEqual(len(review_table_body.find_elements_by_css_selector('tr')), 1)

        review_table_footer = self.selenium.find_element_by_css_selector('table tfoot')

        self.assertIn('12.34', review_table_footer.text)

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        # pay with stripe
        self.selenium.find_element_by_css_selector('#payment-form button[type=submit]').click()

        self.selenium.switch_to.frame('stripe_checkout_app')
        self.selenium.find_element_by_css_selector('[type=email][placeholder="Email"]').send_keys('test@example.com')
        self.selenium.find_element_by_css_selector('[type=tel][placeholder="Card number"]').send_keys('4100000000000019')
        self.selenium.find_element_by_css_selector('[type=tel][placeholder="MM / YY"]').send_keys(f'01{self._card_expiration_year}')
        self.selenium.find_element_by_css_selector('[type=tel][placeholder="CVC"]').send_keys('123')

        pay_button = self.selenium.find_element_by_css_selector('form button[type=submit]')

        self.assertIn('12.34', pay_button.text)

        pay_button.click()

        self.selenium.switch_to.default_content()

        error_alert = self.selenium.find_element_by_css_selector('.alert.alert-danger.show')

        self.assertIn('Payment unsuccessful.', error_alert.text)

        self.assertEqual(len(mail.outbox), 0)

        orderObj.refresh_from_db()

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.34), currency=CURRENCY)).amount), 0.001)
        self.assertFalse(orderObj.is_fully_paid())
        self.assertEqual(orderObj.payments.count(), 1)

        self.assertEqual(orderObj.status, Order.STATUS_PAYMENT_ERROR)
        self.assertTrue(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_shipped)
        self.assertFalse(orderObj.is_user_required)
        self.assertFalse(orderObj.needs_completed)


class OrderHistoryFlowTests(StoreDataTestMixin, UserDataMixin, SeleniumTestCase):
    """
    Viewing order list and detail by token or login. Also cancelling.
    """
    def setUp(self):
        super().setUp()

        self.orderObj.user = self.userObj
        self.orderObj.save(update_fields=['user'])

    def test_order_history_flow(self):
        self.login_user('test@example.com', 'secret')

        self.selenium.get(f"{self.live_server_url}{reverse('store:order-list')}")

        self.assertEqual(self.get_current_url(), reverse('store:order-list'))

        self.selenium.find_element_by_xpath(f'//a[contains(text(), "#{self.orderObj.token_id}")]').click()

        self.assertEqual(self.get_current_url(), self.orderObj.get_absolute_url())

        self.selenium.find_element_by_xpath(f'//button[contains(text(), "Cancel")]').click()

        time.sleep(0.5)

        self.selenium.find_element_by_css_selector('button.btn-secondary[data-dismiss="modal"]').click()

        time.sleep(0.5)

        self.orderObj.refresh_from_db()

        self.assertEqual(self.orderObj.status, Order.STATUS_PENDING)

        self.selenium.find_element_by_xpath(f'//button[contains(text(), "Cancel")]').click()

        time.sleep(0.5)

        self.selenium.find_element_by_xpath(f'//a[contains(text(), "Cancel Order")]').click()

        time.sleep(0.5)

        self.orderObj.refresh_from_db()

        self.assertEqual(self.orderObj.status, Order.STATUS_CANCELLED)

        self.assertEqual(self.get_current_url(), self.orderObj.get_absolute_url())
