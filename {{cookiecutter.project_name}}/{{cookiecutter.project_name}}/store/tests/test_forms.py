from decimal import Decimal

from django.conf import settings

from prices import Money

from store.models import Product, Address
from store.forms import (
    ProductFilterForm, CartAddForm, CartUpdateForm,
    CheckoutContactForm,
)

from {{ cookiecutter.project_name }}.tests.test_base import BaseTestCase, UserDataMixin
from store.tests.test_models import StoreDataTestMixin

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')


class ProductFilterFormTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def test_product_filter_form_clean(self):
        self.assertTrue(ProductFilterForm({}).is_valid())

        form = ProductFilterForm({
            'price_low': 10,
            'price_high': 15,
        })

        self.assertTrue(form.is_valid())

        self.assertEqual(form.cleaned_data.get('price_low'), Money(Decimal(10.00), currency=CURRENCY))
        self.assertEqual(form.cleaned_data.get('price_high'), Money(Decimal(15.00), currency=CURRENCY))

        form = ProductFilterForm({
            'price_low': 10,
            'price_high': 5,
        })

        self.assertTrue(form.is_valid())

        self.assertEqual(form.cleaned_data.get('price_low'), Money(Decimal(10.00), currency=CURRENCY))
        self.assertIsNone(form.cleaned_data.get('price_high'))

    def test_product_filter_form_get_categories_roots(self):
        categoryObj1 = self.create_category('Category 1', 'category-1')
        categoryObj1_1 = self.create_category('Category 1.1', 'category-1-1', parent=categoryObj1)
        categoryObj2 = self.create_category('Category 2', 'category-2')
        categoryObj2_1 = self.create_category('Category 2.1', 'category-2-1', parent=categoryObj2)

        form = ProductFilterForm()

        roots = form.get_categories_roots()

        self.assertIn(categoryObj1, roots)
        self.assertIn(categoryObj2, roots)
        self.assertNotIn(categoryObj1_1, roots)
        self.assertNotIn(categoryObj2_1, roots)

    def test_product_filter_form_has_filters(self):
        form = ProductFilterForm({})

        self.assertTrue(form.is_valid())

        self.assertFalse(form.has_filters())

        form = ProductFilterForm({
            'search': 'test',
        })

        self.assertFalse(form.has_filters())

        form = ProductFilterForm({
            'search': 'test',
        })

        self.assertTrue(form.is_valid())

        self.assertTrue(form.has_filters())

        form = ProductFilterForm({
            'price_high': 20,
        })

        self.assertTrue(form.is_valid())

        self.assertTrue(form.has_filters())

    def test_product_filter_form_filter_queryset(self):
        categoryObj = self.create_category('Category 1', 'category-1')
        attributeObj = self.create_productattribute('Attribute 1')

        productObj1 = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(1.50), currency=CURRENCY))
        productObj2 = self.create_product('Product 2', 'product-2', 'Product 2.', Money(Decimal(2.50), currency=CURRENCY), categories=[categoryObj])
        productObj3 = self.create_product('Product 3', 'product-3', 'Product 3.', Money(Decimal(3.50), currency=CURRENCY), attributes=[attributeObj])
        productObj4 = self.create_product('Product 4', 'product-4', 'Product 4.', Money(Decimal(4.50), currency=CURRENCY), categories=[categoryObj], attributes=[attributeObj])

        form = ProductFilterForm({})

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertIn(productObj1, products)
        self.assertIn(productObj2, products)
        self.assertIn(productObj3, products)
        self.assertIn(productObj4, products)

        form = ProductFilterForm({
            'categories': [categoryObj.pk],
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertNotIn(productObj1, products)
        self.assertIn(productObj2, products)
        self.assertNotIn(productObj3, products)
        self.assertIn(productObj4, products)

        form = ProductFilterForm({
            'attributes': [attributeObj.pk],
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertNotIn(productObj1, products)
        self.assertNotIn(productObj2, products)
        self.assertIn(productObj3, products)
        self.assertIn(productObj4, products)

        form = ProductFilterForm({
            'categories': [categoryObj.pk],
            'attributes': [attributeObj.pk],
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertNotIn(productObj1, products)
        self.assertNotIn(productObj2, products)
        self.assertNotIn(productObj3, products)
        self.assertIn(productObj4, products)

        form = ProductFilterForm({
            'price_low': 3,
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertNotIn(productObj1, products)
        self.assertNotIn(productObj2, products)
        self.assertIn(productObj3, products)
        self.assertIn(productObj4, products)

        form = ProductFilterForm({
            'price_low': 3,
            'price_high': 4,
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertNotIn(productObj1, products)
        self.assertNotIn(productObj2, products)
        self.assertIn(productObj3, products)
        self.assertNotIn(productObj4, products)

        form = ProductFilterForm({
            'price_high': 4,
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertIn(productObj1, products)
        self.assertIn(productObj2, products)
        self.assertIn(productObj3, products)
        self.assertNotIn(productObj4, products)

        form = ProductFilterForm({
            'categories': [categoryObj.pk],
            'price_high': 3,
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertNotIn(productObj1, products)
        self.assertIn(productObj2, products)
        self.assertNotIn(productObj3, products)
        self.assertNotIn(productObj4, products)

        form = ProductFilterForm({
            'search': 'Product 1',
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertIn(productObj1, products)
        self.assertNotIn(productObj2, products)
        self.assertNotIn(productObj3, products)
        self.assertNotIn(productObj4, products)

        form = ProductFilterForm({
            'categories': [categoryObj.pk],
            'attributes': [attributeObj.pk],
            'price_low': 4,
            'price_high': 5,
            'search': '4',
        })

        self.assertTrue(form.is_valid())

        products = form.filter_queryset(Product.objects.all())

        self.assertNotIn(productObj1, products)
        self.assertNotIn(productObj2, products)
        self.assertNotIn(productObj3, products)
        self.assertIn(productObj4, products)


class CartAddFormTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))
        self.productvariantObj = self.create_productvariant('Product 1', self.productObj)

    def test_cart_add_form_clean(self):
        self.assertFalse(CartAddForm({}).is_valid())

        self.assertFalse(CartAddForm({
            'variant': self.productvariantObj.pk,
            'quantity': 0,
        }).is_valid())

        self.assertFalse(CartAddForm({
            'variant': self.productvariantObj.pk,
            'quantity': 1000,
        }).is_valid())

        self.assertTrue(CartAddForm({
            'variant': self.productvariantObj.pk,
            'quantity': 5,
        }).is_valid())

        self.productvariantObj.singular = True
        self.productvariantObj.save(update_fields=['singular'])

        form = CartAddForm({
            'variant': self.productvariantObj.pk,
            'quantity': 5,
        })

        self.assertTrue(form.is_valid())
        self.assertEqual(form.cleaned_data.get('quantity'), 1)

        self.assertTrue(CartAddForm({
            'variant': self.productvariantObj.pk,
            'quantity': 1,
        }).is_valid())

        self.productvariantObj.available = False
        self.productvariantObj.save(update_fields=['available'])

        self.assertFalse(CartUpdateForm({
            'variant': self.productvariantObj.pk,
            'quantity': 1,
        }).is_valid())


class CartUpdateFormTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))
        self.productvariantObj = self.create_productvariant('Product 1', self.productObj)

    def test_cart_update_form_clean(self):
        self.assertFalse(CartUpdateForm({}).is_valid())

        self.assertTrue(CartUpdateForm({
            'variant': self.productvariantObj.pk,
            'quantity': 0,
        }).is_valid())

        self.assertFalse(CartUpdateForm({
            'variant': self.productvariantObj.pk,
            'quantity': 1000,
        }).is_valid())

        self.assertTrue(CartUpdateForm({
            'variant': self.productvariantObj.pk,
            'quantity': 5,
        }).is_valid())

        self.productvariantObj.singular = True
        self.productvariantObj.save(update_fields=['singular'])

        self.assertFalse(CartUpdateForm({
            'variant': self.productvariantObj.pk,
            'quantity': 5,
        }).is_valid())

        self.assertTrue(CartUpdateForm({
            'variant': self.productvariantObj.pk,
            'quantity': 1,
        }).is_valid())

        self.productvariantObj.available = False
        self.productvariantObj.save(update_fields=['available'])

        self.assertFalse(CartUpdateForm({
            'variant': self.productvariantObj.pk,
            'quantity': 1,
        }).is_valid())


class CheckoutContactFormTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    CREATE_USER_DATA = False
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.userObj = self.create_user('john@example.com', 'secret')

    def test_checkout_contact_form_init(self):
        form = CheckoutContactForm()

        self.assertEqual(len(form.fields.keys()), 9)

        form = CheckoutContactForm(shipping_required=True)

        self.assertEqual(len(form.fields.keys()), 19)

        form = CheckoutContactForm(user=self.userObj)

        self.assertEqual(len(form.fields.keys()), 9)

        self.assertEqual(form.fields['billing_email'].initial, 'john@example.com')
        self.assertEqual(form.fields['billing_first_name'].initial, '')
        self.assertEqual(form.fields['billing_last_name'].initial, '')

        self.userObj.first_name = 'John'
        self.userObj.last_name = 'Doe'
        self.userObj.save(update_fields=['first_name', 'last_name'])

        form = CheckoutContactForm(user=self.userObj, shipping_required=True)

        self.assertEqual(len(form.fields.keys()), 19)

        self.assertEqual(form.fields['billing_email'].initial, 'john@example.com')
        self.assertEqual(form.fields['shipping_email'].initial, 'john@example.com')
        self.assertEqual(form.fields['billing_first_name'].initial, 'John')
        self.assertEqual(form.fields['shipping_first_name'].initial, 'John')
        self.assertEqual(form.fields['billing_last_name'].initial, 'Doe')
        self.assertEqual(form.fields['shipping_last_name'].initial, 'Doe')

        addressObj = self.create_address('John', 'Doe', '1234 5th Street', 'Two Dot', 'MT', '59085', '406-555-1234')

        form = CheckoutContactForm(user=self.userObj)

        self.assertEqual(len(form.fields.keys()), 9)

        self.assertEqual(form.fields['billing_email'].initial, 'john@example.com')
        self.assertEqual(form.fields['billing_first_name'].initial, 'John')
        self.assertEqual(form.fields['billing_last_name'].initial, 'Doe')
        self.assertIsNone(form.fields['billing_address1'].initial)
        self.assertIsNone(form.fields['billing_address2'].initial)
        self.assertIsNone(form.fields['billing_city'].initial)
        self.assertIsNone(form.fields['billing_state'].initial)
        self.assertIsNone(form.fields['billing_zip_code'].initial)
        self.assertIsNone(form.fields['billing_phone'].initial)

        addressObj.user = self.userObj
        addressObj.save(update_fields=['user'])

        form = CheckoutContactForm(user=self.userObj)

        self.assertEqual(len(form.fields.keys()), 9)

        self.assertEqual(form.fields['billing_email'].initial, 'john@example.com')
        self.assertEqual(form.fields['billing_first_name'].initial, 'John')
        self.assertEqual(form.fields['billing_last_name'].initial, 'Doe')
        self.assertEqual(form.fields['billing_address1'].initial, '1234 5th Street')
        self.assertEqual(form.fields['billing_address2'].initial, '')
        self.assertEqual(form.fields['billing_city'].initial, 'Two Dot')
        self.assertEqual(form.fields['billing_state'].initial, 'MT')
        self.assertEqual(form.fields['billing_zip_code'].initial, '59085')
        self.assertEqual(form.fields['billing_phone'].initial, '+14065551234')

    def test_checkout_contact_form_clean(self):
        self.assertFalse(CheckoutContactForm({}, shipping_required=False).is_valid())

        self.assertTrue(CheckoutContactForm({
            'billing_email': 'john@example.com',
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
        }, shipping_required=False).is_valid())

        self.assertFalse(CheckoutContactForm({
            'billing_email': 'john@example.com',
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
        }, shipping_required=True).is_valid())

        self.assertTrue(CheckoutContactForm({
            'billing_email': 'john@example.com',
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
            'use_same_contacts': True,
        }, shipping_required=True).is_valid())

        self.assertTrue(CheckoutContactForm({
            'billing_email': 'john@example.com',
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
            'use_same_contacts': False,
            'shipping_email': 'john@example.com',
            'shipping_first_name': 'John',
            'shipping_last_name': 'Doe',
            'shipping_address1': '1234 5th Street',
            'shipping_address2': '',
            'shipping_city': 'Two Dot',
            'shipping_state': 'MT',
            'shipping_zip_code': '59085',
            'shipping_phone': '406-555-1234',
        }, shipping_required=True).is_valid())

    def test_checkout_contact_form_get_billing_address(self):
        form = CheckoutContactForm({
            'billing_email': 'john@example.com',
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
        })

        self.assertEqual(Address.objects.all().count(), 0)

        with self.assertRaises(Exception):
            form.get_billing_address()

        self.assertEqual(Address.objects.all().count(), 0)

        self.assertTrue(form.is_valid())

        self.assertIsNotNone(form.get_billing_address())

        self.assertEqual(Address.objects.all().count(), 1)

        self.assertIsNotNone(form.get_billing_address())

        self.assertEqual(Address.objects.all().count(), 1)

    def test_checkout_contact_form_get_shipping_address(self):
        form = CheckoutContactForm({
            'billing_email': 'john@example.com',
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
            'use_same_contacts': True,
        }, shipping_required=True)

        self.assertEqual(Address.objects.all().count(), 0)

        with self.assertRaises(Exception):
            form.get_shipping_address()

        self.assertEqual(Address.objects.all().count(), 0)

        self.assertTrue(form.is_valid())

        self.assertIsNotNone(form.get_shipping_address())

        self.assertEqual(Address.objects.all().count(), 1)

        self.assertIsNotNone(form.get_shipping_address())

        self.assertEqual(Address.objects.all().count(), 1)

        form = CheckoutContactForm({
            'billing_email': 'john@example.com',
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
        }, shipping_required=False)

        self.assertTrue(form.is_valid())

        self.assertIsNone(form.get_shipping_address())
