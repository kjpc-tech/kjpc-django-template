import datetime

from decimal import Decimal
from io import BytesIO

from django.conf import settings
from django.core import mail
from django.core.files.base import ContentFile
from django.utils import timezone

from payments import get_payment_model, PaymentStatus

from PIL import Image

from prices import Money

from {{ cookiecutter.project_name }}.tests.test_base import BaseTestCase, UserDataMixin

from store.models import (
    Category, ProductAttribute, Product, ProductImage,
    ProductVariant, Address, Order, OrderItem,
)
from store.cart import Cart

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')


class StoreDataTestMixin:
    CREATE_STORE_DATA = True

    def setUp(self):
        super().setUp()

        if self.CREATE_STORE_DATA:
            self.categoryObj = self.create_category('Test Category', 'test-category')
            self.productattributeObj = self.create_productattribute('Test Attribute')
            self.productObj = self.create_product('Test Product', 'test-product', 'A test product.', Money(Decimal(12.34), currency=CURRENCY), categories=[self.categoryObj], attributes=[self.productattributeObj])
            self.productimageObj = self.create_productimage(self.productObj, self.create_image(), alternative_text='Test product image.')
            self.productvariantObj = self.create_productvariant('Test Product Variant', self.productObj, sku='12345', images=[self.productimageObj])
            self.addressObj = self.create_address('John', 'Doe', '1234 5th Street', 'Two Dot', 'MT', '59085', '406-555-1234', user=getattr(self, 'userObj', None))
            self.orderObj = self.create_order(self.addressObj, user=getattr(self, 'userObj', None), shipping_address=self.addressObj, anonymous_user_email='john@example.com')
            self.orderitemObj = self.create_orderitem(self.orderObj, self.productObj, self.productvariantObj, 'Test Product Variant', '12345', 1, Money(Decimal(12.34), currency=CURRENCY))

    def tearDown(self):
        # explicitly delete these so the files on disk are removed
        ProductImage.objects.all().delete()

        super().tearDown()

    def create_category(self, name, slug, description=None, parent=None):
        return Category.objects.create(
            name=name,
            slug=slug,
            description=description or '',
            parent=parent,
        )

    def create_productattribute(self, name):
        return ProductAttribute.objects.create(
            name=name,
        )

    def create_product(self, name, slug, description, price, weight=None, available=True, categories=[], attributes=[]):
        productObj = Product.objects.create(
            name=name,
            slug=slug,
            description=description,
            price=price,
            weight=weight,
            available=available
        )

        for categoryObj in categories:
            productObj.categories.add(categoryObj)

        for attributeObj in attributes:
            productObj.attributes.add(attributeObj)

        return productObj

    def create_image(self, size=(500, 500), name='photo.png'):
        img = Image.frombytes('L', size, bytes("\x00" * size[0] * size[1], 'utf-8'))
        img_in_mem = BytesIO()
        img.save(img_in_mem, format='PNG')
        img_in_mem.seek(0)
        content_file = ContentFile(img_in_mem.getvalue(), name=name)
        img_in_mem.close()
        return content_file

    def create_productimage(self, product, image, alternative_text=None):
        return ProductImage.objects.create(
            product=product,
            image=image,
            alternative_text=alternative_text or '',
        )

    def create_productvariant(self, name, product, sku=None, price_override=None, weight_override=None, available=True, shipped=True, possessed=False, singular=False, images=[]):
        productvariantObj = ProductVariant.objects.create(
            name=name,
            sku=sku,
            price_override=price_override,
            weight_override=weight_override,
            product=product,
            available=available,
            shipped=shipped,
            possessed=possessed,
            singular=singular
        )

        for productimageObj in images:
            productvariantObj.images.add(productimageObj)

        return productvariantObj

    def create_address(self, first_name, last_name, address1, city, state, zip_code, phone, address2=None, user=None):
        return Address.objects.create(
            first_name=first_name,
            last_name=last_name,
            address1=address1,
            address2=address2 or '',
            city=city,
            state=state,
            zip_code=zip_code,
            phone=phone,
            user=user,
        )

    def create_order(self, billing_address, total_price=None, total_shipping=None, status=None, user=None, shipping_address=None, anonymous_user_email=None):
        return Order.objects.create(
            status=status or Order.STATUS_PENDING,
            billing_address=billing_address,
            shipping_address=shipping_address,
            anonymous_user_email=anonymous_user_email or '',
            total_price=total_price,
            total_shipping=total_shipping,
        )

    def create_orderitem(self, order, product, variant, product_name, product_sku, quantity, unit_price):
        return OrderItem.objects.create(
            order=order,
            product=product,
            variant=variant,
            product_name=product_name,
            product_sku=product_sku,
            quantity=quantity,
            unit_price=unit_price,
        )

    def create_payment(self, order, variant, description, currency=None):
        return get_payment_model().objects.create(
            order=order,
            variant=variant,
            description=description,
            currency=currency or getattr(settings, 'STORE_CURRENCY', 'USD'),
        )


class CategoryModelTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False


class ProductAttributeModelTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False


class ProductModelTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def test_product_get_feature_image(self):
        productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))

        self.assertIsNone(productObj.get_feature_image())

        productimageObj = self.create_productimage(productObj, self.create_image(), alternative_text='Product 1 image 1.')

        self.assertEqual(productObj.get_feature_image(), productimageObj)

        productimageObj2 = self.create_productimage(productObj, self.create_image(), alternative_text='Product 1 image 2.')

        self.assertEqual(productObj.get_feature_image(), productimageObj)

        self.create_productvariant('Product 1', productObj, sku='12345', images=[productimageObj2])

        self.assertEqual(productObj.get_feature_image(), productimageObj2)


class ProductImageModelTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False


class ProductVariantModelTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))

    def test_product_variant_get_price_per_item(self):
        productvariantObj = self.create_productvariant('Product 1', self.productObj)

        self.assertEqual(productvariantObj.get_price_per_item(), Money(Decimal(10.00), currency=CURRENCY))

        productvariantObj.price_override = Money(Decimal(9.99), currency=CURRENCY)
        productvariantObj.save(update_fields=['price_override'])

        self.assertEqual(productvariantObj.get_price_per_item(), Money(Decimal(9.99), currency=CURRENCY))

    def test_product_variant_get_feature_image(self):
        productvariantObj = self.create_productvariant('Product 1', self.productObj)

        self.assertIsNone(productvariantObj.get_feature_image())

        productimageObj = self.create_productimage(self.productObj, self.create_image(), alternative_text='Product 1 image 1.')

        self.assertEqual(productvariantObj.get_feature_image(), productimageObj)

        productimageObj2 = self.create_productimage(self.productObj, self.create_image(), alternative_text='Product 1 image 2.')

        self.assertEqual(productvariantObj.get_feature_image(), productimageObj)

        productvariantObj.images.add(productimageObj2)

        self.assertEqual(productvariantObj.get_feature_image(), productimageObj2)

    def test_product_variant_is_available(self):
        productvariantObj = self.create_productvariant('Product 1', self.productObj, available=True)

        self.assertTrue(productvariantObj.is_available)

        productvariantObj.available = False
        productvariantObj.save(update_fields=['available'])

        self.assertFalse(productvariantObj.is_available)

        self.productObj.available = False
        self.productObj.save(update_fields=['available'])

        self.assertFalse(productvariantObj.is_available)

        productvariantObj.available = True
        productvariantObj.save(update_fields=['available'])

        self.assertFalse(productvariantObj.is_available)

    def test_product_variant_is_shipping_required(self):
        productvariantObj = self.create_productvariant('Product 1', self.productObj, shipped=True)

        self.assertTrue(productvariantObj.is_shipping_required)

        productvariantObj.shipped = False
        productvariantObj.save(update_fields=['shipped'])

        self.assertFalse(productvariantObj.is_shipping_required)

    def test_product_variant_is_user_required(self):
        productvariantObj = self.create_productvariant('Product 1', self.productObj, possessed=True)

        self.assertTrue(productvariantObj.is_user_required)

        productvariantObj.possessed = False
        productvariantObj.save(update_fields=['possessed'])

        self.assertFalse(productvariantObj.is_user_required)


class AddressModelTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def test_address_get_html_display(self):
        addressObj = self.create_address('John', 'Doe', '1234 5th Street', 'Two Dot', 'MT', '59085', '406-555-1234')

        self.assertEqual(
            addressObj.get_html_display,
            'John Doe<br>1234 5th Street<br>Two Dot, MT 59085<br>+14065551234'
        )

        addressObj.address2 = 'Suite B'
        addressObj.save(update_fields=['address2'])

        self.assertEqual(
            addressObj.get_html_display,
            'John Doe<br>1234 5th Street<br>Suite B<br>Two Dot, MT 59085<br>+14065551234'
        )

        addressObj.first_name = '<script>hacking</script>'
        addressObj.save(update_fields=['first_name'])

        self.assertNotIn(
            '<script>hacking</script>',
            addressObj.get_html_display,
        )


class OrderModelTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    CREATE_USER_DATA = False
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.userObj = self.create_user('john@example.com', 'secret')
        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))
        self.productvariantObj = self.create_productvariant('Product 1', self.productObj, shipped=True)
        self.addressObj = self.create_address('John', 'Doe', '1234 5th Street', 'Two Dot', 'MT', '59085', '406-555-1234')

    def test_order_create_from_cart(self):
        cartObj = Cart()
        cartObj.add(self.productvariantObj, 1)

        # no user
        orderObj = Order.create_from_cart(cartObj, 'john@example.com', self.addressObj, self.addressObj, userObj=None)

        self.assertIsNone(orderObj.user)
        self.assertEqual(orderObj.status, Order.STATUS_PENDING)
        self.assertEqual(orderObj.billing_address, self.addressObj)
        self.assertEqual(orderObj.shipping_address, self.addressObj)
        self.assertEqual(orderObj.anonymous_user_email, 'john@example.com')
        self.assertIsNone(orderObj.total_price)
        self.assertEqual(orderObj.get_total(), Money(Decimal(10.00), currency=CURRENCY))
        self.assertIsNone(orderObj.total_shipping)
        self.assertEqual(orderObj.get_shipping(), Money(0, currency=CURRENCY))
        self.assertEqual(orderObj.items.count(), 1)

        # with user
        self.addressObj.user = self.userObj
        self.addressObj.save(update_fields=['user'])
        orderObj = Order.create_from_cart(cartObj, 'john@example.com', self.addressObj, self.addressObj, userObj=self.userObj)

        self.assertEqual(orderObj.user, self.userObj)
        self.assertEqual(orderObj.status, Order.STATUS_PENDING)
        self.assertEqual(orderObj.billing_address, self.addressObj)
        self.assertEqual(orderObj.shipping_address, self.addressObj)
        self.assertEqual(orderObj.anonymous_user_email, '')
        self.assertIsNone(orderObj.total_price)
        self.assertEqual(orderObj.get_total(), Money(Decimal(10.00), currency=CURRENCY))
        self.assertIsNone(orderObj.total_shipping)
        self.assertEqual(orderObj.get_shipping(), Money(0, currency=CURRENCY))
        self.assertEqual(orderObj.items.count(), 1)

        # no shipping
        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])

        orderObj = Order.create_from_cart(cartObj, 'john@example.com', self.addressObj, None, userObj=self.userObj)

        self.assertEqual(orderObj.user, self.userObj)
        self.assertEqual(orderObj.status, Order.STATUS_PENDING)
        self.assertEqual(orderObj.billing_address, self.addressObj)
        self.assertIsNone(orderObj.shipping_address)
        self.assertEqual(orderObj.anonymous_user_email, '')
        self.assertIsNone(orderObj.total_price)
        self.assertEqual(orderObj.get_total(), Money(Decimal(10.00), currency=CURRENCY))
        self.assertIsNone(orderObj.total_shipping)
        self.assertEqual(orderObj.get_shipping(), Money(0, currency=CURRENCY))
        self.assertEqual(orderObj.items.count(), 1)

    def test_order_can_access_by_token(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='john@example.com')

        self.assertTrue(orderObj.can_access_by_token())

        orderObj.token_created = orderObj.token_created - datetime.timedelta(days=8)
        orderObj.save(update_fields=['token_created'])

        self.assertFalse(orderObj.can_access_by_token())

        orderObj.token_created = orderObj.token_created + datetime.timedelta(days=4)
        orderObj.save(update_fields=['token_created'])

        self.assertTrue(orderObj.can_access_by_token())

    def test_order_get_user_email(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertEqual(orderObj.get_user_email, 'test@example.com')

        orderObj.user = self.userObj
        orderObj.save(update_fields=['user'])
        del orderObj.get_user_email  # since it's cached

        self.assertEqual(orderObj.get_user_email, 'john@example.com')

    def test_order_get_total(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        with self.assertRaises(AttributeError):
            orderObj.get_total()

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        self.assertEqual(orderObj.get_total(), Money(Decimal(10.00), currency=CURRENCY))

        orderObj.total_price = Money(Decimal(9.99), currency=CURRENCY)
        orderObj.save(update_fields=['total_price'])

        self.assertEqual(orderObj.get_total(), Money(Decimal(9.99), currency=CURRENCY))

    def test_order_get_shipping(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertEqual(orderObj.get_shipping(), Money(0, currency=CURRENCY))

        orderObj.total_shipping = Money(Decimal(4.99), currency=CURRENCY)
        orderObj.save(update_fields=['total_shipping'])

        self.assertEqual(orderObj.get_shipping(), Money(Decimal(4.99), currency=CURRENCY))

    def test_order_get_grand_total(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        with self.assertRaises(AttributeError):
            orderObj.get_grand_total()

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(9.99), currency=CURRENCY))

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(9.99), currency=CURRENCY)).amount), 0.001)

        orderObj.total_price = Money(Decimal(9.99), currency=CURRENCY)
        orderObj.total_shipping = Money(Decimal(2.99), currency=CURRENCY)
        orderObj.save(update_fields=['total_price', 'total_shipping'])

        self.assertLessEqual(abs((orderObj.get_grand_total() - Money(Decimal(12.98), currency=CURRENCY)).amount), 0.001)

    def test_order_can_user_cancel(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertEqual(orderObj.status, Order.STATUS_PENDING)

        self.assertFalse(orderObj.can_user_cancel(None))

        self.assertFalse(orderObj.can_user_cancel(self.userObj))

        orderObj.user = self.userObj
        orderObj.save(update_fields=['user'])

        self.assertTrue(orderObj.can_user_cancel(self.userObj))

        orderObj.status = Order.STATUS_CANCELLED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.can_user_cancel(self.userObj))

        orderObj.status = Order.STATUS_REVIEWED
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.can_user_cancel(self.userObj))

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.can_user_cancel(self.userObj))

        orderObj.status = Order.STATUS_PAYMENT_ERROR
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.can_user_cancel(self.userObj))

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.can_user_cancel(self.userObj))

        orderObj.status = Order.STATUS_COMPLETE
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.can_user_cancel(self.userObj))

    def test_order_cancel_if_applicable(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com', status=Order.STATUS_COMPLETE)

        self.assertFalse(orderObj.cancel_if_applicable())

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.cancel_if_applicable())

        orderObj.status = Order.STATUS_CANCELLED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.cancel_if_applicable())

        orderObj.status = Order.STATUS_PAYMENT_ERROR
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.cancel_if_applicable())

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        paymentObj = self.create_payment(orderObj, 'dummy', orderObj.token_id)
        paymentObj.status = PaymentStatus.CONFIRMED
        paymentObj.save(update_fields=['status'])

        self.assertTrue(orderObj.cancel_if_applicable())

        self.assertEqual(orderObj.status, Order.STATUS_CANCELLED)

        paymentObj.refresh_from_db()

        self.assertEqual(paymentObj.status, PaymentStatus.REFUNDED)

        orderObj.status = Order.STATUS_REVIEWED
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.cancel_if_applicable())

        orderObj.status = Order.STATUS_PENDING
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.cancel_if_applicable())

    def test_order_is_fully_paid(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com', status=Order.STATUS_COMPLETE)

        with self.assertRaises(AttributeError):
            orderObj.is_fully_paid()

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        self.assertFalse(orderObj.is_fully_paid())

        paymentObj = self.create_payment(orderObj, 'dummy', orderObj.token_id)
        paymentObj.total = Decimal(10.00)
        paymentObj.captured_amount = Decimal(10.00)
        paymentObj.save(update_fields=['total', 'captured_amount'])

        self.assertFalse(orderObj.is_fully_paid())

        paymentObj.status = PaymentStatus.CONFIRMED
        paymentObj.save(update_fields=['status'])

        self.assertTrue(orderObj.is_fully_paid())

        paymentObj.total = Decimal(9.99)
        paymentObj.captured_amount = Decimal(9.99)
        paymentObj.save(update_fields=['total', 'captured_amount'])

        self.assertFalse(orderObj.is_fully_paid())

        paymentObj.total = Decimal(10.01)
        paymentObj.captured_amount = Decimal(10.01)
        paymentObj.save(update_fields=['total', 'captured_amount'])

        self.assertTrue(orderObj.is_fully_paid())

    def test_order_update_order_with_cart(self):
        productvariantObj2 = self.create_productvariant('Product 1.2', self.productObj, price_override=Money(Decimal(100.00), currency=CURRENCY), shipped=True)

        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        cartObj = Cart()
        cartObj.add(self.productvariantObj, 1)

        self.assertEqual(orderObj.items.count(), 0)

        orderObj.update_order_with_cart(cartObj)

        self.assertEqual(orderObj.items.count(), 1)
        self.assertEqual(orderObj.get_total(), Money(Decimal(10.00), currency=CURRENCY))

        cartObj.add(self.productvariantObj, 1)

        orderObj.update_order_with_cart(cartObj)

        self.assertEqual(orderObj.items.count(), 1)
        self.assertEqual(orderObj.get_total(), Money(Decimal(20.00), currency=CURRENCY))

        cartObj.add(self.productvariantObj, 1, replace=True)

        orderObj.update_order_with_cart(cartObj)

        self.assertEqual(orderObj.items.count(), 1)
        self.assertEqual(orderObj.get_total(), Money(Decimal(10.00), currency=CURRENCY))

        cartObj.add(productvariantObj2, 1)

        orderObj.update_order_with_cart(cartObj)

        self.assertEqual(orderObj.items.count(), 2)
        self.assertEqual(orderObj.get_total(), Money(Decimal(110.00), currency=CURRENCY))

        cartObj.add(self.productvariantObj, 0, replace=True)

        orderObj.update_order_with_cart(cartObj)

        self.assertEqual(orderObj.items.count(), 1)
        self.assertEqual(orderObj.get_total(), Money(Decimal(100.00), currency=CURRENCY))

    def test_order_is_shipping_required(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertFalse(orderObj.is_shipping_required)

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        del orderObj.is_shipping_required  # since it's cached

        self.assertTrue(orderObj.is_shipping_required)

        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])
        del orderObj.is_shipping_required  # since it's cached

        self.assertFalse(orderObj.is_shipping_required)

    def test_order_needs_shipped(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertFalse(orderObj.needs_shipped)

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        del orderObj.is_shipping_required  # since it's cached

        self.assertTrue(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_shipped)

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.is_shipping_required)
        self.assertTrue(orderObj.needs_shipped)

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_shipped)

        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])
        del orderObj.is_shipping_required  # since it's cached

        self.assertFalse(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_shipped)

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_shipped)

    def test_order_is_shipped(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertFalse(orderObj.is_shipped)

        orderObj.status = Order.STATUS_CANCELLED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_shipped)

        orderObj.status = Order.STATUS_REVIEWED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_shipped)

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_shipped)

        orderObj.status = Order.STATUS_PAYMENT_ERROR
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_shipped)

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.is_shipped)

        orderObj.status = Order.STATUS_COMPLETE
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.is_shipped)

    def test_order_ship(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertFalse(orderObj.ship())

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        del orderObj.is_shipping_required  # since it's cached

        self.assertTrue(orderObj.is_shipping_required)

        self.assertFalse(orderObj.ship())

        orderObj.status = Order.STATUS_REVIEWED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.ship())

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.ship())

        self.assertEqual(orderObj.status, Order.STATUS_SHIPPED)

    def test_order_needs_completed(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertFalse(orderObj.needs_completed)

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        del orderObj.is_shipping_required  # since it's cached

        self.assertTrue(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_completed)

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_completed)

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.is_shipping_required)
        self.assertTrue(orderObj.needs_completed)

        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])
        del orderObj.is_shipping_required  # since it's cached

        self.assertFalse(orderObj.is_shipping_required)
        self.assertFalse(orderObj.needs_completed)

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_shipping_required)
        self.assertTrue(orderObj.needs_completed)

    def test_order_is_complete(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertFalse(orderObj.is_complete)

        orderObj.status = Order.STATUS_CANCELLED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_complete)

        orderObj.status = Order.STATUS_REVIEWED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_complete)

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_complete)

        orderObj.status = Order.STATUS_PAYMENT_ERROR
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_complete)

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.is_complete)

        orderObj.status = Order.STATUS_COMPLETE
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.is_complete)

    def test_order_complete(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertFalse(orderObj.complete())

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        del orderObj.is_shipping_required  # since it's cached

        self.assertTrue(orderObj.is_shipping_required)

        self.assertFalse(orderObj.complete())

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.complete())

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.complete())

        self.assertEqual(orderObj.status, Order.STATUS_COMPLETE)

        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])
        del orderObj.is_shipping_required  # since it's cached

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        self.assertFalse(orderObj.complete())

        orderObj.status = Order.STATUS_PAID
        orderObj.save(update_fields=['status'])

        self.assertTrue(orderObj.complete())

        self.assertEqual(orderObj.status, Order.STATUS_COMPLETE)

    def test_order_is_user_required(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertFalse(orderObj.is_user_required)

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        del orderObj.is_user_required  # since it's cached

        self.assertFalse(orderObj.is_user_required)

        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['possessed'])
        del orderObj.is_user_required  # since it's cached

        self.assertTrue(orderObj.is_user_required)

    def test_order_update_status(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertEqual(orderObj.status, Order.STATUS_PENDING)

        status_time = orderObj.status_changed_at

        orderObj.update_status(Order.STATUS_REVIEWED)

        self.assertEqual(orderObj.status, Order.STATUS_REVIEWED)
        self.assertGreater(orderObj.status_changed_at, status_time)

        status_time = orderObj.status_changed_at

        orderObj.update_status(Order.STATUS_CANCELLED)

        self.assertEqual(orderObj.status, Order.STATUS_CANCELLED)
        self.assertGreater(orderObj.status_changed_at, status_time)

        status_time = orderObj.status_changed_at

        orderObj.update_status(Order.STATUS_PAID)

        self.assertEqual(orderObj.status, Order.STATUS_CANCELLED)
        self.assertEqual(orderObj.status_changed_at, status_time)

    def test_order_can_send_email(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.assertTrue(orderObj._can_send_email())

        orderObj.last_email_sent_at = timezone.now()
        orderObj.save(update_fields=['last_email_sent_at'])

        self.assertFalse(orderObj._can_send_email())

        orderObj.last_email_sent_at = timezone.now() - datetime.timedelta(minutes=90)
        orderObj.save(update_fields=['last_email_sent_at'])

        self.assertTrue(orderObj._can_send_email())

    def test_order_send_confirmation_email(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        orderObj.send_confirmation_email()

        self.assertEqual(len(mail.outbox), 0)
        self.assertIsNone(orderObj.last_email_sent_at)

        paymentObj = self.create_payment(orderObj, 'dummy', orderObj.token_id)
        paymentObj.status = PaymentStatus.CONFIRMED
        paymentObj.total = Decimal(10.00)
        paymentObj.captured_amount = Decimal(10.00)
        paymentObj.save(update_fields=['status', 'total', 'captured_amount'])

        orderObj.send_confirmation_email()

        self.assertEqual(len(mail.outbox), 1)
        self.assertIsNotNone(orderObj.last_email_sent_at)

        self.assertEqual(mail.outbox[0].subject, '{{ cookiecutter.project_name }} Order Confirmed')
        self.assertEqual(mail.outbox[0].body, f'Your order {orderObj.token_id} has been confirmed.')
        self.assertIn('test@example.com', mail.outbox[0].to)

        email_time = orderObj.last_email_sent_at

        orderObj.send_confirmation_email()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(orderObj.last_email_sent_at, email_time)

    def test_order_send_shipped_email(self):
        orderObj = self.create_order(self.addressObj, anonymous_user_email='test@example.com')

        self.create_orderitem(orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        orderObj.send_shipped_email()

        self.assertEqual(len(mail.outbox), 0)
        self.assertIsNone(orderObj.last_email_sent_at)

        orderObj.status = Order.STATUS_SHIPPED
        orderObj.save(update_fields=['status'])

        orderObj.send_shipped_email()

        self.assertEqual(len(mail.outbox), 1)
        self.assertIsNotNone(orderObj.last_email_sent_at)

        self.assertEqual(mail.outbox[0].subject, '{{ cookiecutter.project_name }} Order Shipped')
        self.assertEqual(mail.outbox[0].body, f'Your order {orderObj.token_id} has been shipped.')
        self.assertIn('test@example.com', mail.outbox[0].to)

        email_time = orderObj.last_email_sent_at

        orderObj.send_shipped_email()

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(orderObj.last_email_sent_at, email_time)


class OrderItemModelTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))
        self.productvariantObj = self.create_productvariant('Product 1', self.productObj)
        self.addressObj = self.create_address('John', 'Doe', '1234 5th Street', 'Two Dot', 'MT', '59085', '406-555-1234')
        self.orderObj = self.create_order(self.addressObj, anonymous_user_email='john@example.com')

    def test_order_item_get_quantity(self):
        orderitemObj = self.create_orderitem(self.orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        self.assertEqual(orderitemObj.get_quantity(), 1)

        orderitemObj.quantity = 2
        orderitemObj.save(update_fields=['quantity'])

        self.assertEqual(orderitemObj.get_quantity(), 2)

    def test_order_item_get_price_per_item(self):
        orderitemObj = self.create_orderitem(self.orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        self.assertEqual(orderitemObj.get_price_per_item(), Money(Decimal(10.00), currency=CURRENCY))

        orderitemObj.unit_price = Money(Decimal(9.99), currency=CURRENCY)
        orderitemObj.save(update_fields=['unit_price'])

        self.assertEqual(orderitemObj.get_price_per_item(), Money(Decimal(9.99), currency=CURRENCY))


class PaymentModelTests(StoreDataTestMixin, BaseTestCase):
    CREATE_STORE_DATA = False

    def setUp(self):
        super().setUp()

        self.productObj = self.create_product('Product 1', 'product-1', 'Product 1.', Money(Decimal(10.00), currency=CURRENCY))
        self.productvariantObj = self.create_productvariant('Product 1', self.productObj)
        self.addressObj = self.create_address('John', 'Doe', '1234 5th Street', 'Two Dot', 'MT', '59085', '406-555-1234')
        self.orderObj = self.create_order(self.addressObj, anonymous_user_email='john@example.com')

    def test_payment_get_purchased_items(self):
        paymentObj = self.create_payment(self.orderObj, 'dummy', self.orderObj.token_id)

        self.assertEqual(len([i for i in paymentObj.get_purchased_items()]), 0)

        self.create_orderitem(self.orderObj, self.productObj, self.productvariantObj, 'Product 1', '12345', 1, Money(Decimal(10.00), currency=CURRENCY))

        self.assertEqual(len([i for i in paymentObj.get_purchased_items()]), 1)
