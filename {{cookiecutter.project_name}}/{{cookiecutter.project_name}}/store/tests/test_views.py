import datetime

from decimal import Decimal

from django.conf import settings
from django.test import override_settings
from django.urls import reverse

from payments import get_payment_model, PaymentStatus

from prices import Money

from {{ cookiecutter.project_name }}.tests.test_base import BaseTestCase, UserDataMixin
from store.tests.test_models import StoreDataTestMixin

from store.models import Order
from store.cart import CartItem, Cart

CURRENCY = getattr(settings, 'STORE_CURRENCY', 'USD')


class DashboardViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def setUp(self):
        super().setUp()

        self.staffObj = self.create_user('staff@example.com', 'secret', is_staff=True)

    def test_dashboard_view_requires_staff(self):
        response = self.client.get(reverse('store:dashboard'))

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:dashboard')}")

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard'))

        self.assertEqual(response.status_code, 403)

        self.client.logout()

        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard'))

        self.assertTemplateUsed(response, 'store/dashboard.html')

    def test_dashboard_view_filters(self):
        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard'))

        self.assertEqual(response.context['filter'], '')
        self.assertEqual(len(response.context['orders']), 0)

        response = self.client.get(f"{reverse('store:dashboard')}?filter=pending")

        self.assertEqual(response.context['filter'], 'pending')
        self.assertEqual(len(response.context['orders']), 1)

        response = self.client.get(f"{reverse('store:dashboard')}?filter=all")

        self.assertEqual(response.context['filter'], 'all')
        self.assertEqual(len(response.context['orders']), 1)

        response = self.client.get(f"{reverse('store:dashboard')}?filter=paid")

        self.assertEqual(response.context['filter'], 'paid')
        self.assertEqual(len(response.context['orders']), 0)


class DashboardOrderViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def setUp(self):
        super().setUp()

        self.staffObj = self.create_user('staff@example.com', 'secret', is_staff=True)

    def test_dashboard_order_view_requires_staff(self):
        response = self.client.get(reverse('store:dashboard-order', kwargs={'order': self.orderObj.pk}))

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:dashboard-order', kwargs={'order': self.orderObj.pk})}")

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 403)

        self.client.logout()

        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order', kwargs={'order': self.orderObj.pk}))

        self.assertTemplateUsed(response, 'store/dashboard_order.html')

    def test_dashboard_order_view_preserves_filters(self):
        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.context['filter'], '')

        response = self.client.get(f"{reverse('store:dashboard-order', kwargs={'order': self.orderObj.pk})}?filter=paid")

        self.assertEqual(response.context['filter'], 'paid')


class DashboardOrderShipViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def setUp(self):
        super().setUp()

        self.staffObj = self.create_user('staff@example.com', 'secret', is_staff=True)

    def test_dashboard_order_ship_view_requires_staff(self):
        response = self.client.get(reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk}))

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk})}")

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 403)

        self.client.logout()

        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk}))

        self.assertRedirects(response, f"{reverse('store:dashboard')}?filter=")

    def test_dashboard_order_ship_view_preserves_filters(self):
        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk}))

        self.assertRedirects(response, f"{reverse('store:dashboard')}?filter=")

        response = self.client.get(f"{reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk})}?filter=paid")

        self.assertRedirects(response, f"{reverse('store:dashboard')}?filter=paid")

    def test_dashboard_order_ship_view_only_ships_applicable_orders(self):
        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        self.assertFalse(self.orderObj.needs_shipped)
        self.assertFalse(self.orderObj.is_shipped)

        response = self.client.get(reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 302)

        self.orderObj.refresh_from_db()

        self.assertFalse(self.orderObj.needs_shipped)
        self.assertFalse(self.orderObj.is_shipped)

        self.orderObj.status = Order.STATUS_PAID
        self.orderObj.save(update_fields=['status'])

        self.assertTrue(self.orderObj.needs_shipped)
        self.assertFalse(self.orderObj.is_shipped)

        response = self.client.get(reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 302)

        self.orderObj.refresh_from_db()

        self.assertFalse(self.orderObj.needs_shipped)
        self.assertTrue(self.orderObj.is_shipped)

    def test_dashboard_order_ship_view_marks_order_as_shipped(self):
        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        self.orderObj.status = Order.STATUS_PAID
        self.orderObj.save(update_fields=['status'])

        response = self.client.get(reverse('store:dashboard-order-ship', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 302)

        self.orderObj.refresh_from_db()

        self.assertEqual(self.orderObj.status, Order.STATUS_SHIPPED)


class DashboardOrderCompleteViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def setUp(self):
        super().setUp()

        self.staffObj = self.create_user('staff@example.com', 'secret', is_staff=True)

    def test_dashboard_order_complete_view_requires_staff(self):
        response = self.client.get(reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk}))

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk})}")

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 403)

        self.client.logout()

        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk}))

        self.assertRedirects(response, f"{reverse('store:dashboard')}?filter=")

    def test_dashboard_order_complete_view_preserves_filters(self):
        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        response = self.client.get(reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk}))

        self.assertRedirects(response, f"{reverse('store:dashboard')}?filter=")

        response = self.client.get(f"{reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk})}?filter=paid")

        self.assertRedirects(response, f"{reverse('store:dashboard')}?filter=paid")

    def test_dashboard_order_complete_view_only_completes_applicable_orders(self):
        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        self.assertFalse(self.orderObj.needs_completed)
        self.assertFalse(self.orderObj.is_complete)

        response = self.client.get(reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 302)

        self.orderObj.refresh_from_db()

        self.assertFalse(self.orderObj.needs_completed)
        self.assertFalse(self.orderObj.is_complete)

        self.orderObj.status = Order.STATUS_SHIPPED
        self.orderObj.save(update_fields=['status'])

        self.assertTrue(self.orderObj.needs_completed)
        self.assertFalse(self.orderObj.is_complete)

        response = self.client.get(reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 302)

        self.orderObj.refresh_from_db()

        self.assertFalse(self.orderObj.needs_completed)
        self.assertTrue(self.orderObj.is_complete)

    def test_dashboard_order_complete_view_marks_order_as_complete(self):
        self.assertTrue(self.client.login(email='staff@example.com', password='secret'))

        self.orderObj.status = Order.STATUS_SHIPPED
        self.orderObj.save(update_fields=['status'])

        response = self.client.get(reverse('store:dashboard-order-complete', kwargs={'order': self.orderObj.pk}))

        self.assertEqual(response.status_code, 302)

        self.orderObj.refresh_from_db()

        self.assertEqual(self.orderObj.status, Order.STATUS_COMPLETE)


class ProductListViewTests(StoreDataTestMixin, BaseTestCase):
    def setUp(self):
        super().setUp()

        self.productObj2 = self.create_product('Product 2', 'product-2', 'Product 2.', Money(Decimal(5.49), currency=CURRENCY))
        self.productvariantObj2 = self.create_productvariant('Product 2', self.productObj2)
        self.productObj3 = self.create_product('Product 3', 'product-3', 'Product 3.', Money(Decimal(19.99), currency=CURRENCY))
        self.productvariantObj3 = self.create_productvariant('Product 3', self.productObj3)

    def test_product_list_view_renders(self):
        response = self.client.get(reverse('store:product-list'))

        self.assertTemplateUsed(response, 'store/product_list.html')

        self.assertIn('products', response.context)
        self.assertIn('form', response.context)

    def test_product_list_view_filters(self):
        response = self.client.get(reverse('store:product-list'))

        self.assertEqual(response.context['products'].count(), 3)

        response = self.client.get(reverse('store:product-list'), {
            'categories': [self.categoryObj.pk],
        })

        self.assertEqual(response.context['products'].count(), 1)

        response = self.client.get(reverse('store:product-list'), {
            'attributes': [self.productattributeObj.pk],
        })

        self.assertEqual(response.context['products'].count(), 1)

        response = self.client.get(reverse('store:product-list'), {
            'price_low': 7.5,
        })

        self.assertEqual(response.context['products'].count(), 2)

        response = self.client.get(reverse('store:product-list'), {
            'price_low': 7.5,
            'price_high': 17.5,
        })

        self.assertEqual(response.context['products'].count(), 1)

        response = self.client.get(reverse('store:product-list'), {
            'price_high': 17.5,
            'search': '3',
        })

        self.assertEqual(response.context['products'].count(), 0)

        response = self.client.get(reverse('store:product-list'), {
            'search': '3',
        })

        self.assertEqual(response.context['products'].count(), 1)


class ProductDetailViewTests(StoreDataTestMixin, BaseTestCase):
    def test_product_detail_view_renders(self):
        response = self.client.get(reverse('store:product-detail', kwargs={'product': self.productObj.slug}))

        self.assertTemplateUsed(response, 'store/product_detail.html')

        self.assertIn('product', response.context)


class CartDetailViewTests(StoreDataTestMixin, BaseTestCase):
    def test_cart_detail_view_renders(self):
        response = self.client.get(reverse('store:cart-detail'))

        self.assertTemplateUsed(response, 'store/cart_detail.html')

        self.assertIn('cart', response.context)


class CartAddViewTests(StoreDataTestMixin, BaseTestCase):
    def test_cart_add_view_adds_item_to_cart(self):
        response = self.client.get(reverse('store:cart-detail'))

        self.assertEqual(response.context['cart'].count(), 0)

        response = self.client.post(reverse('store:cart-add'), {
            'product': self.productObj.pk,
            'variant': self.productvariantObj.pk,
            'quantity': 1,
        }, follow=True)

        self.assertEqual(response.context['cart'].count(), 1)

        response = self.client.post(reverse('store:cart-add'), {
            'product': self.productObj.pk,
            'variant': self.productvariantObj.pk,
            'quantity': 1,
        }, follow=True)

        self.assertEqual(response.context['cart'].count(), 2)


class CartUpdateViewTests(StoreDataTestMixin, BaseTestCase):
    def test_cart_update_view_updates_item_in_cart(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)]).serialize()
        session.save()

        response = self.client.get(reverse('store:cart-detail'))

        self.assertEqual(response.context['cart'].count(), 1)

        response = self.client.post(reverse('store:cart-update'), {
            'product': self.productObj.pk,
            'variant': self.productvariantObj.pk,
            'quantity': 2,
        }, follow=True)

        self.assertEqual(response.context['cart'].count(), 2)

        response = self.client.post(reverse('store:cart-update'), {
            'product': self.productObj.pk,
            'variant': self.productvariantObj.pk,
            'quantity': 0,
        }, follow=True)

        self.assertEqual(response.context['cart'].count(), 0)


class CartClearViewTests(StoreDataTestMixin, BaseTestCase):
    def test_cart_clear_view_clears_cart(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)]).serialize()
        session.save()

        response = self.client.get(reverse('store:cart-detail'))

        self.assertEqual(response.context['cart'].count(), 1)

        response = self.client.post(reverse('store:cart-clear'), follow=True)

        self.assertEqual(response.context['store_cart'].count(), 0)


class CartContentViewTests(StoreDataTestMixin, BaseTestCase):
    def test_cart_content_view_renders(self):
        response = self.client.get(reverse('store:cart-content'))

        self.assertTemplateUsed(response, 'store/cart_content.html')


class CheckoutStartViewTests(StoreDataTestMixin, BaseTestCase):
    def test_checkout_start_view_requires_items_in_cart(self):
        response = self.client.get(reverse('store:checkout-start'), follow=True)

        self.assertRedirects(response, reverse('store:cart-detail'))

        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)]).serialize()
        session.save()

        response = self.client.get(reverse('store:checkout-start'), follow=True)

        self.assertRedirects(response, reverse('store:checkout-contact'))

    def test_checkout_start_view_redirects_correctly(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)]).serialize()
        session.save()

        response = self.client.get(reverse('store:checkout-start'), follow=True)

        self.assertRedirects(response, reverse('store:checkout-contact'))

        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['possessed'])

        response = self.client.get(reverse('store:checkout-start'), follow=True)

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:checkout-start')}")


class CheckoutUserViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def test_checkout_user_view_redirects_correctly(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)]).serialize()
        session.save()

        response = self.client.get(reverse('store:checkout-user'), follow=True)

        self.assertRedirects(response, reverse('store:checkout-contact'))

        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['possessed'])

        response = self.client.get(reverse('store:checkout-user'), follow=True)

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:checkout-start')}")

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:checkout-user'), follow=True)

        self.assertRedirects(response, reverse('store:checkout-contact'))


class CheckoutContactViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def test_checkout_contact_view_requires_login_when_applicable(self):
        self.productvariantObj.possessed = True
        self.productvariantObj.save(update_fields=['possessed'])

        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)], data={'user': self.userObj.pk}).serialize()
        session.save()

        response = self.client.get(reverse('store:checkout-contact'), follow=True)

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:checkout-start')}")

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:checkout-contact'), follow=True)

        self.assertTemplateUsed(response, 'store/checkout_contact.html')

    def test_checkout_contact_view_form_fields_and_initial(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)]).serialize()
        session.save()

        response = self.client.get(reverse('store:checkout-contact'), follow=True)

        self.assertTemplateUsed(response, 'store/checkout_contact.html')

        self.assertIn('form', response.context)
        self.assertTrue(response.context['form'].shipping_required)
        self.assertEqual(len(response.context['form'].fields.keys()), 19)

        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])

        response = self.client.get(reverse('store:checkout-contact'), follow=True)

        self.assertTemplateUsed(response, 'store/checkout_contact.html')

        self.assertIn('form', response.context)
        self.assertFalse(response.context['form'].shipping_required)
        self.assertEqual(len(response.context['form'].fields.keys()), 9)
        self.assertIsNone(response.context['form'].fields['billing_email'].initial)

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:checkout-contact'), follow=True)

        self.assertTemplateUsed(response, 'store/checkout_contact.html')

        self.assertIn('form', response.context)
        self.assertEqual(response.context['form'].fields['billing_email'].initial, 'test@example.com')

    def test_checkout_contact_view_updates_order(self):
        Order.objects.all().delete()

        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)]).serialize()
        session.save()

        self.assertEqual(Order.objects.all().count(), 0)

        self.assertIsNone(self.client.session['store_cart']['order'])

        response = self.client.post(reverse('store:checkout-contact'), {
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_email': 'john@example.com',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
            'use_same_contacts': True,
        }, follow=True)

        self.assertRedirects(response, reverse('store:checkout-review'))

        self.assertEqual(Order.objects.all().count(), 1)

        self.assertIsNotNone(self.client.session['store_cart']['order'])

        orderObj = Order.objects.first()

        self.assertIsNotNone(orderObj.billing_address)
        self.assertIsNotNone(orderObj.shipping_address)
        self.assertEqual(orderObj.get_user_email, 'john@example.com')

    def test_checkout_contact_view_redirects_correctly(self):
        self.productvariantObj.shipped = False
        self.productvariantObj.save(update_fields=['shipped'])

        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)]).serialize()
        session.save()

        response = self.client.post(reverse('store:checkout-contact'), {
            'billing_first_name': 'John',
            'billing_last_name': 'Doe',
            'billing_email': 'john@example.com',
            'billing_address1': '1234 5th Street',
            'billing_address2': '',
            'billing_city': 'Two Dot',
            'billing_state': 'MT',
            'billing_zip_code': '59085',
            'billing_phone': '406-555-1234',
        }, follow=True)

        self.assertRedirects(response, reverse('store:checkout-review'))


class CheckoutReviewViewTests(StoreDataTestMixin, BaseTestCase):
    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='dummy')
    def test_checkout_review_view_payment(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)], self.orderObj).serialize()
        session.save()

        response = self.client.get(reverse('store:checkout-review'))

        self.assertTemplateUsed(response, 'store/checkout_review.html')

        self.assertIn('payment', response.context)
        self.assertIn('order', response.context)

        self.assertEqual(get_payment_model().objects.all().count(), 1)

        self.assertEqual(response.context['payment'].order, self.orderObj)
        self.assertEqual(response.context['payment'].description, f'#{self.orderObj.token_id}')
        self.assertLessEqual(abs(response.context['payment'].total - self.orderObj.get_total().amount), 0.001)
        self.assertEqual(response.context['payment'].delivery, self.orderObj.get_shipping().amount)

        payment_id = response.context['payment'].pk
        payment_amount = response.context['payment'].total

        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 2)], self.orderObj).serialize()
        session.save()

        response = self.client.get(reverse('store:checkout-review'))

        self.assertTemplateUsed(response, 'store/checkout_review.html')

        self.assertIn('payment', response.context)

        self.assertEqual(get_payment_model().objects.all().count(), 1)

        self.assertEqual(response.context['payment'].pk, payment_id)
        self.assertLessEqual(response.context['payment'].total - (payment_amount * 2), 0.001)

    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='dummy')
    def test_checkout_review_view_updates_order(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)], self.orderObj).serialize()
        session.save()

        self.assertEqual(self.orderObj.status, Order.STATUS_PENDING)

        self.client.post(reverse('store:checkout-review'), {
            'status': 'rejected',
            'fraud_status': 'accept',
            'gateway_response': '3ds-redirect',
            'verification_result': 'rejected',
        }, follow=True)

        self.orderObj.refresh_from_db()

        self.assertEqual(self.orderObj.status, Order.STATUS_PAYMENT_ERROR)
        self.assertIsNone(self.orderObj.total_price)
        self.assertIsNone(self.orderObj.total_shipping)

        self.client.post(reverse('store:checkout-review'), {
            'status': 'confirmed',
            'fraud_status': 'accept',
            'gateway_response': '3ds-redirect',
            'verification_result': 'confirmed',
        }, follow=True)

        self.orderObj.refresh_from_db()

        self.assertEqual(self.orderObj.status, Order.STATUS_PAID)
        self.assertIsNotNone(self.orderObj.total_price)
        self.assertIsNotNone(self.orderObj.total_shipping)

    @override_settings(STORE_DEFAULT_PAYMENT_VARIANT='dummy')
    def test_checkout_review_view_redirects_correctly(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)], self.orderObj).serialize()
        session.save()

        response = self.client.post(reverse('store:checkout-review'), {
            'status': 'rejected',
            'fraud_status': 'accept',
            'gateway_response': '3ds-redirect',
            'verification_result': 'rejected',
        }, follow=True)

        # unsuccessful payment goes back to the checkout review page
        self.assertRedirects(response, reverse('store:checkout-review'))

        response = self.client.post(reverse('store:checkout-review'), {
            'status': 'confirmed',
            'fraud_status': 'accept',
            'gateway_response': '3ds-redirect',
            'verification_result': 'confirmed',
        }, follow=True)

        # successful payment goes to the order detail page
        self.assertRedirects(response, self.orderObj.get_absolute_url())


class CheckoutPaymentFailedViewTests(StoreDataTestMixin, BaseTestCase):
    def test_checkout_payment_failed_view_redirects_correctly(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)], self.orderObj).serialize()
        session.save()

        self.assertEqual(self.orderObj.status, Order.STATUS_PENDING)

        response = self.client.get(reverse('store:checkout-payment-failed'), follow=True)

        self.orderObj.refresh_from_db()
        self.assertEqual(self.orderObj.status, Order.STATUS_PAYMENT_ERROR)

        self.assertRedirects(response, reverse('store:checkout-review'))


class CheckoutPaymentSuccessViewTests(StoreDataTestMixin, BaseTestCase):
    def test_checkout_payment_success_view_redirects_correctly(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)], self.orderObj).serialize()
        session.save()

        self.assertEqual(self.orderObj.status, Order.STATUS_PENDING)

        response = self.client.get(reverse('store:checkout-payment-success'), follow=True)

        self.assertRedirects(response, self.orderObj.get_absolute_url())

        self.orderObj.refresh_from_db()
        self.assertEqual(self.orderObj.status, Order.STATUS_PENDING)

        paymentObj = self.create_payment(self.orderObj, 'dummy', self.orderObj.token_id)
        paymentObj.status = PaymentStatus.CONFIRMED
        paymentObj.total = self.orderObj.get_total().amount
        paymentObj.captured_amount = self.orderObj.get_total().amount
        paymentObj.save(update_fields=['status', 'total', 'captured_amount'])

        response = self.client.get(reverse('store:checkout-payment-success'), follow=True)

        self.orderObj.refresh_from_db()
        self.assertEqual(self.orderObj.status, Order.STATUS_PAID)

        self.assertRedirects(response, self.orderObj.get_absolute_url())


class CheckoutCancelViewTests(StoreDataTestMixin, BaseTestCase):
    def test_checkout_cancel_view_clears_order_but_maintains_cart(self):
        session = self.client.session
        session['store_cart'] = Cart([CartItem(self.productvariantObj, 1)], self.orderObj).serialize()
        session.save()

        response = self.client.get(reverse('store:checkout-cancel'), follow=True)

        self.assertRedirects(response, reverse('store:product-list'))

        self.assertEqual(response.context['store_cart'].count(), 1)
        self.assertIsNone(response.context['store_cart'].order)


class OrderListViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def test_order_list_view_requires_user(self):
        response = self.client.get(reverse('store:order-list'))

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:order-list')}")

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:order-list'))

        self.assertTemplateUsed(response, 'store/order_list.html')

    def test_order_list_view_queryset(self):
        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        self.assertIsNone(self.orderObj.user)

        response = self.client.get(reverse('store:order-list'))

        self.assertTemplateUsed(response, 'store/order_list.html')

        self.assertIn('orders', response.context)

        self.assertEqual(response.context['orders'].count(), 0)

        self.orderObj.user = self.userObj
        self.orderObj.save(update_fields=['user'])

        response = self.client.get(reverse('store:order-list'))

        self.assertEqual(response.context['orders'].count(), 1)


class OrderDetailViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def test_order_detail_view_access(self):
        response = self.client.get(self.orderObj.get_absolute_url())

        self.assertTemplateUsed(response, 'store/order_detail.html')

        self.orderObj.token_created = self.orderObj.token_created - datetime.timedelta(days=10)
        self.orderObj.save(update_fields=['token_created'])

        response = self.client.get(self.orderObj.get_absolute_url())

        self.assertEqual(response.status_code, 404)

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(self.orderObj.get_absolute_url())

        self.assertEqual(response.status_code, 403)

        self.orderObj.user = self.userObj
        self.orderObj.save(update_fields=['user'])

        response = self.client.get(self.orderObj.get_absolute_url())

        self.assertTemplateUsed(response, 'store/order_detail.html')

    def test_order_detail_view_can_cancel_order(self):
        response = self.client.get(self.orderObj.get_absolute_url())

        self.assertTemplateUsed(response, 'store/order_detail.html')

        self.assertIn('can_cancel_order', response.context)
        self.assertFalse(response.context['can_cancel_order'])

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(self.orderObj.get_absolute_url())

        self.assertTemplateUsed(response, 'store/order_detail.html')

        self.assertIn('can_cancel_order', response.context)
        self.assertFalse(response.context['can_cancel_order'])

        self.orderObj.user = self.userObj
        self.orderObj.save(update_fields=['user'])

        response = self.client.get(self.orderObj.get_absolute_url())

        self.assertTemplateUsed(response, 'store/order_detail.html')

        self.assertIn('can_cancel_order', response.context)
        self.assertTrue(response.context['can_cancel_order'])


class OrderCancelViewTests(StoreDataTestMixin, UserDataMixin, BaseTestCase):
    def test_order_cancel_view_requires_user(self):
        self.orderObj.user = self.userObj
        self.orderObj.save(update_fields=['user'])

        response = self.client.get(reverse('store:order-cancel', kwargs={'token': self.orderObj.token_id}))

        self.assertRedirects(response, f"{reverse('accounts:login')}?next={reverse('store:order-cancel', kwargs={'token': self.orderObj.token_id})}")

        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:order-cancel', kwargs={'token': self.orderObj.token_id}))

        self.assertRedirects(response, self.orderObj.get_absolute_url())

    def test_order_cancel_view_only_cancels_when_applicable(self):
        self.assertTrue(self.client.login(email='test@example.com', password='secret'))

        response = self.client.get(reverse('store:order-cancel', kwargs={'token': self.orderObj.token_id}))

        self.assertEqual(response.status_code, 403)

        self.orderObj.status = Order.STATUS_SHIPPED
        self.orderObj.user = self.userObj
        self.orderObj.save(update_fields=['status', 'user'])

        response = self.client.get(reverse('store:order-cancel', kwargs={'token': self.orderObj.token_id}))

        self.assertRedirects(response, self.orderObj.get_absolute_url())

        self.orderObj.refresh_from_db()

        self.assertEqual(self.orderObj.status, Order.STATUS_SHIPPED)

        self.orderObj.status = Order.STATUS_PAID
        self.orderObj.save(update_fields=['status'])

        response = self.client.get(reverse('store:order-cancel', kwargs={'token': self.orderObj.token_id}))

        self.assertRedirects(response, self.orderObj.get_absolute_url())

        self.orderObj.refresh_from_db()

        self.assertEqual(self.orderObj.status, Order.STATUS_CANCELLED)
