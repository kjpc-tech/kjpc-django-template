from django.urls import path

from store import views

app_name = 'store'


urlpatterns = [
    path('dashboard/', views.DashboardView.as_view(), name='dashboard'),
    path('dashboard/<int:order>/', views.DashboardOrderView.as_view(), name='dashboard-order'),
    path('dashboard/<int:order>/ship/', views.DashboardOrderShipView.as_view(), name='dashboard-order-ship'),
    path('dashboard/<int:order>/complete/', views.DashboardOrderCompleteView.as_view(), name='dashboard-order-complete'),

    path('cart/', views.CartDetailView.as_view(), name='cart-detail'),
    path('cart/add/', views.CartAddView.as_view(), name='cart-add'),
    path('cart/update/', views.CartUpdateView.as_view(), name='cart-update'),
    path('cart/clear/', views.CartClearView.as_view(), name='cart-clear'),
    path('cart/content/', views.cart_content, name='cart-content'),

    path('checkout/', views.CheckoutStartView.as_view(), name='checkout-start'),
    path('checkout/user/', views.CheckoutUserView.as_view(), name='checkout-user'),
    path('checkout/contact/', views.CheckoutContactView.as_view(), name='checkout-contact'),
    path('checkout/review/', views.CheckoutReviewView.as_view(), name='checkout-review'),
    path('checkout/payment/failed/', views.CheckoutPaymentFailedView.as_view(), name='checkout-payment-failed'),
    path('checkout/payment/success/', views.CheckoutPaymentSuccessView.as_view(), name='checkout-payment-success'),
    path('checkout/cancel/', views.CheckoutCancelView.as_view(), name='checkout-cancel'),

    path('orders/', views.OrderListView.as_view(), name='order-list'),
    path('orders/<slug:token>/', views.OrderDetailView.as_view(), name='order-detail'),
    path('orders/<slug:token>/cancel/', views.OrderCancelView.as_view(), name='order-cancel'),

    path('<slug:product>/', views.ProductDetailView.as_view(), name='product-detail'),
    path('', views.ProductListView.as_view(), name='product-list'),
]
