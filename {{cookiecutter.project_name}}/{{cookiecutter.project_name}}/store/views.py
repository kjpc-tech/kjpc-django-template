import logging

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.mixins import (
    LoginRequiredMixin, UserPassesTestMixin,
)
from django.core.exceptions import PermissionDenied
from django.db import transaction
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.views.generic import (
    DetailView, FormView, ListView, RedirectView, TemplateView,
)
from django.urls import reverse, reverse_lazy

from payments import get_payment_model, PaymentStatus, RedirectNeeded

from .models import Product, Order
from .forms import (
    ProductFilterForm, CartAddForm, CartUpdateForm,
    CheckoutContactForm,
)
from .checkout import Checkout

LOGGER_STORE = logging.getLogger('{{ cookiecutter.project_name }}.store')


class StoreDashboardMixin(UserPassesTestMixin):
    """
    Require a staff member.
    """
    def test_func(self):
        return self.request.user.is_staff


class DashboardView(StoreDashboardMixin, ListView):
    """
    Show a dashboard of orders. Only available to staff.
    """
    model = Order
    context_object_name = 'orders'
    template_name = 'store/dashboard.html'
    paginate_by = 50

    def _get_filter(self):
        return self.request.GET.get('filter', '')

    def get_queryset(self):
        queryset = super().get_queryset()

        _filter = self._get_filter()

        if _filter == 'pending':
            queryset = queryset.filter(status=Order.STATUS_PENDING)
        elif _filter == 'cancelled':
            queryset = queryset.filter(status=Order.STATUS_CANCELLED)
        elif _filter == 'reviewed':
            queryset = queryset.filter(status=Order.STATUS_REVIEWED)
        elif _filter == 'paid':
            queryset = queryset.filter(status=Order.STATUS_PAID)
        elif _filter == 'payment_error':
            queryset = queryset.filter(status=Order.STATUS_PAYMENT_ERROR)
        elif _filter == 'shipped':
            queryset = queryset.filter(status=Order.STATUS_SHIPPED)
        elif _filter == 'completed':
            queryset = queryset.filter(status=Order.STATUS_COMPLETE)
        elif _filter == 'all':
            pass
        else:  # default
            queryset = queryset.filter(
                status__in=[Order.STATUS_PAID, Order.STATUS_SHIPPED],
            )

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['filter'] = self._get_filter()

        return context


class DashboardOrderView(StoreDashboardMixin, DetailView):
    """
    Show order details. Only available to staff.
    """
    model = Order
    pk_url_kwarg = 'order'
    context_object_name = 'order'
    template_name = 'store/dashboard_order.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['filter'] = self.request.GET.get('filter', '')

        return context


class DashboardOrderShipView(StoreDashboardMixin, RedirectView):
    """
    Mark an order as shipped. Only available to staff.
    """
    def get_redirect_url(self, *args, **kwargs):
        orderObj = get_object_or_404(Order, pk=self.kwargs.get('order'))

        shipped = orderObj.ship()

        if shipped:
            messages.success(self.request, f'Order #{orderObj.token_id} has been marked as shipped.')

            try:
                orderObj.send_shipped_email()
            except Exception as e:
                LOGGER_STORE.error(f'DashboardOrderShipView.get_redirect_url: {e}')

        _filter = self.request.GET.get('filter', '')

        return f"{reverse('store:dashboard')}?filter={_filter}"


class DashboardOrderCompleteView(StoreDashboardMixin, RedirectView):
    """
    Mark an order as complete. Only available to staff.
    """
    def get_redirect_url(self, *args, **kwargs):
        orderObj = get_object_or_404(Order, pk=self.kwargs.get('order'))

        completed = orderObj.complete()

        if completed:
            messages.success(self.request, f'Order #{orderObj.token_id} has been marked as complete.')

        _filter = self.request.GET.get('filter', '')

        return f"{reverse('store:dashboard')}?filter={_filter}"


class ProductListView(ListView):
    """
    Show all products. May be filtered.
    """
    model = Product
    context_object_name = 'products'

    def get_queryset(self):
        queryset = super().get_queryset()

        form = self._get_form()

        queryset = form.filter_queryset(queryset)

        queryset = queryset.prefetch_related('images', 'variants__images')

        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['form'] = self._get_form()

        return context

    def _get_form(self):
        form = ProductFilterForm(self.request.GET)

        if not form.is_valid():
            form = ProductFilterForm()

        return form


class ProductDetailView(DetailView):
    model = Product
    context_object_name = 'product'
    slug_url_kwarg = 'product'


class CartViewMixin:
    @property
    def cart(self):
        return self.request.store_cart


class CartDetailView(CartViewMixin, TemplateView):
    template_name = 'store/cart_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['cart'] = self.cart

        return context


class CartAddView(CartViewMixin, FormView):
    form_class = CartAddForm
    template_name = 'store/cart_detail.html'
    success_url = reverse_lazy('store:cart-detail')

    def form_invalid(self, form):
        if self.request.is_ajax():
            return JsonResponse({
                'status': 'error',
                'product': self.request.POST.get('product'),
            })
        else:
            return super().form_invalid(form)

    def form_valid(self, form):
        product = form.cleaned_data.get('variant')
        quantity = form.cleaned_data.get('quantity')

        # add item to cart
        self.cart.add(product, quantity)

        if self.request.is_ajax():
            return JsonResponse({
                'status': 'added',
                'product': product.product_id,
            })
        else:
            return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['cart'] = self.cart

        return context


class CartUpdateView(CartViewMixin, FormView):
    form_class = CartUpdateForm
    template_name = 'store/cart_detail.html'
    success_url = reverse_lazy('store:cart-detail')

    def form_valid(self, form):
        product = form.cleaned_data.get('variant')
        quantity = form.cleaned_data.get('quantity')

        # update item in cart
        self.cart.add(product, quantity, replace=True)

        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['cart'] = self.cart

        return context


class CartClearView(CartViewMixin, RedirectView):
    """
    Empty the cart.
    """
    def get_redirect_url(self, *args, **kwargs):
        self.cart.clear()

        messages.success(self.request, "Your cart was emptied successfully.")

        return reverse('store:product-list')


def cart_content(request):
    return render(request, 'store/cart_content.html')


class CheckoutViewMixin:
    _checkout = None

    @property
    def checkout(self):
        if self._checkout is None:
            self._checkout = Checkout(self.request.store_cart)

        return self._checkout


class CheckoutStartView(CheckoutViewMixin, CartViewMixin, RedirectView):
    """
    Create checkout. Redirect as necessary.
    """
    def get_redirect_url(self, *args, **kwargs):
        if not self.cart.count():
            # don't checkout an empty cart
            messages.warning(self.request, 'You cannot checkout an empty cart.')

            return reverse('store:cart-detail')

        return self.checkout.get_next_step().get_absolute_url()


class CheckoutUserView(CheckoutViewMixin, CartViewMixin, RedirectView):
    """
    Either login or create a new user.
    """
    def get_redirect_url(self, *args, **kwargs):
        if not self.cart.is_user_required or self.request.user.is_authenticated:
            self.cart.data['user'] = self.request.user.pk

            return self.checkout.get_next_step().get_absolute_url()
        else:
            return f"{reverse('accounts:login')}?next={reverse('store:checkout-start')}"


class CheckoutContactView(CheckoutViewMixin, CartViewMixin, FormView):
    """
    Enter contact information.
    """
    form_class = CheckoutContactForm
    template_name = 'store/checkout_contact.html'

    def dispatch(self, request, *args, **kwargs):
        # if user logged out, remove them from the cart
        if self.cart.data.get('user') and not self.request.user.is_authenticated:
            del self.cart.data['user']

            return redirect(self.get_success_url())

        return super().dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()

        kwargs['user'] = self.request.user
        kwargs['shipping_required'] = self.cart.is_shipping_required

        return kwargs

    def form_valid(self, form):
        email_address = form.cleaned_data.get('billing_email')
        billing_address = form.get_billing_address()
        shipping_address = form.get_shipping_address()

        # create/update order with contact information
        if self.cart.order is None:
            self.cart.order = Order.create_from_cart(self.cart, email_address, billing_address, shipping_address, self.request.user)
        else:
            self.cart.order.billing_address = billing_address
            self.cart.order.shipping_address = shipping_address
            if self.request.user.is_authenticated:
                self.cart.order.user = self.request.user
            else:
                self.cart.order.anonymous_user_email = email_address
            self.cart.order.save()

        return super().form_valid(form)

    def get_success_url(self, *args, **kwargs):
        return self.checkout.get_next_step().get_absolute_url()


class CheckoutReviewView(CheckoutViewMixin, CartViewMixin, DetailView):
    """
    Show order summary with prompt to pay/complete or cancel.
    """
    context_object_name = 'order'
    template_name = 'store/checkout_review.html'
    _order = None

    def get_object(self, queryset=None):
        if self._order is None:
            if self.cart.order is None:
                raise Http404

            # make sure order is up-to-date
            self.cart.order.update_order_with_cart(self.cart)

            self._order = self.cart.order

        return self._order

    def get(self, request, *args, **kwargs):
        self.get_object()  # ensure order is up to date

        _redirect = self._handle_payment()

        if _redirect:
            return redirect(_redirect)

        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        orderObj = self.get_object()  # ensure order is up to date

        orderObj.update_status(Order.STATUS_REVIEWED)

        _redirect = self._handle_payment(request.POST)

        if _redirect:
            return redirect(_redirect)

        return redirect(self.checkout.get_next_step().get_absolute_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['payment'] = self._payment
        context['form'] = self.payment_form

        if self._payment.status in [
            PaymentStatus.REJECTED,
            PaymentStatus.ERROR,
        ]:
            # mark the order as having a payment error
            self.get_object().update_status(Order.STATUS_PAYMENT_ERROR)

            messages.error(self.request, 'Payment unsuccessful.')

        return context

    def _get_payment(self):
        orderObj = self.get_object()

        # create/update the payment
        self._payment = get_payment_model().objects.get_or_create(
            order=self.cart.order,
            variant=getattr(settings, 'STORE_DEFAULT_PAYMENT_VARIANT', 'stripe'),
            description=f'#{self.cart.order.token_id}',
            currency=getattr(settings, 'STORE_CURRENCY', 'USD'),
        )[0]
        self._payment.billing_first_name = orderObj.billing_address.first_name
        self._payment.billing_last_name = orderObj.billing_address.last_name
        self._payment.billing_address_1 = orderObj.billing_address.address1
        self._payment.billing_address_2 = orderObj.billing_address.address2
        self._payment.billing_city = orderObj.billing_address.city
        self._payment.billing_postcode = orderObj.billing_address.zip_code
        self._payment.billing_country_code = 'US'
        self._payment.billing_country_area = orderObj.billing_address.state
        self._payment.billing_email = orderObj.get_user_email
        self._payment.customer_ip_address = self.request.META.get('REMOTE_ADDR')
        self._payment.total = orderObj.get_grand_total().amount
        self._payment.tax = orderObj.get_tax().amount
        self._payment.delivery = orderObj.get_shipping().amount
        self._payment.save()

        return self._payment

    def _handle_payment(self, data=None):
        try:
            self.payment_form = self._get_payment().get_form(data)
        except RedirectNeeded as redirect_to:
            return str(redirect_to)

        return None


class CheckoutPaymentFailedView(CheckoutViewMixin, CartViewMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        # mark the order as having a payment error
        self.cart.order.update_status(Order.STATUS_PAYMENT_ERROR)

        messages.error(self.request, 'Payment unsuccessful.')

        return self.checkout.get_next_step().get_absolute_url()


class CheckoutPaymentSuccessView(CheckoutViewMixin, CartViewMixin, RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        orderObj = self.cart.order

        if orderObj.is_fully_paid():
            with transaction.atomic():
                # mark the order as paid
                orderObj.update_status(Order.STATUS_PAID)

                # store the order totals
                orderObj.total_price = orderObj.get_total()
                orderObj.total_tax = orderObj.get_tax()
                orderObj.total_shipping = orderObj.get_shipping()
                orderObj.save(update_fields=['total_price', 'total_tax', 'total_shipping'])

            messages.success(self.request, 'Payment successful.')
            messages.success(self.request, 'Your order has been recieved.')

            # send a confirmation email
            try:
                orderObj.send_confirmation_email()
            except Exception as e:
                LOGGER_STORE.error(f'CheckoutPaymentSuccessView.get_redirect_url: {e}')

            # clear the cart
            self.cart.clear(cancel_order=False)

        return orderObj.get_absolute_url()


class CheckoutCancelView(CheckoutViewMixin, CartViewMixin, RedirectView):
    """
    Redirect to store page with cancelled message.
    """
    def get_redirect_url(self, *args, **kwargs):
        # clear order, but keep items in cart
        self.cart.clear_order()

        messages.info(self.request, 'Your order has been cancelled.')

        return reverse('store:product-list')


class OrderListView(LoginRequiredMixin, ListView):
    """
    Show an authenticated user their orders.
    """
    model = Order
    context_object_name = 'orders'

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user)


class OrderDetailView(DetailView):
    """
    Show a user their order details. Amount of detail depends on authentication.
    """
    model = Order
    context_object_name = 'order'

    def get_object(self, queryset=None):
        queryset = queryset or self.get_queryset()

        try:
            orderObj = queryset.get(token_id=self.kwargs.get('token'))
        except Order.DoesNotExist:
            raise Http404

        # new orders can be viewed by token
        if orderObj.can_access_by_token():
            return orderObj

        # older orders must be viewed by authentication
        if self.request.user.is_authenticated:
            if self.request.user == orderObj.user:
                return orderObj
            else:
                raise PermissionDenied

        raise Http404

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        context['can_cancel_order'] = self.object.can_user_cancel(self.request.user)

        return context


class OrderCancelView(LoginRequiredMixin, RedirectView):
    """
    Allow an authenticated user to cancel their (uncompleted) order.
    """
    def get_redirect_url(self, *args, **kwargs):
        orderObj = get_object_or_404(Order, token_id=self.kwargs.get('token'))

        # make sure the user owns this order
        if orderObj.user != self.request.user:
            raise PermissionDenied

        # cancel the order if applicable
        if orderObj.can_user_cancel(self.request.user):
            cancelled = orderObj.cancel_if_applicable()

            if cancelled:
                messages.success(self.request, "Your order was cancelled.")

        return orderObj.get_absolute_url()
