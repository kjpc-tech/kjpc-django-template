from wagtail.contrib.modeladmin.options import (
    ModelAdmin, ModelAdminGroup, modeladmin_register,
)

from .models import (
    Category, ProductAttribute, Product, ProductVariant,
    Address, Order,
)
from .admin import (
    CategoryAdminMixin, ProductAttributeAdminMixin,
    ProductAdminMixin, AddressAdminMixin, OrderAdminMixin,
)


class CategoryAdmin(CategoryAdminMixin, ModelAdmin):
    model = Category
    menu_icon = 'code'


class ProductAttributeAdmin(ProductAttributeAdminMixin, ModelAdmin):
    model = ProductAttribute
    menu_icon = 'openquote'


class ProductAdmin(ProductAdminMixin, ModelAdmin):
    model = Product
    menu_icon = 'tag'


class ProductVariantAdmin(ModelAdmin):
    model = ProductVariant
    menu_icon = 'tag'
    list_display = ['product', 'name', 'get_price_per_item', 'is_available']
    search_fields = ['product__name', 'name']


class AddressAdmin(AddressAdminMixin, ModelAdmin):
    model = Address
    menu_icon = 'user'


class OrderAdmin(OrderAdminMixin, ModelAdmin):
    model = Order
    menu_icon = 'form'


class StoreAdminGroup(ModelAdminGroup):
    menu_label = 'Store'
    menu_icon = 'list-ul'
    items = [
        CategoryAdmin,
        ProductAttributeAdmin,
        ProductAdmin,
        ProductVariantAdmin,
        AddressAdmin,
        OrderAdmin,
    ]


modeladmin_register(StoreAdminGroup)
