from modelcluster.models import ClusterableModel

from wagtail.admin.edit_handlers import FieldPanel, InlinePanel

from {{ cookiecutter.project_name }}.models_abstract import AbstractSoftModel

from .models_abstract import AbstractOrderableSoftModel


class AbstractClusterableSoftModel(ClusterableModel, AbstractSoftModel):
    class Meta(AbstractSoftModel.Meta):
        abstract = True


class AbstractClusterableOrderableSoftModel(ClusterableModel, AbstractOrderableSoftModel):
    class Meta(AbstractOrderableSoftModel.Meta):
        abstract = True


class CategoryCMSMixin:
    panels = [
        FieldPanel('name'),
        FieldPanel('slug'),
        FieldPanel('description'),
        FieldPanel('parent'),
    ]


class ProductAttributeCMSMixin:
    panels = [
        FieldPanel('name'),
    ]


class ProductCMSMixin:
    panels = [
        FieldPanel('name'),
        FieldPanel('slug'),
        FieldPanel('description'),
        FieldPanel('categories'),
        FieldPanel('price'),
        FieldPanel('weight'),
        FieldPanel('attributes'),
        FieldPanel('available'),
        InlinePanel('images', label='Images'),
    ]


class ProductCMSModel(ProductCMSMixin, AbstractClusterableOrderableSoftModel):
    class Meta(AbstractClusterableOrderableSoftModel.Meta):
        abstract = True


class ProductImageCMSMixin:
    panels = [
        FieldPanel('image'),
        FieldPanel('alternative_text'),
    ]


class ProductImageCMSModel(ProductImageCMSMixin, AbstractOrderableSoftModel):
    class Meta(AbstractOrderableSoftModel.Meta):
        abstract = True


class ProductVariantCMSMixin:
    panels = [
        FieldPanel('product'),
        FieldPanel('name'),
        FieldPanel('sku'),
        FieldPanel('price_override'),
        FieldPanel('weight_override'),
        FieldPanel('images'),
        FieldPanel('available'),
        FieldPanel('shipped'),
        FieldPanel('possessed'),
        FieldPanel('singular'),
    ]


class ProductVariantCMSModel(ProductVariantCMSMixin, AbstractOrderableSoftModel):
    class Meta(AbstractOrderableSoftModel.Meta):
        abstract = True


class AddressCMSMixin:
    panels = [
        FieldPanel('first_name'),
        FieldPanel('last_name'),
        FieldPanel('address1'),
        FieldPanel('address2'),
        FieldPanel('city'),
        FieldPanel('state'),
        FieldPanel('zip_code'),
        FieldPanel('phone'),
        FieldPanel('user'),
    ]


class OrderCMSMixin:
    panels = [
        FieldPanel('status'),
        FieldPanel('user'),
        FieldPanel('billing_address'),
        FieldPanel('shipping_address'),
        FieldPanel('anonymous_user_email'),
        FieldPanel('total_price'),
        FieldPanel('total_tax'),
        FieldPanel('total_shipping'),
        FieldPanel('last_email_sent_at'),
        InlinePanel('items', label='Order Items'),
        InlinePanel('payments', label='Payments'),
    ]


class OrderCMSModel(OrderCMSMixin, AbstractClusterableSoftModel):
    class Meta:
        abstract = True


class OrderItemCMSMixin:
    panels = [
        FieldPanel('product'),
        FieldPanel('variant'),
        FieldPanel('product_name'),
        FieldPanel('product_sku'),
        FieldPanel('quantity'),
        FieldPanel('unit_price'),
    ]


class PaymentCMSMixin:
    panels = [
        FieldPanel('variant'),
        FieldPanel('status'),
        FieldPanel('fraud_status'),
        FieldPanel('currency'),
        FieldPanel('total'),
        FieldPanel('tax'),
        FieldPanel('delivery'),
        FieldPanel('description'),
        FieldPanel('billing_first_name'),
        FieldPanel('billing_last_name'),
        FieldPanel('billing_address_1'),
        FieldPanel('billing_address_2'),
        FieldPanel('billing_city'),
        FieldPanel('billing_postcode'),
        FieldPanel('billing_email'),
        FieldPanel('customer_ip_address'),
        FieldPanel('captured_amount'),
    ]
