"""
ASGI config for {{ cookiecutter.project_name }} project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
{% if cookiecutter.use_channels == 'y' %}http://channels.readthedocs.io/en/latest/deploying.html
"""

import os

import django

from channels.routing import get_default_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{ cookiecutter.project_name }}.settings.production")

django.setup()

application = get_default_application()
{% else %}https://docs.djangoproject.com/en/{{ '{{' }} docs_version {{ '}}' }}/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "{{ cookiecutter.project_name }}.settings.production")

application = get_asgi_application()
{% endif %}
