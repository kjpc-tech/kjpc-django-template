from django.conf import settings


def base(request):
    return {
        'is_debug': settings.DEBUG,
        'is_registration_open': settings.REGISTRATION_OPEN,
    }
