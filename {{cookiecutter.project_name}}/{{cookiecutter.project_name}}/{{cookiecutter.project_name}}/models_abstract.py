from django.db import models
from django.utils import timezone


class SoftModelManagerMixin:
    def get_queryset(self):
        return super().get_queryset().filter(deleted__isnull=True)

    def all_with_deleted(self):
        return super().get_queryset()

    def deleted(self):
        return super().get_queryset().filter(deleted__isnull=False)

    def hard_delete(self):
        return self.get_queryset().hard_delete()


class SoftModelManager(SoftModelManagerMixin, models.Manager):
    pass


class AbstractSoftModelMixin:
    def delete(self):
        self.deleted = timezone.now()
        self.save()

    def hard_delete(self):
        super().delete()


class AbstractSoftModel(AbstractSoftModelMixin, models.Model):
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    deleted = models.DateTimeField(blank=True, null=True)

    objects = SoftModelManager()

    class Meta:
        abstract = True
