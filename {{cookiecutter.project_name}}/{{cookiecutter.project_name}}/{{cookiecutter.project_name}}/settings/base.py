"""
Django settings for {{ cookiecutter.project_name }} project. (Django {{ '{{' }} django_version {{ '}}' }})

For more information on this file, see
https://docs.djangoproject.com/en/{{ '{{' }} docs_version {{ '}}' }}/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/{{ '{{' }} docs_version {{ '}}' }}/ref/settings/
"""

import logging
import os

from environ import Env


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BASE_DIR = os.path.dirname(PROJECT_DIR)
ROOT_DIR = os.path.dirname(BASE_DIR)

# django-environ
env = Env()
env.read_env(env_file=os.path.join(BASE_DIR, '.env'))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/{{ '{{' }} docs_version {{ '}}' }}/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY')

ALLOWED_HOSTS = env('ALLOWED_HOSTS').split(',')

# Application definition
INSTALLED_APPS = [
    # Django apps
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',{% if cookiecutter.use_geodjango == 'y' %}
    'django.contrib.gis',{% endif %}
    # 3rd party apps
    'django_registration',
    'webpack_loader',
    'bootstrap4',{% if cookiecutter.use_celery == 'y' %}
    'django_celery_results',
    'django_celery_beat',{% endif %}{% if cookiecutter.use_channels == 'y' %}
    'channels',{% endif %}{% if cookiecutter.use_drf == 'y' %}
    'rest_framework',{% endif %}{% if cookiecutter.use_wagtail == 'y' %}
    'wagtail.contrib.forms',
    'wagtail.contrib.redirects',
    'wagtail.contrib.modeladmin',
    'wagtail.embeds',
    'wagtail.sites',
    'wagtail.users',
    'wagtail.snippets',
    'wagtail.documents',
    'wagtail.images',
    'wagtail.admin',
    'wagtail.core',
    'modelcluster',
    'taggit',{% endif %}{% if cookiecutter.use_ecommerce == 'y' %}
    'localflavor',
    'mptt',
    'orderable',
    'phonenumber_field',
    'django_prices',
    'payments',
    'sorl.thumbnail',{% endif %}
    # Local apps
    'accounts',{% if cookiecutter.use_wagtail == 'y' %}
    'frontend',{% endif %}{% if cookiecutter.use_ecommerce == 'y' %}
    'store',{% endif %}
]

# Databases
{% if cookiecutter.use_geodjango == 'y' and cookiecutter.db != 'postgresql' %}
SPATIALITE_LIBRARY_PATH = 'mod_spatialite.so'
{% endif %}
DATABASES = {
    'default': {
        {% if cookiecutter.db == 'postgresql' %}'ENGINE': '{% if cookiecutter.use_geodjango == 'y' %}django.contrib.gis.db.backends.postgis{% else %}django.db.backends.postgresql{% endif %}',
        'HOST': env('DATABASE_HOST'),
        'PORT': env('DATABASE_PORT'),
        'NAME': env('DATABASE_NAME'),
        'USER': env('DATABASE_USER'),
        'PASSWORD': env('DATABASE_PASSWORD'),{% else %}'ENGINE': '{% if cookiecutter.use_geodjango == 'y' %}django.contrib.gis.db.backends.spatialite{% else %}django.db.backends.sqlite3{% endif %}',
        'NAME': env('DATABASE_NAME'),{% endif %}{% if cookiecutter.use_channels == 'y' and cookiecutter.db != 'postgresql' %}
        'TEST': {
            'NAME': os.path.join(ROOT_DIR, f"test_{env('DATABASE_NAME')}"),
        },{% endif %}
    },
}

# Middleware
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',{% if cookiecutter.use_wagtail == 'y' %}
    'wagtail.core.middleware.SiteMiddleware',
    'wagtail.contrib.redirects.middleware.RedirectMiddleware',{% endif %}{% if cookiecutter.use_ecommerce == 'y' %}
    'store.middleware.StoreMiddleware',{% endif %}
]

ROOT_URLCONF = '{{ cookiecutter.project_name }}.urls'

# Templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(PROJECT_DIR, "templates"),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                '{{ cookiecutter.project_name }}.context_processors.base',{% if cookiecutter.use_ecommerce == 'y' %}
                'store.context_processors.store',{% endif %}
            ],
        },
    },
]

WSGI_APPLICATION = '{{ cookiecutter.project_name }}.wsgi.application'

# Password validation
# https://docs.djangoproject.com/en/{{ '{{' }} docs_version {{ '}}' }}/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTH_USER_MODEL = 'accounts.User'

# Internationalization
# https://docs.djangoproject.com/en/{{ '{{' }} docs_version {{ '}}' }}/topics/i18n/
LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/{{ '{{' }} docs_version {{ '}}' }}/howto/static-files/
STATICFILES_DIRS = [
    os.path.join(PROJECT_DIR, "static"),
    os.path.join(ROOT_DIR, "webpack", "assets"),
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(ROOT_DIR, 'static_files/')

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(ROOT_DIR, 'media_files/')

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

{% if cookiecutter.cache == 'redis' or cookiecutter.use_celery == 'y' or cookiecutter.use_channels == 'y' %}
# Redis
REDIS_HOST = env('REDIS_HOST')
REDIS_PORT = env('REDIS_PORT'){% endif %}

# Caches
CACHES = {
    'default': {
        {% if cookiecutter.cache == 'redis' %}'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': f'redis://{REDIS_HOST}:{REDIS_PORT}/0',
        {% else %}'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',{% endif %}
        'KEY_PREFIX': '{{ cookiecutter.project_name }}',
    },
}

# Logging
class InfoOnlyFilter(logging.Filter):
    def filter(self, record):
        return record.levelno == logging.INFO

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'django.server': {
            '()': 'django.utils.log.ServerFormatter',
            'format': '[%(server_time)s] %(message)s',
        },
        'file_simple': {
            'format': '%(levelname)s %(asctime)s %(message)s'
        },
    },
    'filters': {
        'info_only': {
            '()': InfoOnlyFilter,
        },
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
        'django.server': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'django.server',
        },
        'django_file_error': {
            'level': 'WARNING',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(ROOT_DIR, 'logs/django_error_log.txt'),
            'maxBytes': 1024 * 1024,  # 1 MB
            'backupCount': 10,
            'formatter': 'file_simple',
        },
        'django_file_info': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(ROOT_DIR, 'logs/django_info_log.txt'),
            'maxBytes': 1024 * 1024,  # 1 MB
            'backupCount': 10,
            'formatter': 'file_simple',
            'filters': ['info_only'],
        },
        'file_default': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(ROOT_DIR, 'logs/default.txt'),
            'maxBytes': 1024 * 1024,  # 1 MB
            'backupCount': 10,
            'formatter': 'file_simple',
        },{% if cookiecutter.use_ecommerce == 'y' %}
        'store_file': {
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.join(ROOT_DIR, 'logs/store.txt'),
            'maxBytes': 1024 * 1024,  # 1 MB
            'backupCount': 10,
            'formatter': 'file_simple',
        },{% endif %}
    },
    'loggers': {
        'django': {
            'handlers': ['django_file_error', 'django_file_info', 'mail_admins'],
            'level': 'INFO',
            'propagate': True,
        },
        'django.server': {
            'handlers': ['django.server'],
            'level': 'INFO',
            'propagate': False,
        },
        '{{ cookiecutter.project_name }}.default': {
            'handlers': ['file_default'],
            'level': 'INFO',
            'propagate': True,
        },{% if cookiecutter.use_ecommerce == 'y' %}
        '{{ cookiecutter.project_name }}.store': {
            'handlers': ['store_file', 'mail_admins'],
            'level': 'INFO',
            'propagate': True,
        },{% endif %}
    },
}

# Email
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = env('EMAIL_HOST')
EMAIL_PORT = env('EMAIL_PORT')
EMAIL_HOST_USER = env('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = env('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL')
SERVER_EMAIL = DEFAULT_FROM_EMAIL

ADMINS = [
    # TODO add admins
]
MANAGERS = ADMINS

# django-webpack-loader
WEBPACK_LOADER = {
    'DEFAULT': {
        'CACHE': True,
        'BUNDLE_DIR_NAME': 'bundles/',
        'STATS_FILE': os.path.join(ROOT_DIR, 'webpack', 'webpack-stats.json'),
        'POLL_INTERVAL': 0.1,
        'TIMEOUT': None,
    },
}

# django-registration
REGISTRATION_OPEN = True


{% if cookiecutter.use_celery == 'y' %}
# Celery
CELERY_BROKER_URL = f'redis://{REDIS_HOST}:{REDIS_PORT}/1'
CELERY_RESULT_BACKEND = 'django-db'
CELERY_ACCEPT_CONTENT = ['application/json']
CELERY_RESULT_SERIALIZER = 'json'
CELERY_TASK_SERIALIZER = 'json'{% endif %}

{% if cookiecutter.use_channels == 'y' %}
# Channels
ASGI_APPLICATION = '{{ cookiecutter.project_name }}.routing.application'

CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [f'redis://{REDIS_HOST}:{REDIS_PORT}/2'],
        },
    },
}{% endif %}

{% if cookiecutter.use_wagtail == 'y' %}
# Wagtail
WAGTAIL_SITE_NAME = '{{ cookiecutter.project_name }}'{% endif %}

{% if cookiecutter.use_ecommerce == 'y' %}
# django-phonenumberfield
PHONENUMBER_DB_FORMAT = 'NATIONAL'
PHONENUMBER_DEFAULT_REGION = 'US'

# django-payments
PAYMENT_HOST = '127.0.0.1:8000'
PAYMENT_USES_SSL = False
PAYMENT_MODEL = 'store.Payment'
PAYMENT_VARIANTS = {
    'dummy': ('payments.dummy.DummyProvider', {}),
    #'stripe': ('payments.stripe.StripeProvider', {
    #    'name': '{{ cookiecutter.project_name }}',
    #    'secret_key': 'sk_test_your_key',
    #    'public_key': 'pk_test_your_key',
    #}),
}

STORE_CURRENCY = 'USD'
STORE_DEFAULT_PAYMENT_VARIANT = 'dummy'{% endif %}

# selenium
RUN_SELENIUM_TESTS = env.bool('RUN_SELENIUM_TESTS')
