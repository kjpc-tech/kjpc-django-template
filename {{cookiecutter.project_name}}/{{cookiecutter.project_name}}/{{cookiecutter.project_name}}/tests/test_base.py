import unittest

from urllib.parse import urlparse

from django.conf import settings
from django.contrib.auth import get_user_model
{% if cookiecutter.use_channels == 'n' %}from django.contrib.staticfiles.testing import StaticLiveServerTestCase{% endif %}
from django.test import tag, TestCase, override_settings
from django.urls import reverse
{% if cookiecutter.use_channels == 'y' %}
from channels.testing import ChannelsLiveServerTestCase
{% endif %}{% if cookiecutter.use_pytest == 'y' %}
import pytest
{% endif %}
if settings.RUN_SELENIUM_TESTS:
    from selenium.common.exceptions import NoSuchElementException
    from selenium.webdriver.common.by import By
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.firefox.webdriver import WebDriver
    from selenium.webdriver.support import expected_conditions
    from selenium.webdriver.support.ui import Select


@override_settings(SECURE_SSL_REDIRECT=False)
@tag('base')
class BaseTestCase(TestCase):
    """
    Don't redirect to https in tests.
    """
    pass


@unittest.skipUnless(settings.RUN_SELENIUM_TESTS, "RUN_SELENIUM_TESTS is False")
@tag('selenium'){% if cookiecutter.use_pytest == 'y' %}
@pytest.mark.selenium{% endif %}
class SeleniumTestCase({% if cookiecutter.use_channels == 'y' %}ChannelsLiveServerTestCase{% else %}StaticLiveServerTestCase{% endif %}):
    serve_static = True

    @classmethod
    def setUpClass(cls):
        super().setUpClass()

        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(10)

        cls.KEY_BACKSPACE = Keys.BACKSPACE

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()

        super().tearDownClass()

    def get_current_url(self):
        return urlparse(self.selenium.current_url).path

    def wait_for_text(self, css_selector, expected_text):
        self.explicit_wait.until(expected_conditions.text_to_be_present_in_element((By.CSS_SELECTOR, css_selector), expected_text))

    def wait_for_element(self, css_selector):
        self.explicit_wait.until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, css_selector)))

    def assertElementDoesExist(self, css_selector):
        self.assertIsNotNone(self.selenium.find_element_by_css_selector(css_selector))

    def assertElementDoesntExist(self, css_selector):
        with self.assertRaises(NoSuchElementException):
            self.selenium.find_element_by_css_selector(css_selector)

    def assertPageDoesContain(self, text):
        self.assertIn(text, self.selenium.page_source)

    def assertPageDoesntContain(self, text):
        self.assertNotIn(text, self.selenium.page_source)

    def clear_input_with_backspace(self, input_name):
        input_element = self.selenium.find_element_by_name(input_name)
        while len(input_element.get_attribute('value')):
            input_element.send_keys(self.KEY_BACKSPACE)

    def update_input_value(self, input_name, input_value):
        input_element = self.selenium.find_element_by_name(input_name)
        input_element.clear()
        input_element.send_keys(str(input_value))

    def choose_select_option(self, select_name, option_value):
        select = Select(self.selenium.find_element_by_name(select_name))
        select.select_by_value(str(option_value))

    def login_user(self, username, password):
        self.selenium.get(f"{self.live_server_url}{reverse('accounts:login')}")

        self.assertEqual(self.get_current_url(), reverse('accounts:login'))

        self.update_input_value('username', username)
        self.update_input_value('password', password)

        self.selenium.find_element_by_css_selector("main form .btn-primary").click()

        self.assertEqual(self.get_current_url(), reverse('accounts:profile'))


def _create_user(email, password, **kwargs):
    return get_user_model().objects.create_user(email, password, **kwargs)


class UserDataMixin:
    {% if cookiecutter.use_wagtail == 'y' %}fixtures = ['first-data.json']  # cms

    {% endif %}CREATE_USER_DATA = True

    def setUp(self):
        super().setUp()

        if self.CREATE_USER_DATA:
            self.userObj = self.create_user('test@example.com', 'secret')

    def create_user(self, email, password, **kwargs):
        return _create_user(email, password, **kwargs)

{% if cookiecutter.use_pytest == 'y' %}
@pytest.fixture
def user_data_fixture(db):
    _create_user('test@example.com', 'secret')
{% endif %}
