from django.urls import reverse

from {{ cookiecutter.project_name }}.tests.test_base import BaseTestCase


class IndexViewTests(BaseTestCase):
    def test_index_view(self):
        response = self.client.get(reverse('index'))

        self.assertEqual(response.status_code, 200)

        self.assertTemplateUsed(response, 'index.html')
