"""{{ cookiecutter.project_name }} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.generic import TemplateView

from accounts.views import AccountsRegistrationView

from {{ cookiecutter.project_name }} import views


# customize the admin site
admin.site.site_header = "{{ cookiecutter.project_name }} Administration"
admin.site.site_title = "{{ cookiecutter.project_name }} Admin"


urlpatterns = [
    # django admin
    path('admin/', admin.site.urls),

    # django registration and accounts
    path('accounts/register/', AccountsRegistrationView.as_view(), name='django_registration_register'),  # noqa
    path('accounts/', include('django_registration.backends.one_step.urls')),
    path('accounts/', include('accounts.urls')),

    {% if cookiecutter.use_ecommerce == 'y' %}
    # payments
    path('payments/', include('payments.urls')),

    path('store/', include('store.urls')),{% endif %}

    {% if cookiecutter.use_wagtail == 'y' %}
    # cms admin
    path('cms-admin/', include('wagtail.admin.urls')),
    path('cms-docs/', include('wagtail.documents.urls')),
    # cms pages served under `/cms/` path
    path('cms/', include('wagtail.core.urls')),{% endif %}

    path('page1/', TemplateView.as_view(template_name='page1.html'), name='page1'),
    path('', views.IndexView.as_view(), name='index'),
]


if settings.DEBUG:
    from django.conf.urls.static import static

    # serve media files while debugging
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    # django-debug-toolbar
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [
            path('__debug__/', include(debug_toolbar.urls)),
        ] + urlpatterns
